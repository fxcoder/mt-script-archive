#property copyright "MultiStochastic v7.1. © FXcoder"
#property link      "http://fxcoder.blogspot.com"
#property strict

#property indicator_separate_window
#property indicator_buffers 8
#property indicator_plots 8

#property indicator_color1 DodgerBlue
#property indicator_color2 C'62,123,218'
#property indicator_color3 C'94,102,182'
#property indicator_color4 C'126,82,145'
#property indicator_color5 C'158,61,109'
#property indicator_color6 C'190,41,72'
#property indicator_color7 C'222,20,36'
#property indicator_color8 Red

#property indicator_minimum 0
#property indicator_maximum 100

#define MAX_PERIOD 10000
#define PUT_IN_RANGE(A, L, H) ((H) < (L) ? (A) : ((A) < (L) ? (L) : ((A) > (H) ? (H) : (A))))
#define COLOR_IS_NONE(C) (((C) >> 24) != 0)
#define RGB_TO_COLOR(R, G, B) ((color)((((B) & 0x0000FF) << 16) + (((G) & 0x0000FF) << 8) + ((R) & 0x0000FF)))

/* Периоды расчета */
input int LineCount = 8;                         // Line count
input int PeriodStart = 3;                       // Period start
input int PeriodEnd = 89;                        // Period end
input bool ExpPeriodStep = true;                 // Use exponential period step
input string Periods = "";                       // User periods (overrides above params)

/* Периоды сглаживания. При Slowing > 1 индикатор превращается в Stochastic */
input int SlowingStart = 1;                      // Slowing period start
input int SlowingEnd = 5;                        // Slowing period end
input bool ExpSlowingStep = true;                // Use exponential slowing period step
input ENUM_MA_METHOD SlowingMethod = MODE_SMA;   // Slowing method

/* Источник */
input string Sym = "";                           // Symbol
input bool Reverse = false;                      // Reverse
input ENUM_STO_PRICE PriceType = STO_LOWHIGH;    // Price type

/* Оформление */
input color StartColor = Red;                    // Start color
input color EndColor = DodgerBlue;               // End color
input bool StartInFront = true;                  // Start in front
input int LineWidth = 1;                         // Line width

/* Бары */
input int Shift = 0;                             // Shift
input int MaxBars = 5000;                        // Maximum number of bars

int OnInit()
{
	// Проверка параметров
	_symbol = Sym == "" ? _Symbol : Sym;
	_isChartSymbol = _symbol == _Symbol;
	_lineCount = PutInRange(LineCount, 1, 8);

	_periodStart = PutInRange(PeriodStart, 1, MAX_PERIOD);
	_periodEnd = PutInRange(PeriodEnd, 1, MAX_PERIOD);

	_slowingStart = PutInRange(SlowingStart, 1, MAX_PERIOD);
	_slowingEnd = PutInRange(SlowingEnd, 1, MAX_PERIOD);

	// Расчет периодов
	_lineCount = MultiGetPeriods(_periodStart, _periodEnd, _lineCount, ExpPeriodStep, Periods, _periods);
	MultiGetPeriodsRange(_slowingStart, _slowingEnd, _lineCount, ExpSlowingStep, _slowings);

	ArrayResize(_stochs, _lineCount);

	_drawingBegin = 0;

	for (int i = 0; i < _lineCount; i++)
	{
		int p = StartInFront ? _lineCount - 1 - i : i;
		SetIndexBuffer(i, _stochs[p].Buffer);
		ArraySetAsSeries(_stochs[p].Buffer, true);

		int drawingBegin = _periods[p] + _slowings[p];

		if (drawingBegin > _drawingBegin)
			_drawingBegin = drawingBegin;

		PlotIndexSetInteger(i, PLOT_DRAW_BEGIN, drawingBegin);
		PlotIndexSetInteger(i, PLOT_SHIFT, Shift);
		_stochs[p].Handle = iStochastic(_symbol, _Period, _periods[p], 1, _slowings[p], SlowingMethod, PriceType);

	}

	// Стили и подписи линий
	MultiSetLineStyles(_periods, StartColor, EndColor, StartInFront, LineWidth, "Stoch ");

	// Имя индикатора с перечислением диапазонов периодов
	IndicatorSetString(INDICATOR_SHORTNAME, MQLInfoString(MQL_PROGRAM_NAME) + ": " +
		(Reverse ? "-" : "") + _symbol + "(" +
		((Periods != "") ? ArrayToString(_periods, ",") : IntRangeToString(_periods)) + "/" +
		(_slowingStart == _slowingEnd ? IntegerToString(_slowingStart) : IntRangeToString(_slowings)) + ")");

	return(INIT_SUCCEEDED);
}

int OnCalculate(const int ratesTotal, const int prevCalculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
	ArraySetAsSeries(time, true);

	int barsToCalc = GetBarsToCalculate(ratesTotal, _symbol, prevCalculated, MaxBars, _isChartSymbol ? 1 : 5, _drawingBegin);
	int calculatedBars = ratesTotal - barsToCalc;

	EmptyBars(_isFirstRun ? -1 : barsToCalc);
	_isFirstRun = false;

	for (int i = barsToCalc - 1; i >= 0; i--)
	{

		double stoch = EMPTY_VALUE;

		for (int p = 0; p < _lineCount; p++)
		{
			stoch = IndicatorValueByTime(_stochs[p].Handle, time[i]);

			if (stoch == EMPTY_VALUE)
				break;

			if (Reverse)
				stoch = 100 - stoch;

			_stochs[p].Buffer[i] = stoch;
		}

		if (stoch == EMPTY_VALUE)
		{
			EmptyBar(i);
			break;
		}

		calculatedBars++;
	}

	return(calculatedBars);
}

void OnDeinit(const int reason)
{
	for (int i = 0; i < _lineCount; i++)
		IndicatorRelease(_stochs[i].Handle);
}

bool EmptyBars(int bars = -1)
{
	if (bars < 0)
	{
		for (int i = 0; i < _lineCount; i++)
			ArrayInitialize(_stochs[i].Buffer, EMPTY_VALUE);
	}
	else
	{
		for (int i = 0; i < _lineCount; i++)
			ArrayInitialize(_stochs[i].Buffer, EMPTY_VALUE, 0, bars);
	}

	return(true);
}

void EmptyBar(int bar)
{
	for (int i = 0; i < _lineCount; i++)
		_stochs[i].Buffer[bar] = EMPTY_VALUE;
}

template <typename T>
int ArrayIndexOf(const T &arr[], const T value, const int startingFrom = 0)
{
	int count = ArraySize(arr);

	for (int i = startingFrom; i < count; i++)
	{
		if (arr[i] == value)
			return(i);
	}

	return(-1);
}

template <typename T>
int ArrayAdd(T &arr[], T value, const bool checkUnique = false, const int reserveSize = 100)
{
	int i;

	// Найти существующий, если необходимо
	if (checkUnique)
	{
		i = ArrayIndexOf(arr, value);

		if (i != -1)
			return(i);
	}

	// Добавить новый элемент в массив
	i = ArrayResize(arr, ArraySize(arr) + 1, reserveSize);

	if (i == -1)
		return(-1);

	i--;
	arr[i] = value;

	return(i);
}

template <typename T>
string ArrayToString(const T &arr[], const string separator)
{
	int size = ArraySize(arr);

	if (size == 0)
		return("");

	string result = (string)arr[0];

	for (int i = 1; i < size; i++)
	{
		StringAdd(result, separator);
		StringAdd(result, (string)arr[i]);
	}

	return(result);
}

template <typename T>
bool ArrayCheckRange(const T &arr[], int &start, int &count)
{
	int size = ArraySize(arr);

	// в случае пустого массива результат неопределён, но вернуть как ошибочный
	if (size <= 0)
		return(false);

	// в случае нулевого диапазона результат неопределён, но вернуть как ошибочный
	if (count == 0)
		return(false);

	// старт выходит за границы массива
	if ((start > size - 1) || (start < 0))
		return(false);

	if (count < 0)
	{
		// если количество не указано, вернуть всё от start
		count = size - start;
	}
	else if (count > size - start)
	{
		// если элементов недостаточно для указанного количества, вернуть максимально возможное
		count = size - start;
	}

	return(true);
}

template <typename T>
T PutInRange(const T value, const T from, const T to)
{
	return(PUT_IN_RANGE(value, from, to));
}

double MathRound(const double value, const double error)
{
	return(error == 0 ? value : MathRound(value / error) * error);
}

template <typename ArrayType, typename ValueType>
int ArrayInitialize(ArrayType &arr[], const ValueType value, int start, int count = -1)
{
	if (!ArrayCheckRange(arr, start, count))
		return(0);

	for (int i = start, end = start + count; i < end; i++)
		arr[i] = (ArrayType)value;

	return(count);
}

struct Plot
{
	double Buffer[];  // буфер линии

	int Handle;       // хэндл основного индикатора

};

string ReplaceString(string inputString, const string from, const string to)
{
	// Используем стандартную функцию
	StringReplace(inputString, from, to);
	return(inputString);
}

string Trim(string s)
{
	StringTrimRight(s);
	StringTrimLeft(s);

	return(s);
}

int SplitString(string s, const string sep, string &parts[], const bool removeEmpty = false, const bool unique = false)
{
	int count = 0;
	int sepLen = StringLen(sep);
	ArrayFree(parts);

	string part;

	while (true)
	{
		int p = StringFind(s, sep);

		if (p >= 0)
		{
			if (p == 0)
				part = "";
			else
				part = StringSubstr(s, 0, p);

			if (!removeEmpty || (Trim(part) != ""))
			{
				ArrayAdd(parts, part, unique);
				count = ArraySize(parts);
			}

			s = StringSubstr(s, p + sepLen);
		}
		else
		{
			// Последний кусок
			if (!removeEmpty || (Trim(s) != ""))
			{
				ArrayAdd(parts, s, unique);
				count = ArraySize(parts);
			}

			break;
		}
	}

	// удалить последнюю пустую строку
	if ((count > 0) && (parts[count - 1] == ""))
	{
		count--;

		if (count > 0)
			ArrayResize(parts, count);
	}

	return(count);
}

string IntRangeToString(int &range[])
{
	int count = ArraySize(range);

	if (count > 0)
	{
		return(
			IntegerToString(range[0]) +
				(count > 1 ? ", " + IntegerToString(range[1]) : "") +
				(count > 2 ? ", " + IntegerToString(range[2]) : "") +
				(count > 3 ? ".." + IntegerToString(range[count - 1]) : "")
			);
	}
	else
	{
		return("");
	}
}

double IndicatorValueByTime(const int handle, const datetime barTime, const double emptyValue = EMPTY_VALUE)
{
	if (handle == INVALID_HANDLE)
		return(emptyValue);

	double values[];

	if (CopyBuffer(handle, 0, barTime, 1, values) != 1)
		return(emptyValue);

	return(values[0]);
}

int GetBarsToCalculate(const int ratesTotal, const string symbol, const int prevCalculated, const int maxBars, const int minBars, const int reservedBars)
{
	int barsToCalculate;

	// нерассчитанные бары
	barsToCalculate = ratesTotal - prevCalculated;

	// сначала проверим минимум, т.к. он необязателен
	// минимум может быть 0 (например, при расчёте по ценам открытия)
	if (barsToCalculate < minBars)
		barsToCalculate = minBars;

	// проверить максимум
	if ((maxBars > 0) && (barsToCalculate > maxBars))
		barsToCalculate = maxBars;

	// максимум баров инструмента
	int symbolRatesTotal = symbol == _Symbol ? ratesTotal : Bars(symbol, _Period);

	if (barsToCalculate > symbolRatesTotal)
		barsToCalculate = symbolRatesTotal;

	// проверить резерв
	if (symbolRatesTotal - barsToCalculate < reservedBars)
		barsToCalculate = symbolRatesTotal - reservedBars;

	return(barsToCalculate >= 0 ? barsToCalculate : 0);
}

bool ColorToRGB(const color c, int &r, int &g, int &b)
{
	// Если цвет задан неверный, либо задан как отсутствующий, вернуть false
	if (COLOR_IS_NONE(c))
		return(false);

	b = (c & 0xFF0000) >> 16;
	g = (c & 0x00FF00) >> 8;
	r = (c & 0x0000FF);

	return(true);
}

color MixColors(const color color1, const color color2, double mix, double step = 16)
{
	// Коррекция неверных параметров
	step = PUT_IN_RANGE(step, 1.0, 255.0);
	mix = PUT_IN_RANGE(mix, 0.0, 1.0);

	int r1, g1, b1;
	int r2, g2, b2;

	// Разбить на компоненты
	ColorToRGB(color1, r1, g1, b1);
	ColorToRGB(color2, r2, g2, b2);

	// вычислить
	int r = PUT_IN_RANGE((int)MathRound(r1 + mix * (r2 - r1), step), 0, 255);
	int g = PUT_IN_RANGE((int)MathRound(g1 + mix * (g2 - g1), step), 0, 255);
	int b = PUT_IN_RANGE((int)MathRound(b1 + mix * (b2 - b1), step), 0, 255);

	return(RGB_TO_COLOR(r, g, b));
}

// Расчет периодов для индикаторов серии Multi*
int MultiGetPeriodsRange(int periodStart, int periodEnd, int lineCount, bool expStep, int &periods[])
{
	if (lineCount <= 0)
	{
		ArrayFree(periods);
		return(0);
	}

	ArrayResize(periods, lineCount);

	// Периоды расчета и периоды сглаживания
	if (lineCount == 1)
	{
		periods[0] = periodStart;
	}
	else
	{
		if (expStep)
		{
			// расчет экспоненциально изменяющегося набора периодов по крайним значениям
			double a = 0;
			double b = 0;

			if (periodStart > 0)
			{
				b = MathPow(1.0 * periodEnd / periodStart, 1.0 / (lineCount - 1));
				a = periodStart;
			}

			for (int i = 0; i < lineCount; i++)
				periods[i] = (int)MathRound(a * MathPow(b, i));
		}
		else
		{
			double step = 1.0 * (periodEnd - periodStart) / (lineCount - 1);

			for (int j = 0; j < lineCount; j++)
				periods[j] = (int)MathRound(1.0 * periodStart + j * step);
		}
	}

	return(lineCount);
}

int MultiGetPeriods(int periodStart, int periodEnd, int lineCount, bool expStep, string userPeriods, int &periods[])
{
	if (userPeriods != "")
		return(MultiGetPeriodsUser(userPeriods, lineCount, periods));

	return(MultiGetPeriodsRange(periodStart, periodEnd, lineCount, expStep, periods));
}

// Инициализировать набор периодов, заданный вручную. Вернуть количество периодов и их набор, не более lineCount.
int MultiGetPeriodsUser(string userPeriods, int lineCount, int &periods[])
{
	string parts[];
	userPeriods = ReplaceString(userPeriods, " ", ",");
	userPeriods = ReplaceString(userPeriods, ";", ",");
	int periodCount = SplitString(userPeriods, ",", parts, true, true);

	int realCount = 0;
	ArrayFree(periods);

	for (int i = 0; i < periodCount; i++)
	{
		int period = (int)StringToInteger(parts[i]);

		if (period != 0)
		{
			realCount = ArrayAdd(periods, period) + 1;

			// жесткое ограничение на количество периодов
			if (realCount == lineCount)
				break;
		}
	}

	return(realCount);
}

void MultiSetLineStyles(int &periods[], color startColor, color endColor, bool startInFront, int lineWidth, string prefix)
{
	int lineCount = ArraySize(periods);

	// Стили и подписи линий
	for (int i = 0; i < lineCount; i++)
	{
		color c = MixColors(startColor, endColor, 1.0 * i / (lineCount - 1), 1);

		// номер линии в соответствии с порядком
		int k = startInFront ? lineCount - 1 - i : i;

		PlotIndexSetInteger(k, PLOT_DRAW_TYPE, DRAW_LINE);
		PlotIndexSetInteger(k, PLOT_LINE_STYLE, STYLE_SOLID);
		PlotIndexSetInteger(k, PLOT_LINE_WIDTH, lineWidth);
		PlotIndexSetInteger(k, PLOT_LINE_COLOR, c);
		PlotIndexSetString(k, PLOT_LABEL, prefix + IntegerToString(periods[i]));

	}
}

int _periods[], _slowings[]; // наборы периодов
bool _isChartSymbol; // является ли расчетный символ символом чарта

int _lineCount, _periodStart, _periodEnd, _slowingStart, _slowingEnd;
string _symbol;
bool _isFirstRun = true;
int _drawingBegin;

Plot _stochs[];

/*
Список изменений:

7.1
	* изменены ссылки на сайт
	* список изменений в коде

7.0
    * вариант для MT5
    * тип параметра SlowingMethod изменён на ENUM_MA_METHOD

6.0
	* из имени файла убран префикс "+"
	* изменены ссылки на сайт
	* оптимизация
	* изменены отображаемые названия параметров
	* добавлен параметр Price type (тип цены для расчёта: Low/High или Close/Close)

5.2
	* совместимость с MetaTrader версии 4.00 Build 600 и новее

5.1
	* минимальная совместимость с новым MQL4

Лицензия:

Разрешается повторное распространение и использование как в виде исходного кода, так и в двоичной форме, с изменениями
или без, при соблюдении следующих условий:

При повторном распространении исходного кода должно оставаться указанное выше уведомление об авторском праве, этот
список условий и последующий отказ от гарантий.

При повторном распространении двоичного кода должна сохраняться указанная выше информация об авторском праве, этот
список условий и последующий отказ от гарантий в документации и/или в других материалах, поставляемых при распространении.

Ни название FXcoder, ни имена ее сотрудников не могут быть использованы в качестве поддержки или продвижения продуктов,
основанных на этом ПО без предварительного письменного разрешения.

ЭТА ПРОГРАММА ПРЕДОСТАВЛЕНА ВЛАДЕЛЬЦАМИ АВТОРСКИХ ПРАВ И/ИЛИ ДРУГИМИ СТОРОНАМИ «КАК ОНА ЕСТЬ» БЕЗ КАКОГО-ЛИБО ВИДА
ГАРАНТИЙ, ВЫРАЖЕННЫХ ЯВНО ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ, ПОДРАЗУМЕВАЕМЫЕ ГАРАНТИИ КОММЕРЧЕСКОЙ
ЦЕННОСТИ И ПРИГОДНОСТИ ДЛЯ КОНКРЕТНОЙ ЦЕЛИ. НИ В КОЕМ СЛУЧАЕ, ЕСЛИ НЕ ТРЕБУЕТСЯ СООТВЕТСТВУЮЩИМ ЗАКОНОМ, ИЛИ НЕ
УСТАНОВЛЕНО В УСТНОЙ ФОРМЕ, НИ ОДИН ВЛАДЕЛЕЦ АВТОРСКИХ ПРАВ И НИ ОДНО ДРУГОЕ ЛИЦО, КОТОРОЕ МОЖЕТ ИЗМЕНЯТЬ И/ИЛИ
ПОВТОРНО РАСПРОСТРАНЯТЬ ПРОГРАММУ, КАК БЫЛО СКАЗАНО ВЫШЕ, НЕ НЕСЁТ ОТВЕТСТВЕННОСТИ, ВКЛЮЧАЯ ЛЮБЫЕ ОБЩИЕ, СЛУЧАЙНЫЕ,
СПЕЦИАЛЬНЫЕ ИЛИ ПОСЛЕДОВАВШИЕ УБЫТКИ, ВСЛЕДСТВИЕ ИСПОЛЬЗОВАНИЯ ИЛИ НЕВОЗМОЖНОСТИ ИСПОЛЬЗОВАНИЯ ПРОГРАММЫ (ВКЛЮЧАЯ, НО
НЕ ОГРАНИЧИВАЯСЬ ПОТЕРЕЙ ДАННЫХ, ИЛИ ДАННЫМИ, СТАВШИМИ НЕПРАВИЛЬНЫМИ, ИЛИ ПОТЕРЯМИ ПРИНЕСЕННЫМИ ИЗ-ЗА ВАС ИЛИ ТРЕТЬИХ
ЛИЦ, ИЛИ ОТКАЗОМ ПРОГРАММЫ РАБОТАТЬ СОВМЕСТНО С ДРУГИМИ ПРОГРАММАМИ), ДАЖЕ ЕСЛИ ТАКОЙ ВЛАДЕЛЕЦ ИЛИ ДРУГОЕ ЛИЦО БЫЛИ
ИЗВЕЩЕНЫ О ВОЗМОЖНОСТИ ТАКИХ УБЫТКОВ.

License:

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

Neither the name of the FXcoder nor the names of its contributors may be used to endorse or promote products derived
from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

// 2016-02-04 06:49:40 UTC. MQLMake 1.37. © FXcoder