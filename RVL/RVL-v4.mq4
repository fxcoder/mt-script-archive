/*
   Индикатор распределения реальных объёмов фьючерсов по уровням.
   Copyright © FXcoder, 2010-2014. http://fxcoder.net
   Внимание! Индикатор работает с данными, загружаемыми из программы RVL Server (http://fxcoder.net/software/rvlserver),
   либо другого источника с совместимым API (http://fxcoder.net/software/rvlserver/api)
*/

#property copyright "Real Volume Levels v4.0. Copyright © FXcoder, 2010-2014"
#property link      "http://fxcoder.net"
#property strict
#property indicator_chart_window

enum ENUM_RVL_MODE {
	RVL_MODE_BETWEEN_LINES = 0,     // Between lines
	RVL_MODE_CONTRACT = 1,          // Contract
	RVL_MODE_DAYS_TO_LINE = 2,      // Days to line
	RVL_MODE_CONTRACT_TO_LINE = 3,  // Contract to line
	RVL_MODE_DAILY = 4,             // Daily
	RVL_MODE_WEEKLY = 5,            // Weekly
	RVL_MODE_DAILY_CONTRACT = 6     // Daily contract
};

enum ENUM_RVL_HG_STYLE {
	RVL_HG_STYLE_LINES = 0,             // Lines
	RVL_HG_STYLE_EMPTY_RECTANGLES = 1,  // Empty rectangles
	RVL_HG_STYLE_FILLED_RECTANGLES = 2, // Filled rectangles
	RVL_HG_STYLE_OUTLINE = 3     // Outline
};

enum ENUM_RVL_HG_POSITION {
	RVL_HG_POSITION_LEFT = 0,  // Left
	RVL_HG_POSITION_RIGHT = 1, // Right
};

/* Параметры расчетов */
input string ContractMonth = "";                                     // Contract month (YY/MM), empty=current
input ENUM_RVL_MODE RangeMode = RVL_MODE_DAILY;                      // Range mode
input int RangeCount = 10;                                           // Range count
input int ModeStep = 15;                                             // Mode step, points

/* Гистограмма и уровни */
input ENUM_RVL_HG_POSITION HGPosition = RVL_HG_POSITION_LEFT;        // Histogram position
input color HGColor = LightSteelBlue;                                // Histogram color
input color HGColor2 = clrNONE;                                      // Histogram second color
input int VolumeThreshold = 0;                                       // Volume threshold for second color
input ENUM_RVL_HG_STYLE HGStyle = RVL_HG_STYLE_EMPTY_RECTANGLES;     // Histogram bar style
input int HGLineWidth = 1;                                           // Histogram line width

input color ModeColor = Blue;                                        // Mode color
input color MaxModeColor = clrNONE;                                  // Maximum volume mode color
input color MedianColor = clrNONE;                                   // Median color
input color VWAPColor = clrNONE;                                     // VWAP color

input int ModeWidth = 1;                                             // Mode line width
input ENUM_LINE_STYLE ModeStyle = STYLE_SOLID;                       // Mode line style

/* Уровни */
input color ModeLevelColor = clrNONE;                                // Mode level line color
input int ModeLevelWidth = 1;                                        // Mode level line width
input ENUM_LINE_STYLE ModeLevelStyle = STYLE_DOT;                    // Mode level line style

/* Служебные */
input double Zoom = 0;                                               // Zoom, 0=auto
input string Id = "rvl";                                             // Identifier
input double VerticalShift = 0;                                      // Vertical shift

bool NoCache = false;         // Не использовать файловый кэш
int UpdateWait = 3;           // Пауза между обновлениями
bool UseProductsFile = true;  // Использовать файл с инструментами (products_*.txt), отдельный для каждого сервера, иначе будет использоваться только список по умолчанию

color TimeFromColor = Blue;                  // левая граница диапазона - цвет
ENUM_LINE_STYLE TimeFromStyle = STYLE_DASH;  // левая граница диапазона - стиль
color TimeToColor = Red;                     // правая граница диапазона - цвет
ENUM_LINE_STYLE TimeToStyle = STYLE_DASH;    // правая граница диапазона - стиль

string _onp, _tfn, _ttn;
string _baseURL;
bool _isSymbolFound = false;

string _futCode = "";     // код фьючерса
string _optCode = "";     // код опциона
string _exchange = "";    // биржа
double _scale = 1;        // масштаб
bool _reverse = false;    // реверс (для некоторых кроссов FX)
string _month = "";

bool _showHG, _showModes, _showMaxMode, _showModeLevel, _showMedian, _showVWAP;
bool _hgBack = true;
bool _hgUseRectangles = false;
double _hgPoint = 0.0001;
bool _isPeriodicMode;
bool _isDailyMode;

double _prices[];
int _volumes[];
int _vCount = 0;
datetime _calcTimeFrom = 0;
datetime _calcTimeTo = 0;
int _calcRangeMode = -1;
datetime _lastCalcDay = 0;
bool _useWininet = false;    // Использовать ли библиотеку wininet.dll для загрузки данных. Если нет, будет использоваться WebRequest().

// используемые цвета
color _hgColor, _hgColor2, _modeColor, _maxModeColor, _medianColor, _vwapColor, _modeLevelColor;

int init()
{
	_onp = Id + "-m" + IntegerToString(RangeMode) + " " + ContractMonth + " ";
	_tfn = Id + "-m" + IntegerToString(RangeMode) + "-from";
	_ttn = Id + "-m" + IntegerToString(RangeMode) + "-to";

	_isDailyMode = (RangeMode == RVL_MODE_DAILY) || (RangeMode == RVL_MODE_DAILY_CONTRACT);
	_isPeriodicMode = _isDailyMode || (RangeMode == RVL_MODE_WEEKLY);

	// цвет
	_maxModeColor = MaxModeColor;
	_medianColor = MedianColor;
	_vwapColor = VWAPColor;

	_hgColor = HGColor;
	_hgColor2 = HGColor2;

	if (ColorIsNone(_hgColor2))
		_hgColor2 = HGColor;

	_modeColor = ModeColor;
	_modeLevelColor = ModeLevelColor;

	if (ColorIsNone(_hgColor))
		_hgColor2 = _hgColor;

	// настройки отображения
	_showHG = !ColorIsNone(_hgColor);
	_showModes = !ColorIsNone(_modeColor);
	_showMaxMode = !ColorIsNone(_maxModeColor);
	_showModeLevel = !ColorIsNone(_modeLevelColor);
	_showMedian = !ColorIsNone(_medianColor);
	_showVWAP = !ColorIsNone(_vwapColor);

	// корректируем параметры стиля
	if (HGStyle == RVL_HG_STYLE_EMPTY_RECTANGLES)
	{
		_hgBack = false;
		_hgUseRectangles = true;
	}
	else if (HGStyle == RVL_HG_STYLE_FILLED_RECTANGLES)
	{
		_hgBack = true;
		_hgUseRectangles = true;
	}

	// параметры сервера

	int port = (int)GlobalVariableGet("rvl-port");

	if (port == 0)
		port = 8089;

	if (GlobalVariableGet("rvl-remote") == 1)
		_baseURL = "http://rvlserver:" + IntegerToString(port) + "/?service=volprice";
	else
		_baseURL = "http://localhost:" + IntegerToString(port) + "/?service=volprice";

	// таймеры
	TimerInit();
	TimerSet("update", UpdateWait);

	return(0);
}

int start()
{
	// Сразу выходим, если информации инструмент не был определён
	if (!_isSymbolFound)
		if (!initParams())
			return(0);

	// Уменьшение частоты обновления индикатора
	// обновлять не чаще, чем раз в несколько секунд
	if (!TimerCheck("update"))
		return(0);

	// Примерное время последней минутки (без использования M1)
	datetime lastMinuteTime = Time[0] + Period() * 60 - 1;

	// Определение рабочего диапазона
	datetime timeFrom, timeTo;

	if (RangeMode == RVL_MODE_BETWEEN_LINES) // между двух линий
	{
		timeFrom = getObjectTime1(_tfn);
		timeTo = getObjectTime1(_ttn);

		if ((timeFrom == 0) || (timeTo == 0))
		{
			// если границы диапазона не заданы, устанавить их в видимую часть экрана
			datetime timeLeft = BarTime(WindowFirstVisibleBar());
			datetime timeRight = BarTime(WindowFirstVisibleBar() - WindowBarsPerChart());
			datetime r = timeRight - timeLeft;

			timeFrom = timeLeft + r / 3;
			timeTo = timeLeft + r * 2 / 3;

			drawVLine(_tfn, timeFrom, TimeFromColor, 1, TimeFromStyle, false);
			drawVLine(_ttn, timeTo, Crimson, 1, STYLE_DASH, false);
		}

		if (timeFrom > timeTo)
		{
			datetime dt = timeTo;
			timeTo = timeFrom;
			timeFrom = dt;
		}
	}
	else if (RangeMode == RVL_MODE_CONTRACT_TO_LINE) // контракт, от правой линии
	{
		timeFrom = 0;
		ObjectDelete(_tfn);

		timeTo = getObjectTime1(_ttn);

		if (timeTo == 0)
		{
			// если границы диапазона не заданы, устанавить их в видимую часть экрана
			timeTo = BarTime((int)(WindowFirstVisibleBar() - WindowBarsPerChart() * 2.0 / 3.0));
			drawVLine(_ttn, timeTo, Crimson, 1, STYLE_DASH, false);
		}

	}
	else if (RangeMode == RVL_MODE_DAYS_TO_LINE) // от правой границы
	{
		// Определить правую границу

		ObjectDelete(_tfn);
		timeTo = getObjectTime1(_ttn);

		if (timeTo == 0)
		{
			// если границы диапазона не заданы, устанавить их в видимую часть экрана
			timeTo = BarTime((int)(WindowFirstVisibleBar() - WindowBarsPerChart() * 2.0 / 3.0));
			drawVLine(_ttn, timeTo, Crimson, 1, STYLE_DASH, false);
		}

		// Определить левую границу
		// Если граница указана правее баров, отсчет идет от самой правой минутки
		timeFrom = BeginOfDay(TimeAddWorkDays(MathMin(timeTo, lastMinuteTime), -(RangeCount - 1)));

		drawVLine(_tfn, timeFrom, TimeFromColor, 1, TimeFromStyle, false);
	}
	else if (RangeMode == RVL_MODE_CONTRACT) // контракт, последние данные
	{
		ObjectDelete(_tfn);
		ObjectDelete(_ttn);
		timeFrom = 0;
		timeTo = Time[0];// getObjectTime1(ttn);
	}
	else// if (RangeMode == 4, 5, 6)
	{
		ObjectDelete(_tfn);
		ObjectDelete(_ttn);
		timeFrom = 0;
		timeTo = 0;
	}

	double zoom;
	double maxVolume;
	// бар нулевой отметки гистограммы
	int bar0;

	// Загрузка и отображение данных

	if (!_isPeriodicMode) // режимы с одной гистограммой
	{
		// удаляем старые объекты
		clearChart(_onp);

		if (timeTo > lastMinuteTime)
			timeTo = lastMinuteTime;

		if (timeFrom > lastMinuteTime)
			timeFrom = lastMinuteTime;

		timeFrom = BeginOfDay(timeFrom);
		timeTo = BeginOfDay(timeTo);

		// получаем гистограмму
		if ((timeFrom != _calcTimeFrom) || (timeTo != _calcTimeTo) || (_calcRangeMode != RangeMode))
		{
			_vCount = loadHG(_futCode, _exchange, ContractMonth, timeFrom, timeTo, _scale, _reverse, _prices, _volumes);
			_calcTimeFrom = timeFrom;
			_calcTimeTo = timeTo;
			_calcRangeMode = RangeMode;
		}

		if (_vCount > 0)
		{
			// определение масштаба
			zoom = Zoom * 0.000001;

			if (zoom <= 0)
			{
				maxVolume = _volumes[ArrayMaximum(_volumes)];
				zoom = WindowBarsPerChart()*0.1 / maxVolume;
			}

			if (HGPosition == 0)			// левая граница окна
			{
				bar0 = WindowFirstVisibleBar();
			}
			else 	// правая граница окна
			{
				bar0 = WindowFirstVisibleBar() - WindowBarsPerChart();
				zoom = -zoom;	// справа переворачиваем
			}

			// рисуем

			if (_showHG)
				drawHG(_onp + "hg ", _prices, _volumes, bar0, _hgColor, _hgColor2, zoom, HGLineWidth);

			if (_showModes || _showModeLevel || _showMaxMode || _showMedian || _showVWAP)
				drawLevels(_onp, _prices, _volumes, bar0, zoom);
		}
	}
	else // режимы с разбивкой по периодам
	{
		// Проверка допустимых режимов для текущего таймфрейма
		if ((_isDailyMode && (Period() > PERIOD_D1))
			|| ((RangeMode == RVL_MODE_WEEKLY) && (Period() > PERIOD_W1)))
			return(0);

		int period = PERIOD_D1;

		if (RangeMode == RVL_MODE_WEEKLY)
			period = PERIOD_W1;

		datetime today = BeginOfDay(lastMinuteTime);

		if ((_lastCalcDay != today))
		{

			// Определение последнего доступного дня.
			// Если сейчас выходные, то можно загружать с текущего дня (пятницы), иначе - со вчерашнего

			int dayFrom = 1;
			int dow = TimeDayOfWeek(TimeLocal());

			if ((dow == 0) || (dow == 6) || (TimeLocal() - TimeCurrent() > 60 * 720))
				dayFrom = 0;

			// Определенить начало первой (правой) гистограммы
			if (_isDailyMode)
			{
				timeFrom = today;

				if (dayFrom == 1)
					timeFrom = TimeAddWorkDays(timeFrom, -1);
			}
			else
			{
				timeFrom = BeginOfWeek(today);

				if (dayFrom == 1)
					timeFrom = TimeAddWeeks(timeFrom, -1);
			}

			// Проход по всем дням, загрузка и отображение
			for (int day = dayFrom; day < RangeCount + dayFrom; day++)
			{
				// Границы рисования (время)

				if (_isDailyMode)
				{
					if (day > 0)
						timeTo = TimeAddWorkDays(timeFrom, 1);
					else
						timeTo = timeFrom + period * 60;
				}
				else
				{
					timeTo = TimeAddWeeks(timeFrom, 1);
				}

				int barFrom, barTo;

				//hack: коррекция границ для режима 5
				if (RangeMode == RVL_MODE_WEEKLY)
				{
					// начало
					timeFrom += 60 * 1440 * (1 - TimeDayOfWeek(timeFrom));

					// конец
					timeTo = timeFrom + 60 * 1440 * 4;
				}

				if (getRange(timeFrom, timeTo, barFrom, barTo))
				{
					//hack: коррекция правой границы отображения для режима 5
					if (RangeMode == RVL_MODE_WEEKLY)
					{
						barTo -= 1440 / Period() - 60 / Period(); // убираем дополнительно час, т.к. большинство ДЦ на час раньше заканчивают неделю

						if (barTo < 0)
							barTo = 0;
					}

					// получаем гистограмму
					clearChart(_onp + " " + TimeToStr(timeFrom)); // удаляем старые объекты

					if (RangeMode == RVL_MODE_DAILY_CONTRACT)
					{
						_vCount = loadHG(_futCode, _exchange, ContractMonth, 0, timeFrom, _scale, _reverse, _prices, _volumes);
					}
					else
					{
						if (RangeMode == RVL_MODE_DAILY)
							_vCount = loadHG(_futCode, _exchange, ContractMonth, timeFrom, timeFrom, _scale, _reverse, _prices, _volumes);
						else
							_vCount = loadHG(_futCode, _exchange, ContractMonth, timeFrom, timeTo, _scale, _reverse, _prices, _volumes);
					}

					if (_vCount > 0)
					{
						zoom = Zoom * 0.0001 / Period();

						if (zoom <= 0)
						{
							maxVolume = _volumes[ArrayMaximum(_volumes)];
							zoom = (barFrom - barTo) / maxVolume;
						}

						if (HGPosition == 1)
						{
							barFrom = barTo;
							zoom = -zoom;
						}

						// рисуем

						if (_showHG)
							drawHG(_onp + TimeToStr(timeFrom, TIME_DATE) + " hg ", _prices, _volumes, barFrom, _hgColor, _hgColor2, zoom, HGLineWidth);

						if (_showModes || _showMaxMode || _showMedian || _showVWAP)
							drawLevels(_onp + TimeToStr(timeFrom, TIME_DATE), _prices, _volumes, barFrom, zoom);

						_lastCalcDay = today;
					}
				}

				timeFrom = TimeAddWorkDays(timeFrom, -1);

				Sleep(50);
			}
		}
	}

	return(0);
}

int deinit()
{
	clearChart(_onp);

	if (UninitializeReason() == REASON_REMOVE)
	{
		clearChart(_tfn);
		clearChart(_ttn);
	}

	if (_useWininet)
		InetClose();

	return(0);
}

int loadHG(string ccode, string exchange, string month, datetime dateFrom, datetime dateTo, double scale, bool reverse,
	double &prices[], int &volumes[])
{
	// На сервере нет данных ранее 2009.01.02
	if (dateTo < D'2009.01.02')
		return(0);

	if (dateTo == 0)
		dateTo = BeginOfDay(Time[0]);

	string url = "&product=" + ccode + "&exchange=" + exchange + "&month=" + month;

	if (dateFrom != 0)
		url = url + "&from=" + dateToCME(dateFrom);

	url = _baseURL + url + "&to=" + dateToCME(dateTo);

	string m = month;

	if (m == "")
		m = "nearby"; // ближайший
	else if (m == "*")
		m = "total"; // сумма всех (не рекомендуется к использованию, пока это просто заглушка для тестирования дополнительных расчётов)

	string cachePath = exchange + "_" + ccode + "_FUT_" + m + "_" + TimeToStr(dateFrom, TIME_DATE) + "-" + TimeToStr(dateTo, TIME_DATE);
	cachePath = "rvl\\" + exchange + "\\" + ccode + "\\" + m + "\\" + ReplaceString(cachePath, ".", "") + "_HG.phg1";
	cachePath = ReplaceString(cachePath, "/", "");

	// получение данных
	string lines[];
	int lc = 0;

	if (!NoCache)
		lc = ReadStrings(cachePath, lines);

	// если в кэше пусто, то загружаем с сервера
	if (lc == 0)
	{
		// сначала определяем реальный период данных, чтобы корректно закэшировать
		lc = downloadStrings(url + "&action=getfutenv", lines);

		// проверяем формат (по количеству строк),
		// 		формат: [дата от]\n[дата до]\n[ближайший контракт на последнюю дату] (ровно три строки)
		if (lc == 3)
		{
			dateFrom = StrToTime(ReplaceString(lines[0], "-", "."));
			dateTo = StrToTime(ReplaceString(lines[1], "-", "."));

			if (dateFrom != 0 && dateTo != 0)
			{
				// пытаемся снова загрузить кэш, уже по реальному периоду
				cachePath = exchange + "_" + ccode + "_FUT_" + m + "_" + TimeToStr(dateFrom, TIME_DATE) + "-" + TimeToStr(dateTo, TIME_DATE);
				cachePath = "rvl\\" + exchange + "\\" + ccode + "\\" + m + "\\" + ReplaceString(cachePath, ".", "") + "_HG.phg1";
				cachePath = ReplaceString(cachePath, "/", "");

				if (!NoCache)
					lc = ReadStrings(cachePath, lines);
				else
					lc = 0;

				// если не получается, пытаемся загрузить данные за реальный период с сервера
				if (lc == 0)
				{
					// обновляем адрес, т.к. даты изменились на реальные
					url = _baseURL + "&product=" + ccode + "&exchange=" + exchange + "&month=" + month + "&from=" + dateToCME(dateFrom) + "&to=" + dateToCME(dateTo);
					lc = downloadStrings(url + "&action=getfut", lines);

					if ((lc > 0) && (lines[0] != "ad") && !NoCache)
						WriteStrings(cachePath, lines);
				}
			}
			else
			{
				// неверный формат дат в ответе о реальном периоде
				lc = 0;
			}
		}
		else
		{
			// неверный формат ответа о реальном периоде
			lc = 0;
		}

	}

	int count = 0;
	ArrayResize(prices, 0);
	ArrayResize(volumes, 0);

	// расшифровка строк
	int pc = ArraySize(lines);

	double price;
	int volume;

	for (int i = 0; i < pc; i++)
	{
		if (StringTrimLeft(lines[i]) != "")
		{
			int p = StringFind(lines[i], "\t");

			if (p == -1)
			{
				ArrayResize(prices, 0);
				ArrayResize(volumes, 0);
				count = 0;
				break;
			}

			price = StrToDouble(StringSubstr(lines[i], 0, p)) * scale;
			volume = StrToInteger(StringSubstr(lines[i], p + 1));
			count++;

			ArrayResize(prices, count);
			ArrayResize(volumes, count);
			prices[count-1] = price + VerticalShift;
			volumes[count-1] = volume;
		}
	}

	// переворачиваем, если нужно, и объединяем одинаковые после округления цены
	if (reverse && count > 0 && prices[0] != 0)
	{
		double op[];
		int ov[];
		ArrayCopy(op, prices);
		ArrayCopy(ov, volumes);
		int j = -1;
		double prevPrice = 0;

		for (int i = 0; i < count; i++)
		{
			price = op[count - 1 - i];

			if (price == 0)
			{
				j = -1;
				break;
			}

			price = Round(1.0 / price, _hgPoint);
			volume = ov[count - 1 - i];

			if (NormalizeDouble(price, 4) != NormalizeDouble(prevPrice, 4))
			{
				j++;
				prices[j] = price;
				volumes[j] = volume;
			}
			else
			{
				volumes[j] += volume;
			}

			prevPrice = price;
		}

		count = j + 1;
		ArrayResize(prices, count);
		ArrayResize(volumes, count);
	}

	return (count);
}

// нарисовать моды и прочие расчетные уровни
void drawLevels(string prefix, double& p[], int& v[], int barFrom, double zoom)
{
	int modes[], modeCount, j;
	double price;

	// поиск мод
	modeCount = getModesIndexes(v, ModeStep, modes);

	// макс. мода
	int max = 0;

	for (j = 0; j < modeCount; j++)
	{
		if (v[modes[j]] > max)
			max = v[modes[j]];
	}

	// всегда ужирняем моды в режимах рисования прямоугольниками
	bool back = _hgUseRectangles;

	string on;
	datetime timeFrom, timeTo;

	for (j = 0; j < modeCount; j++)
	{
		double vw = zoom * v[modes[j]];

		// не рисовать коротких линий (меньше бара ТФ), глючит при выделении границ
		if (MathAbs(vw) > 0)
		{
			price = p[modes[j]];

			bool isMaxMode = (v[modes[j]] == max) && _showMaxMode;
			color cl = _modeColor;

			if (isMaxMode)
				cl = _maxModeColor;

			timeFrom = BarTime(barFrom);
			timeTo = BarTime((int)(barFrom - vw));

			if (_showModes || isMaxMode)
			{
				on = prefix + "mode " + IntegerToString(v[modes[j]]) + "@" + DoubleToStr(price, Digits);
				drawBar(on, timeFrom, price, timeTo, price, cl, ModeWidth, ModeStyle, back, false, 0, _hgUseRectangles);

				// дорисовываем сверху ещё линию, иначе прямоугольник теряется
				if (_hgUseRectangles && back)
					drawBar(on + "+", timeFrom, price, timeTo, price, cl, ModeWidth, ModeStyle, false, false, 0, false);

			}

			if (_showModeLevel && !_isPeriodicMode)
				drawHLine(prefix + "level " + IntegerToString(j), price, _modeLevelColor, ModeLevelWidth, ModeLevelStyle, true);
		}
	}

	// прочие параметры
	timeFrom = BarTime(barFrom);

	if (ArraySize(v) > 0)
	{
		// медиана
		if (_showMedian)
		{
			int medianIndex = getMedianIndex(p, v);

			if (medianIndex >= 0)
			{
				timeTo = BarTime((int)(barFrom - zoom * v[medianIndex]));
				double median = p[medianIndex];

				on = _onp + TimeToStr(timeFrom, TIME_DATE) + " median " + DoubleToStr(median, Digits);
				drawBar(on, timeFrom, median, timeTo, median, _medianColor, ModeWidth + 1, ModeStyle, back, false, 0, _hgUseRectangles);

				// дорисовываем сверху ещё линию, иначе прямоугольник теряется
				if (_hgUseRectangles && back)
					drawBar(on + "+", timeFrom, median, timeTo, median, _medianColor, ModeWidth + 1, ModeStyle, false, false, 0, false);
			}
		}

		// vwap
		if (_showVWAP)
		{
			int vwapIndex = getVWAPIndex(p, v);

			if (vwapIndex >= 0)
			{
				timeTo = BarTime((int)(barFrom - zoom * v[vwapIndex]));
				double vwap = p[vwapIndex];

				on = _onp + TimeToStr(timeFrom, TIME_DATE) + " vwap " + DoubleToStr(vwap, Digits);
				drawBar(on, timeFrom, vwap, timeTo, vwap, _vwapColor, ModeWidth + 1, ModeStyle, back, false, 0, _hgUseRectangles);

				// дорисовываем сверху ещё линию, иначе прямоугольник теряется
				if (_hgUseRectangles && back)
					drawBar(on + "+", timeFrom, vwap, timeTo, vwap, _vwapColor, ModeWidth + 1, ModeStyle, false, false, 0, false);
			}
		}
	}
}

// Получить моды на основе гистограммы и сглаженной гистограммы (быстрый метод, без сглаживания)
int getModesIndexes(int& v[], int step, int& modes[]) //, int& maxModeIndex
{
	int modeCount = 0;
	ArrayResize(modes, modeCount);

	int count = ArraySize(v);

	// ищем максимумы по участкам
	for (int i = 0; i < count; i++)
	{
		int maxFrom = i - step;
		int maxRange = 2 * step + 1;

		// корректируем границы
		if (maxFrom < 0)
		{
			maxRange += maxFrom;
			maxFrom = 0;
		}

		// находим максимум вокруг уровня
		int k = ArrayMaximum(v, maxRange, maxFrom);

		// если мы на максимуме, то отмечаем все уровни, которые равны этому максимуму
		if (k == i)
		{

			for (int j = i - step; j <= i + step; j++)
			{
				if (j >= 0 && j < count)
				{
					if (v[j] == v[k])
					{
						modeCount++;
						ArrayResize(modes, modeCount);
						modes[modeCount - 1] = j;
					}
				}
			}
		}
	}

	return(modeCount);
}

int getMedianIndex(double& prices[], int& volumes[])
{
	int count = ArraySize(volumes);

	// общий объём
	int tv = 0;

	for (int i = 0; i < count; i++)
		tv += volumes[i];

	// половина объема
	double hv = tv / 2.0;

	// идём по гистограмме и останавливаемся на середине по объёму
	for (int i = 0, v = 0; i < count; i++)
	{
		v += volumes[i];

		if (v >= hv)
			return(i);
	}

	return(-1);
}

int getVWAPIndex(double& prices[], int& volumes[])
{
	double vwap = 0;
	double v = 0;
	int count = ArraySize(prices);

	for (int i = 0; i < count; i++)
	{
		vwap += prices[i] * volumes[i];
		v += volumes[i];
	}

	if (v == 0)
		return(-1);

	vwap = NormalizeDouble(vwap / v, Digits);

	// ищем индекс
	for (int i = 0; i < count; i++)
	{
		if (vwap <= prices[i])
			return(i);
	}

	return(-1);
}

// нарисовать гистограмму (+цвет +point)
void drawHG(string prefix, double& h[], int& v[], int barFrom, color hgColor, color hgColor2, double zoom, int width)
{
	int max = v[ArrayMaximum(v)];

	if (max == 0)
		return;

	color cl = hgColor;
	datetime timeFrom;
	int count = ArraySize(h);
	bool useGradient = (hgColor != hgColor2);

	if (HGStyle == RVL_HG_STYLE_OUTLINE)
	{
		count--;

		for (int i = 0; i < count; i++)
		{
			double price1 = h[i];
			int volume1 = v[i];

			double price2 = h[i + 1];
			int volume2 = v[i + 1];

			int biggerVolume = MathMax(volume1, volume2);

			int barTo1 = (int)(barFrom - volume1 * zoom);
			int barTo2 = (int)(barFrom - volume2 * zoom);

			// градиент
			if (useGradient)
			{
				if (VolumeThreshold > 0)
					cl = biggerVolume >= VolumeThreshold ? hgColor2 : hgColor;
				else
					cl = MixColors(hgColor, hgColor2, 1.0 * biggerVolume / max, 16);
			}

			timeFrom = BarTime(barFrom);
			datetime timeTo1 = BarTime(barTo1);
			datetime timeTo2 = BarTime(barTo2);

			if ((barFrom != barTo1) || (barFrom != barTo2))
				drawBar(prefix + IntegerToString(volume1) + "@" + DoubleToStr(price1, Digits), timeTo1, price1, timeTo2, price2, cl, width, STYLE_SOLID, _hgBack, false, 0, _hgUseRectangles);
		}
	}
	else
	{
		bool useThreshold = (VolumeThreshold > 0) && useGradient;

		for (int i = 0; i < count; i++)
		{
			double price = h[i];
			int volume = v[i];

			// выделенные бары рисовать поверх в отдельном цикле
			if (useThreshold && (volume < VolumeThreshold))
				continue;

			// градиент при необходимости
			if (useGradient)
				cl = useThreshold ? hgColor : MixColors(hgColor, hgColor2, 1.0 * volume / max, 16);

			// границы рисования
			timeFrom = BarTime(barFrom);
			int barTo = (int)(barFrom - volume * zoom);
			datetime timeTo = BarTime(barTo);

			// нарисовать бар, нулевой пропустить
			if (barFrom != barTo)
				drawBar(prefix + IntegerToString(volume) + "@" + DoubleToStr(price, Digits), timeFrom, price, timeTo, price, cl, width, STYLE_SOLID, _hgBack, false, 0, _hgUseRectangles);
		}

		// еще раз, только большой объем
		if (useThreshold)
		{
			for (int i = 0; i < count; i++)
			{
				double price = h[i];
				int volume = v[i];

				if (volume >= VolumeThreshold)
				{
					// для большого объема использовать второй цвет
					cl = hgColor2;

					// границы рисования
					timeFrom = BarTime(barFrom);
					int barTo = (int)(barFrom - volume * zoom);
					datetime timeTo = BarTime(barTo);

					// нарисовать бар, нулевой пропустить
					if (barFrom != barTo)
						drawBar(prefix + IntegerToString(volume) + "@" + DoubleToStr(price, Digits), timeFrom, price, timeTo, price, cl, width, STYLE_SOLID, _hgBack, false, 0, _hgUseRectangles);
				}
			}
		}
	}
}

bool initParams()
{
	// Определение кода символа (клиринговый вариант - EC, BP и т.п.).

	_isSymbolFound = false;

	if (AccountServer() == "")
		return(false);

	string sym = Symbol();
	int symType = (int)MarketInfo(sym, MODE_MARGINCALCMODE);

	// Добавьте свою логику	для отсечения префикса и суффикса

	// У CFD многих брокеров символы начинаются с # (#ESH0), и для некоторых символов не указывается начальная Z
	if (StringSubstr(sym, 0, 1) == "#")
	{
		sym = StringSubstr(sym, 1);
		// добавляем Z
		if (StringLen(sym) == 3)
			sym = "Z" + sym;
	}

	// для тех, кто добавляет суффиксы к кроссам, например: EURUSD_FX, EURUSD_SX
	if ((symType == 0) && (StringFind(sym, "_") == 6))
		sym = StringSubstr(sym, 0, 6);

	// forex.com
	if ((StringLen(sym) == 9) && (StringSubstr(sym, 6, 3) == "FXF"))
		sym = StringSubstr(sym, 0, 6);

	int symLength = StringLen(sym);

	if (isFuturesSymbol(sym))
	{
		sym = StringSubstr(sym, 0, symLength - 2);
		symLength = StringLen(sym);
	}

	// The list of the products used by default. During the first run the list will be saved to the separate file,
	// each trading server will have its own file. If the file exists, the products will be taken from it, overwise
	// it will be taken from this list.

	// Список инструментов по умолчанию. При первом запуске будет сохранён в отдельный файл. У каждого торгового
	// сервера будет свой файл. При наличии файла список инструментов берётся из него, иначе из этого массива.

	string DEFAULT_PRODUCTS[] =
		{
			// Columns (separate by spaces or tabs):                 Столбцы (разделяйте пробелами или табуляцией):
			//   Clearing Code (futures)                               Клиринговый код (фьючерсы)
			//   Clearing Code (options, not realized yet)             Клиринговый код (опционы, пока не реализовано)
			//   Exchange                                              Биржа
			//   Scale                                                 Масштаб
			//   Reverse (1 - yes, 0 - no)                             Реверс (1 - да, 0 - нет)
			//   Price step (point)                                    Шаг цены (пункт)
			//   MT symbol names (separate by comma)                   Названия символов в MT (разделяйте запятыми)

			// Forex
			"AD  AD  CME    1     0  0.0001  6A,AUDUSD",         // AUD/USD
			"BP  BP  CME    1     0  0.0001  6B,GBPUSD",         // GBP/USD
			"C1  C1  CME    1     1  0.0001  6C,USDCAD",         // CAD/USD
			"E1  -   CME    1     1  0.0001  6S,USDCHF",         // CHF/USD
			"EC  EC  CME    1     0  0.0001  6E,EURUSD",         // EUR/USD
			"J1  J1  CME    0.01  1  0.01    6J,USDJPY",         // USD/JPY
			"NE  -   CME    1     0  0.0001  6N,NZDUSD",         // NZD/USD
			"RF  -   CME    1     0  0.0001  RF,EURCHF",         // EUR/CHF
			"RP  -   CME    1     0  0.00005 RP,EURGBP",         // EUR/GBP
			"RY  -   CME    1     0  0.01    RY,EURJPY",         // EUR/JPY

			// Indexes
			"ES  ES  CME    1     0  0.25    ES,_SP500",         // E-mini S&P500
			"NQ  NQ  CME    1     0  0.25    NQ",                // E-mini NASDAQ 100
			"YM  -   CBOT   1     0  1.0     YM",                // E-mini Dow ($5)

			// Agriculture
			"48  48  CME    1     0  0.025   LE",                // Live Cattle
			"62  -   CME    1     0  0.025   GF",                // Feeder Cattle
			"LN  LN  CME    1     0  0.025   HE",                // Lean Hog
			"06  06  CBOT   1     0  0.1     ZM",                // Soybean Meal
			"07  07  CBOT   1     0  0.01    ZL",                // Soybean Oil
			"C   C   CBOT   1     0  0.25    ZC",                // Corn
			"S   S   CBOT   1     0  0.25    ZS",                // Soybean
			"W   W   CBOT   1     0  0.25    ZW",                // Wheat

			// Metals
			"GC  OG  NYMEX  1     0  0.1     GC,XAUUSD,GOLD",    // Gold
			"SI  SO  NYMEX  1     0  0.005   SI,XAGUSD,SILVER",  // Silver
			"PL  -   NYMEX  1     0  0.1     PL",                // Platinum
			"HG  -   NYMEX  1     0  0.0005  HG",                // Copper

			// Energy
			"BZ  -   NYMEX  1     0  0.01    BZ",                // Brent Crude Oil
			"CL  LO  NYMEX  1     0  0.01    CL,QM",             // Light Sweet Crude Oil
			"HO  -   NYMEX  1     0  0.0001  HO",                // NY Harbor ULSD
			"NG  -   NYMEX  1     0  0.001   NG",                // Natural Gas
			"RB  -   NYMEX  1     0  0.0001  RB",                // RBOB Gasoline

			""
		};

	string productsFilename = "rvl\\products_" + sanitizeFilename(AccountServer() + ".txt");
	string products[];
	int productCount = UseProductsFile ? ReadStrings(productsFilename, products) : 0;

	if (productCount == 0)
	{
		ArrayCopy(products, DEFAULT_PRODUCTS);
		productCount = ArraySize(products);

		if (UseProductsFile)
			WriteStrings(productsFilename, products);
	}
	else
	{
		Print("Данные по инструментам загружены из файла: " + TerminalInfoString(TERMINAL_DATA_PATH) + "\\MQL4\\Files\\" + productsFilename);
	}

	// найти инструмент
	for (int i = 0; i < productCount; i++)
	{
		string product = products[i];
		StringReplace(product, "\t", " ");
		StringReplace(product, ", ", ",");

		string fields[];

		if (SplitString(product, " ", fields, true) == 7)
		{
			string symbols[];
			int symbolCount = SplitString(Trim(fields[6]), ",", symbols, true);
			_isSymbolFound = StringArrayIndexOf(symbols, sym) != -1;

			if (_isSymbolFound)
			{
				_futCode = Trim(fields[0]);
				_optCode = Trim(fields[1]);
				_exchange = Trim(fields[2]);
				_scale = StrToDouble(Trim(fields[3]));
				_reverse = StrToInteger(Trim(fields[4])) > 0;
				_hgPoint = StrToDouble(Trim(fields[5]));
				break;
			}
		}
	}

	// If you need to define the relation between MT symbol and RVL product, and to do it with the list
	// above is impossible, add corresponding code here, filling the values of _futCode, _optCode, _exchange, _scale,
	// _reverse, _hgPoint variables.
	//
	// Example: define the relation between Gold symbol and GC/NYMEX product. All parameters can be copied from the
	// default list above, for this case we will have:

	// Если необходимо определить собственное соответствие символа MT инструменту сервера RVL, и сделать это с помощью
	// списка выше невозможно, добавьте соответствующий код здесь, заполнив значения переменных _futCode, _optCode,
	// _exchange, _scale, _reverse, _hgPoint.
	//
	// Пример: задать соответствие символу Gold инструменту GC/NYMEX. Все параметры можно скопировать из таблицы
	// соответствия символов, для данного случая получим:

	if (!_isSymbolFound && (_Symbol == "Gold"))
	{
		_isSymbolFound = true;

		_futCode = "GC";
		_optCode = "OG";
		_exchange = "NYMEX";
		_scale = 1;
		_reverse = false;
		_hgPoint = 0.1;
	}

	Print("Инструмент определен как: код фьючерса=", _futCode, ", код опциона=", _optCode, ", биржа=", _exchange,
		", масштаб=", _scale, ", реверс=", _reverse, ", пункт=", _hgPoint, ", тип=", symType);

	return(_isSymbolFound);
}

string dateToCME(datetime date)
{
	// Формат по умолчанию: yyyy.MM.dd
	string s = TimeToStr(date, TIME_DATE);

	return (StringConcatenate(StringSubstr(s, 5, 2), "/", StringSubstr(s, 8, 2), "/", StringSubstr(s, 0, 4)));
}

// Очистить график от своих объектов
int clearChart(string prefix)
{
	int count = 0;

	for (int i = ObjectsTotal() - 1; i >= 0; i--)
	{
		string name = ObjectName(i);

		if (StringFind(name, prefix) == 0)
		{
			ObjectDelete(name);
			count++;
		}
	}

	return(count);
}

// вертикальная линия (время)
void drawVLine(string name, datetime time1, color lineColor = Gray, int width = 1, int style = STYLE_SOLID, bool back = true)
{
	if (ObjectFind(name) >= 0)
		ObjectDelete(name);

	if (ObjectCreate(name, OBJ_VLINE, 0, time1, 0))
	{
		ObjectSet(name, OBJPROP_COLOR, lineColor);
		ObjectSet(name, OBJPROP_BACK, back);
		ObjectSet(name, OBJPROP_STYLE, style);
		ObjectSet(name, OBJPROP_WIDTH, width);
	}
}

// трендовая линия
void drawBar(string name, datetime time1, double price1, datetime timeTo, double price2,
	color lineColor, int width, int style, bool back, bool ray, int window, bool useRectangle)
{
	if (ObjectFind(name) >= 0)
		ObjectDelete(name);

	if (price1 * price2 == 0)
		return;

	if (useRectangle)
		ObjectCreate(name, OBJ_RECTANGLE, window, time1, price1 - _hgPoint / 2.0, timeTo, price2 + _hgPoint / 2.0);
	else
		ObjectCreate(name, OBJ_TREND, window, time1, price1, timeTo, price2);

	ObjectSet(name, OBJPROP_BACK, back);
	ObjectSet(name, OBJPROP_COLOR, lineColor);
	ObjectSet(name, OBJPROP_STYLE, style);
	ObjectSet(name, OBJPROP_WIDTH, width);
	ObjectSet(name, OBJPROP_RAY, ray);

	ObjectSetInteger(0, name, OBJPROP_HIDDEN, true);
	ObjectSetInteger(0, name, OBJPROP_SELECTABLE, false);
}

// горизонтальная линия (уровень)
void drawHLine(string name, double price, color lineColor = Gray, int width = 1, int style = STYLE_SOLID, bool back = true)
{
	if (ObjectFind(name) >= 0)
		ObjectDelete(name);

	if (ObjectCreate(name, OBJ_HLINE, 0, 0, price))
	{
		ObjectSet(name, OBJPROP_COLOR, lineColor);
		ObjectSet(name, OBJPROP_WIDTH, width);
		ObjectSet(name, OBJPROP_STYLE, style);
		ObjectSet(name, OBJPROP_BACK, back);
	}
}

// Отдельная функция для получения параметра графического объекта,
//	т.к. функция ObjectGet недостаточно документирована (не описано возвращаемое значение во всех случаях)
datetime getObjectTime1(string name)
{
 	if (ObjectFind(name) != -1)
		return((datetime)ObjectGet(name, OBJPROP_TIME1));

	return(0);
}

// получить параметры диапазона
bool getRange(datetime timeFrom, datetime timeTo, int& barFrom, int& barTo)
{
	barFrom = TimeBarRight(timeFrom);
	barTo = TimeBarLeft(timeTo);

	if (barFrom < barTo)
		return(false);

	return(true);
}

// Проверить, является ли символ фьючерсом с указанием месяца и года контракта
bool isFuturesSymbol(string sym)
{
	int len = StringLen(sym);

	if (len < 3)
		return(false);

	string yearDigit = StringSubstr(sym, len - 1);

	// последняя цифра должна быть последней цифрой года
	if (StringFind("0123456789", yearDigit) == -1)
		return(false);

	string monthLetter = StringSubstr(sym, len - 2, 1);

	// предпоследний символ должен быть одним из фиксированных для обозначения месяцев контрактов
	if (StringFind("FGHJKMNQUVXZ", monthLetter) == -1)
	{
		// проверить случай с указанием года двумя цифрами

		// обозначение с двухцифровым годом не может быть короче 4 символов
		if (len < 4)
			return(false);

		// предпоследний (второй с конца) символ должен быть цифрой
		if (StringFind("0123456789", monthLetter) == -1)
			return(false);

		// третий с конца - символ месяца
		monthLetter = StringSubstr(sym, len - 3, 1);

		if (StringFind("FGHJKMNQUVXZ", monthLetter) == -1)
			return(false);
	}

	return(true);
}

int downloadStrings(string url, string& strings[])
{
	// Загрузить через WebRequest
	// Пока есть баг в MT, не позволяющий использовать в списке игнорирования порт, этот вариант будет только тормозить работу индикатора.

	/*if (_useWininet == false)
	{
		char data[];
		char result[];
		string resultHeaders;

		if (WebRequest("GET", url, NULL, NULL, 5000, data, 0, result, resultHeaders) >= 0)
			return StringSplit(CharArrayToString(result), '\n', strings);

		// При неудачной попытке загрузки переключиться на работу через wininet.dll
		_useWininet = true;
		Print("RVL has switched to wininet.dll.");
	}*/

	// Загрузить через wininet.dll

	if (!InetIsReady())
		if (!InetOpen())
			return(0);

	return InetDownloadStrings(url, strings);
}

string sanitizeFilename(string filename)
{
	StringReplace(filename, "\\", "_");
	StringReplace(filename, "/", "_");
	StringReplace(filename, ":", "_");
	StringReplace(filename, "*", "_");
	StringReplace(filename, "?", "_");
	StringReplace(filename, "\"", "_");
	StringReplace(filename, "<", "_");
	StringReplace(filename, ">", "_");
	StringReplace(filename, "|", "_");

	return(filename);
}

#import "wininet.dll"
int InternetOpenW(string &lpszAgent, int dwAccessType, string &lpszProxyName, string &lpszProxyBypass, int dwFlags);
int InternetOpenUrlW(int hInternet, string &lpszUrl, string &lpszHeaders, int dwHeadersLength, int dwFlags, int dwContext);
int InternetReadFile(int hFile, uchar &lpBuffer[], int dwNumberOfBytesToRead, int &lpdwNumberOfBytesRead);
int InternetCloseHandle(int hInternet);
#import

int inet__sessionHandle = 0;

int inet__loadPause = 0;

// Из-за ограничений на длину переменной используем сокращения в названиях констант

// IOT=INTERNET_OPEN_TYPE
#define INET__IOT_PRECONFIG 0 // use registry configuration
#define INET__IOT_DIRECT 1    // direct to net
#define INET__IOT_PROXY 3     // via named proxy

// IF=INTERNET_FLAG
#define INET__IF_NO_CACHE_WRITE 1024
#define INET__IF_PRAGMA_NOCACHE 16384
#define INET__IF_RELOAD 65536

void InetClose()
{
	if (inet__sessionHandle > 0)
	{
		InternetCloseHandle(inet__sessionHandle);
		inet__sessionHandle = 0;
	}
}

bool InetOpen(string userAgent = "Mozilla/4.0 (MSIE 6.0; Windows NT 5.1)")
{
    string proxyName = "";
    string proxyBypass = "";
	inet__sessionHandle = InternetOpenW(userAgent, INET__IOT_DIRECT, proxyName, proxyBypass, 0);
	return(inet__sessionHandle > 0);
}

bool InetIsReady()
{
	return(inet__sessionHandle > 0);
}

int InetDownloadStrings(string url, string& strings[])
{
	// проверить возможность оиспользования DLL
	if(!IsDllsAllowed())
		return(0);

	if (!InetIsReady())
		return(0);

	// Несмотря на указанные параметры, в W7+IE9 (остальное не тестировалось) все равно
	// используется кэш IE, при этом запросы выполняются очень медленно
	string headers = "";
	int hURL = InternetOpenUrlW(inet__sessionHandle, url, headers, 0, INET__IF_NO_CACHE_WRITE | INET__IF_PRAGMA_NOCACHE | INET__IF_RELOAD, 0);

	if (hURL <= 0)
		return(0);

	// 255 байт - буфер для приема данных
	uchar buffer[255];
	int bytesRead = 0;
	int bufferSize = 255;

	// потоковое чтение и преобразование в массив строк
	int count = 0;
	int ac = ArrayResize(strings, 100);
	string s = ""; // потоковые данные
	int c = 0; // размер потока
	int j = 0; // индекс потока

	while (!IsStopped())
	{
		c = StringLen(s);

		if ((c == 0) || (j == c))
		{
			// подгружаем строку
			int res = InternetReadFile(hURL, buffer, bufferSize, bytesRead);

			// если последнего куска нет, значит предыдущий оканчивался ровно в конце потока
			if (bytesRead == 0)
				break;

			// добавить кусок в конец строки
            if (!StringAdd(s, CharArrayToString(buffer, 0, bytesRead)))
                return(0);

			c = StringLen(s);
		}

		// конец потока и данных
		if ((c == 0) || (j == c))
			break;

		// продолжаем разбор строки
		if (StringGetChar(s, j) == 10)
		{
			count++;

			if (ac < count)
				ac = ArrayResize(strings, ac + 100);

			strings[count - 1] = StringSubstr(s, 0, j);

			s = StringSubstr(s, j + 1);
			j = 0;
		}

		j++;

		// Пауза между приемами блоков, без нее на некоторых системах бывают ошибки при загрузке
		if (inet__loadPause > 0)
			Sleep(inet__loadPause);
	}

	// последняя строка
	if (s != "")
	{
		count++;

		if (count != ac)
			ArrayResize(strings, count);

		strings[count-1] = s;
	}
	else
	{
		ArrayResize(strings, count);
	}

	return(count);
}

int IntPutInRange(int value, int from, int to)
{
	if (to >= from)
	{
		if (value > to)
			value = to;
		else if (value < from)
			value = from;
	}

	return(value);
}

double DoublePutInRange(double value, double from, double to)
{
	if (to >= from)
	{
		if (value > to)
			value = to;
		else if (value < from)
			value = from;
	}

	return(value);
}

double Round(double value, double error)
{
	return(MathRound(value / error) * error);
}

int StringArrayIndexOf(string& array[], string value, int startingFrom = 0)
{
	int index = - 1;
	int count = ArraySize(array);

	for (int i = startingFrom; i < count; i++)
	{
		if (array[i] == value)
		{
			index = i;
			break;
		}
	}

	return(index);
}

// названия таймеров
string timer__name[];

// периоды таймеров в секундах
int timer__seconds[];

// последнее время проверки таймеров
datetime timer__lastTime[];

// количество таймеров
int timer__count = 0;

void TimerInit()
{
	timer__count = 0;
}

void TimerSet(string name, int seconds)
{
	int ti = StringArrayIndexOf(timer__name, name);

	if (ti >= 0)
	{
		// таймер с таким именем уже есть, переустановить
		timer__seconds[ti] = seconds;

		if (IsTesting() || IsOptimization())
			timer__lastTime[ti] = TimeCurrent();
		else
			timer__lastTime[ti] = TimeLocal();
	}
	else
	{
		// добавить новый таймер
		timer__count++;
		ArrayResize(timer__name, timer__count);
		ArrayResize(timer__seconds, timer__count);
		ArrayResize(timer__lastTime, timer__count);
		timer__name[timer__count-1] = name;
		timer__seconds[timer__count-1] = seconds;
		timer__lastTime[timer__count-1] = 0;//TimeCurrent();
	}
}

bool TimerCheck(string name)
{
	bool wait = true;

	int i = StringArrayIndexOf(timer__name, name);

	if (i >= 0)
	{
		// проверить ожидание
		datetime now;

		if (IsTesting() || IsOptimization())
			now = TimeCurrent();
		else
			now = TimeLocal();

		wait = now - timer__lastTime[i] < timer__seconds[i];

		// сбрасываем таймер
		if (!wait)
			timer__lastTime[i] = now;
	}

	return(!wait);
}

datetime BarTime(int bar, int period = 0)
{
	if (period == 0)
		period = Period();

	if (bar >= 0)
		return(iTime(Symbol(), period, bar));
	else
		return(iTime(Symbol(), period, 0) - bar * period * 60);
}

int TimeBarRight(datetime time, int period = 0)
{
	if (period == 0)
		period = Period();

	int bar = iBarShift(Symbol(), period, time);
	datetime t = iTime(Symbol(), period, bar);

	if (t != time && bar == 0)
	{
		// время за пределами диапазона
		bar = (int)((iTime(Symbol(), period, 0) - time) / 60 / period);
	}
	else
	{
		// проверить, чтобы бар был не слева по времени
		if (t < time)
			bar--;
	}

	return(bar);
}

int TimeBarLeft(datetime time, int period = 0)
{
	if (period == 0)
		period = Period();

	int bar = iBarShift(Symbol(), period, time);
	datetime t = iTime(Symbol(), period, bar);

	if (t != time && bar == 0)
	{
		// время за пределами диапазона
		bar = (int)((iTime(Symbol(), period, 0) - time) / 60 / period);
	}
	else
	{
		// проверить, чтобы бар был не справа по времени
		if (t > time)
			bar++;
	}

	return(bar);
}

#import "gdi32.dll"
uint GetPixel(int hDC, int x, int y);
#import

bool ColorToRGB(color c, int& r, int& g, int& b)
{
	// Если цвет задан неверный, либо задан как отсутствующий, вернуть false
	if ((c >> 24) > 0)
	{
		r = 255;
		g = 255;
		b = 255;
		return(false);
	}

	// 0x00BBGGRR
	b = (c & 0xFF0000) >> 16;
	g = (c & 0x00FF00) >> 8;
	r = (c & 0x0000FF);

	return(true);
}

color RGBToColor(int r, int g, int b)
{
	// 0x00BBGGRR
	return((color)(
	    ((b & 0x0000FF) << 16) + ((g & 0x0000FF) << 8) + (r & 0x0000FF)
	    ));
}

color MixColors(color color1, color color2, double mix, double step = 16)
{
	// Коррекция параметров
	step = DoublePutInRange(step, 1.0, 255.0);
	mix = DoublePutInRange(mix, 0.0, 1.0);

	int r1, g1, b1;
	int r2, g2, b2;

	// Разбить на компоненты
	ColorToRGB(color1, r1, g1, b1);
	ColorToRGB(color2, r2, g2, b2);

	// вычислить
	int r = IntPutInRange((int)(MathRound((r1 + mix * (r2 - r1)) / step) * step), 0, 255);
	int g = IntPutInRange((int)(MathRound((g1 + mix * (g2 - g1)) / step) * step), 0, 255);
	int b = IntPutInRange((int)(MathRound((b1 + mix * (b2 - b1)) / step) * step), 0, 255);

	return(RGBToColor(r, g, b));
}

bool ColorIsNone(color c)
{
	return((c >> 24) > 0);
}

string ReplaceString(string inputString, string from, string to)
{
	string result = "";
	int inputLength = StringLen(inputString);
	int fromLength = StringLen(from);

	for (int i = 0; i < inputLength; i++)
	{
		string s = StringSubstr(inputString, i, fromLength);

		if (s == from)
		{
  			result = StringConcatenate(result, to);
  			i += fromLength - 1;
		}
		else
		{
			result = StringConcatenate(result, StringSubstr(inputString, i, 1));
		}
	}

	return(result);
}

string TrimLeft(string s, int ch = 0)
{
	if (ch == 0)
	{
		s = StringTrimLeft(s);
	}
	else
	{
		int len = StringLen(s);

		// слева
		int cut = 0;

		for (int i = 0; i < len; i++)
		{
			if (StringGetChar(s, i) == ch)
				cut++;
			else
				break;
		}

		if (cut > 0)
		{
			if (cut > len - 1)
				s = "";
			else
				s = StringSubstr(s, cut);
		}
	}
	return(s);
}

string TrimRight(string s, int ch = 0)
{
	if (ch == 0)
	{
		s = StringTrimRight(s);
	}
	else
	{
		int len = StringLen(s);

		// справа
		int cut = len;

		for (int i = len - 1; i >= 0; i--)
		{
			if (StringGetChar(s, i) == ch)
				cut--;
			else
				break;
		}

		if (cut != len)
		{
			if (cut == 0)
				s = "";
			else
				s = StringSubstr(s, 0, cut);
		}
	}
	return(s);
}

string Trim(string s, int ch = 0)
{
	if (ch == 0)
		return(StringTrimLeft(StringTrimRight(s)));
	else
		return(TrimLeft(TrimRight(s, ch), ch));
}

int SplitString(string s, string sep, string& parts[], bool removeEmpty = false)
{
	int count = 0;
	int sepLen = StringLen(sep);

	string part;
	while (true)
	{
		int p = StringFind(s, sep);
		if (p >= 0)
		{
			if (p == 0)
				part = "";
			else
				part = StringSubstr(s, 0, p);

			if (!removeEmpty || (Trim(part) != ""))
			{
				count++;
				ArrayResize(parts, count);
				parts[count - 1] = part;
			}

			s = StringSubstr(s, p + sepLen);
		}
		else
		{
			// Последний кусок
			if (!removeEmpty || (Trim(s) != ""))
			{
				count++;
				ArrayResize(parts, count);
				parts[count - 1] = s;
			}

			break;
		}
	}

	// удалить последнюю пустую строку
	if ((count > 0) && (parts[count - 1] == ""))
	{
		count--;
		ArrayResize(parts, count);
	}

	return(count);
}

#define DATETIME__SECONDS_IN_DAY 86400

datetime BeginOfDay(datetime time = EMPTY)
{
	if (time == EMPTY)
		time = TimeCurrent();

	return(time - (time % DATETIME__SECONDS_IN_DAY));
}

datetime BeginOfWeek(datetime time = EMPTY)
{
	if (time == EMPTY)
		time = TimeCurrent();

	time -= time % DATETIME__SECONDS_IN_DAY;

	int dow = TimeDayOfWeek(time);

	if (dow ==0)
		time -= 6 * DATETIME__SECONDS_IN_DAY;
	else
		time -= (dow - 1) * DATETIME__SECONDS_IN_DAY;

	return(time);
}

datetime TimeAddWorkDays(datetime time, int days)
{
	int SECONDS_IN_TWO_DAYS = 2 * DATETIME__SECONDS_IN_DAY;

	int i;
	int dayOfWeek;

	if (days > 0)
	{
		for (i = 1; i <= days; i++)
		{
			time += DATETIME__SECONDS_IN_DAY;
			dayOfWeek = TimeDayOfWeek(time);

			// Воскресенье сдвигаем на понедельник, субботу сдвигаем на понедельник
			if (dayOfWeek == 0)
				time += DATETIME__SECONDS_IN_DAY;
			else if (dayOfWeek == 6)
				time += SECONDS_IN_TWO_DAYS;
		}
	}
	else
	{
		for (i = 1; i <= -days; i++)
		{
			if (time < DATETIME__SECONDS_IN_DAY)
				return(0);

			time -= DATETIME__SECONDS_IN_DAY;
			dayOfWeek = TimeDayOfWeek(time);

			// Воскресенье сдвигаем на пятницу, субботу сдвигаем на пятницу
			if (dayOfWeek == 0)
			{
				if (time < SECONDS_IN_TWO_DAYS)
					return(0);

				time -= SECONDS_IN_TWO_DAYS;
			}
			else if (dayOfWeek == 6)
			{
				if (time < DATETIME__SECONDS_IN_DAY)
					return(0);

				time -= DATETIME__SECONDS_IN_DAY;
			}
		}
	}

	return(time);
}

datetime TimeAddWeeks(datetime time, int weeks)
{
	return(time + 7 * DATETIME__SECONDS_IN_DAY * weeks);
}

#define ERR_END_OF_FILE 4099

int ReadStrings(string path, string& strings[])
{
	int count = 0;
	int arraySize = 0;

	int h = FileOpen(path, FILE_CSV | FILE_READ);

	if (h >= 0)
	{
		arraySize = ArrayResize(strings, (int)(FileSize(h) / 10 + 1));

		while (!FileIsEnding(h))
		{
			string line = FileReadString(h);

			count++;

			// добавить сразу много строк, чтобы не повторять на каждой итерации
			if (count > arraySize)
				arraySize = ArrayResize(strings, arraySize + 100);

			// в случае неудачи остановить загрузку
			if (arraySize == -1)
			{
				count = 0;
				break;
			}

			strings[count - 1] = line;
		}

		FileClose(h);
	}

	ArrayResize(strings, count);

	return(count);
}

bool WriteStrings(string path, string& strings[])
{
	int h = FileOpen(path, FILE_CSV | FILE_WRITE);

	if (h >= 0)
	{
		bool writeError = false;
		int c = ArraySize(strings);

		for (int i = 0; i < c; i++)
		{
			if (FileWrite(h, strings[i]) <= 0)
			{
				writeError = true;
				break;
			}
		}

		FileClose(h);

		return(!writeError);
	}
	else
	{
		return(false);
	}
}

// 2014-09-14 15:48:35 UTC
// MQLMake 1.22. Copyright © FXcoder, 2011-2014. http://fxcoder.net