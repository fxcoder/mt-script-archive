#property copyright "Index v8.3. © FXcoder"
#property link      "http://fxcoder.blogspot.com"
#property strict

#property indicator_separate_window
#property indicator_buffers 1
#property indicator_plots   1

#property indicator_label1  "Index"
#property indicator_type1   DRAW_LINE
#property indicator_color1  clrRed
#property indicator_style1  STYLE_SOLID
#property indicator_width1  1

enum ENUM_DIR
{
	DIR_DIRECT = 1,   // Direct
	DIR_REVERSE = -1  // Reverse
};

struct IndexElement
{
	string Sym;    // Имя символа (инструмент, индекс и т.п.)
	ENUM_DIR Dir;  // Направление учёта (+1 - обычное, -1 - реверсивное)
	bool IsIndex;  // Является ли элемент индексом
};

// Формула (только умножение и деление)
input string Formula = "USD";                                       // Formula (* and / only)

// Степень, в которую возводится вся формула
input double Power = 1.0;                                           // Power

// Масштаб
input double Scale = 1.0;                                           // Scale

// Тип цены. При != Close может проявляться разная специфика
input ENUM_APPLIED_PRICE AppliedPrice = PRICE_CLOSE;                // Applied price

// Средняя цен, используемых для вычисления
input int MAPeriod = 0;                                             // MA period

// Средняя, которая будет вычитаться из результата
input int DiffMAPeriod = 0;                                         // Diff MA period

// Средняя цен, используемых для вычисления
input ENUM_MA_METHOD MAMethod = MODE_SMA;                           // MA method

// Параметры для расчета индексов кроме названия индекса и типа цены
input string IndexCalcCurrs = "AUD,CAD,CHF,EUR,GBP,JPY,NZD,USD";    // Index calculation currency set

// Логирифмировать результат и умножить на 100 (для быстрой и грубой оценки процентного изменения)
input bool LogScale = false;                                        // Logarithmic scale

// Максимальное число баров
input int MaxBars = 1000;                                           // Bar count limit

// Базовая валюта для получения котировок при расчете индексов
// (кроссы будут рассчитаны через кроссы этой валюты)
string IndexBaseCurr = "USD";

int OnInit()
{

	_maPeriod = MAPeriod > 1 ? MAPeriod : 0;
	_diffMaPeriod = DiffMAPeriod > 1 ? DiffMAPeriod : 0;

	// Разбор формулы
	ArrayFree(_elements);

	string f = UpperString(Trim(Formula));

	// Если ничего не указано, показать текущий символ
	if (f == "")
		f = _Symbol;

	// Реверс инструмента определяется знаком перед ним.
	// Если ничего нет, значит нужно брать в прямом порядке, добавлям * для упрощения расчета.
	ushort ch = StringGetCharacter(f, 0);

	if ((ch != '*') && (ch != '/'))
		f = "*" + f;

	f = f + "*";

	// Расшифровка специальных обозначений
	f = ReplaceString(f, "[0]", Symbol());

	if (StringLen(Symbol()) == 6)
	{
		f = ReplaceString(f, "[1]", StringSubstr(Symbol(), 0, 3));
		f = ReplaceString(f, "[2]", StringSubstr(Symbol(), 3, 3));
	}

	// Найти все индексы, добавить их в списки индексов _indexes и валют для расчета _indexCalcCurrs.
	// Выполнить в два прохода: сначала определить список валют для рассчета и только затем создать
	// экземпляры индикаторов на основе этого набора.

	// Расшифровка формулы (разложение на элементы)
	string sym = "";
	ENUM_DIR dir = DIR_DIRECT;

	for (int i = 0, len = StringLen(f); i < len; i++)
	{
		ch = StringGetCharacter(f, i);

		// начало/конец инструмента/индекса
		if ((ch == '*') || (ch == '/'))
		{
			// Конец символа? Записать. Иначе это начало формулы, ничего не делать.
			if (sym != "")
			{
				bool isIndex = IsIndex(sym);
				ArrayAdd(_elements, isIndex ? TrimLeft(sym, '!') : sym, dir, isIndex);
				sym = "";
			}

			//  следующее направление
			dir = (ch == '*') ? DIR_DIRECT : DIR_REVERSE;
		}
		else
		{
			sym = sym + CharToString((uchar)ch);
		}
	}

	// получить единый список валют _indexCalcCurrs для расчёта всех индексов формулы
	CalcIndexCalcCurrs(IndexCalcCurrs, _indexCalcCurrs);

	// Создать набор индексов и машек инструментов, используемых в формуле, если необходимо.
	_mas = new MaPool(AppliedPrice, MAMethod);
	_indexes = new IndexPool(AppliedPrice, MAMethod, _indexCalcCurrs);

	for (int i = 0, count = ArraySize(_elements); i < count; i++)
	{
		sym = _elements[i].Sym;
		ENUM_APPLIED_PRICE appliedPrice = ReverseAppliedPrice(AppliedPrice, _elements[i].Dir == DIR_REVERSE);

		if (IsIndex(sym))
		{
			_indexes.Get(sym, _maPeriod, appliedPrice);

			if (_diffMaPeriod > 0)
				_indexes.Get(sym, _diffMaPeriod, appliedPrice);
		}
		else
		{
			if (_maPeriod > 0)
				_mas.Get(sym, _maPeriod, appliedPrice);

			if (_diffMaPeriod > 0)
				_mas.Get(sym, _diffMaPeriod, appliedPrice);
		}
	}

	string name = MQLInfoString(MQL_PROGRAM_NAME) + ": " +
		(LogScale ? "100*ln(" : "") +
		(Power != 1 ? "(" : "") +
		(Scale != 1 ? DoubleToCompactString(Scale) + "*" : "") + Trim(f, '*') +
		(Power != 1 ? ")^" + DoubleToCompactString(Power) : "") +
		(LogScale ? ")" : "") +
		"," + AppliedPriceToString(AppliedPrice);

	IndicatorSetString(INDICATOR_SHORTNAME, name);
	PlotIndexSetString(0, PLOT_LABEL, name);

	UpdateDrawBegin();

	SetIndexBuffer(0, X, INDICATOR_DATA);
	PlotIndexSetDouble(0, PLOT_EMPTY_VALUE, EMPTY_VALUE);

	ArraySetAsSeries(X, false);

	return(0);
}

int OnCalculate(const int ratesTotal, const int prevCalculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{
	// Работать от конца. Индекс правого бара буфера линии = 0

	// Количество баров (справа), которые нужно пересчитать
	int barsToCalc = ratesTotal - prevCalculated;

	if (barsToCalc <= 0)
		barsToCalc = 1;

	if (MaxBars > 0)
		barsToCalc = MathMin(barsToCalc, MaxBars);

	// Количество рассчитанных баров
	int calculatedBars = ratesTotal - barsToCalc;

	double x;
	double v = EMPTY_VALUE;
	int bar;
	bool err = false;

	// Перебрать бары слева направо
	int elementsCount = ArraySize(_elements);

	for (int i = barsToCalc - 1; i >= 0; i--)
	{
		bar = ratesTotal - 1 - i;
		x = 1;
		err = false;

		for (int k = 0; k < elementsCount; k++)
		{
			v = GetValue(_elements[k], i);

			if ((v == EMPTY_VALUE) || ((v == 0) && (_elements[k].Dir == DIR_REVERSE)))
			{
				err = true;
				break;
			}

			if (_elements[k].Dir == DIR_DIRECT)
				x *= v;
			else
				x /= v;
		}

		// остановить расчеты при ошибке
		if (err)
		{
			X[bar] = EMPTY_VALUE;
			continue;
		}

		// степень
		x = Scale * MathPow(x, Power);

		// логарифмический масштаб
		if (LogScale && (x > 0))
			x = MathLog(x) * 100.0;

		calculatedBars++;
		X[bar] = x;
	}

	UpdateDrawBegin(ratesTotal);

	return(calculatedBars);
}

void OnDeinit(const int reason)
{
	delete(_mas);
	delete(_indexes);
}

void UpdateDrawBegin()
{
	UpdateDrawBegin(Bars(_Symbol, PERIOD_CURRENT));
}

void UpdateDrawBegin(const int ratesTotal)
{
	int drawBegin = (int)MathMax(_maPeriod, _diffMaPeriod);

	if ((MaxBars > 0) && (ratesTotal >= MaxBars))
	{
		if (ratesTotal - MaxBars > drawBegin)
			drawBegin = ratesTotal - MaxBars;
	}

	PlotIndexSetInteger(0, PLOT_DRAW_BEGIN, drawBegin);
}

// Проверяет, является ли символ индексом. Параметр "sym" должен быть в верхнем регистре
bool IsIndex(const string &sym)
{
	int len = StringLen(sym);

	if (len <= 0)
		return(false);

	ushort char0 = StringGetCharacter(sym, 0);

	// индекс валюты
	if ((len == 3) && (char0 >= 'A') && (char0 <= 'Z'))
		return(true);

	// прочий индекс
	return(char0 == '!');
}

//double GetValue(string sym, const ENUM_DIR dir, const int bar)
double GetValue(const IndexElement &element, const int bar)
{
	string sym = element.Sym;

	double v = EMPTY_VALUE;
	ENUM_APPLIED_PRICE appliedPrice = ReverseAppliedPrice(AppliedPrice, element.Dir == DIR_REVERSE);

	if (element.IsIndex)
	{
		// убрать маркер индекса, если он есть
		if (StringGetCharacter(sym, 0) == '!')
			sym = StringSubstr(sym, 1);

		v = _indexes.Get(sym, _maPeriod, appliedPrice).GetValue(bar);

		if (v == EMPTY_VALUE)
			return(v);

		if (_diffMaPeriod > 0)
		{
			double d = _indexes.Get(sym, _diffMaPeriod, appliedPrice).GetValue(bar);
			v = (d > 0) ? v / d : EMPTY_VALUE;
		}
	}
	else
	{
		if (_maPeriod == 0)
			v = GetPrice(sym, appliedPrice, bar);
		else
			v = _mas.Get(sym, _maPeriod, appliedPrice).GetValue(bar);

		if (v == EMPTY_VALUE)
			return(v);

		if (_diffMaPeriod > 0)
		{
			double d = _mas.Get(sym, _diffMaPeriod, appliedPrice).GetValue(bar);
			v = (d > 0) ? v / d : EMPTY_VALUE;
		}
	}

	return(v);
}

void CalcIndexCalcCurrs(const string indexCalcCurrs, string &calcCurrs[])
{
	ArrayFree(calcCurrs);

	// Сначала добавляем все валюты из списка в параметрах
	SplitString(indexCalcCurrs, ",", calcCurrs, true, true);

	// Потом добавляем все названия индексов
	for (int i = 0, count = ArraySize(_elements); i < count; i++)
	{
		if (_elements[i].IsIndex)
			ArrayAdd(calcCurrs, _elements[i].Sym, true);
	}
}

// Получить цену символа для указанного бара символа графика
double GetPrice(const string sym, ENUM_APPLIED_PRICE appliedPrice, const int bar)
{
	// Определить время бара для символа графика
	datetime times[];

	if (CopyTime(_Symbol, PERIOD_CURRENT, bar, 1, times) != 1)
		return(EMPTY_VALUE);

	MqlRates rates[];

	if (CopyRates(sym, PERIOD_CURRENT, times[0], 1, rates) != 1)
		return(EMPTY_VALUE);

	MqlRates rate = rates[0];

	switch(appliedPrice)
	{
		case PRICE_CLOSE:
			return(rate.close);

		case PRICE_OPEN:
			return(rate.open);

		case PRICE_HIGH:
			return(rate.high);

		case PRICE_LOW:
			return(rate.low);

		case PRICE_MEDIAN:
			return((rate.high + rate.low) / 2.0);

		case PRICE_TYPICAL:
			return((rate.high + rate.low + rate.close) / 3.0);

		case PRICE_WEIGHTED:
			return((rate.high + rate.low + 2.0 * rate.close) / 4.0);
	}

	return(EMPTY_VALUE);
}

int ArrayGroup(const string &src[], string &dest[])
{
	int count[];
	int destCount = ArrayGroupCount(src, dest, count);
	return (destCount);
}

int ArrayGroupCount(const string &src[], string &dest[], int &count[])
{
	int srcCount = ArraySize(src);

	// Сброс результата
	int destCount = 0;
	ArrayFree(dest);
	ArrayFree(count);

	for (int i = 0; i < srcCount; i++)
	{
		int si = ArrayIndexOf(dest, src[i]);

		if (si >= 0)
		{
			// увеличить счетчик вхождений
			count[si]++;
		}
		else
		{
			// добавить новый элемент в результат
			destCount++;
			ArrayResize(dest, destCount, 100);
			ArrayResize(count, destCount, 100);
			dest[destCount - 1] = src[i];
			count[destCount - 1] = 1;
		}
	}

	return (destCount);
}

template <typename T>
int ArrayIndexOf(const T &arr[], const T value, const int startingFrom = 0)
{
	int count = ArraySize(arr);

	for (int i = startingFrom; i < count; i++)
	{
		if (arr[i] == value)
			return(i);
	}

	return(-1);
}

template <typename T>
int ArrayAdd(T &arr[], T value, const bool checkUnique = false, const int reserveSize = 100)
{
	int i;

	// Найти существующий, если необходимо
	if (checkUnique)
	{
		i = ArrayIndexOf(arr, value);

		if (i != -1)
			return(i);
	}

	// Добавить новый элемент в массив
	i = ArrayResize(arr, ArraySize(arr) + 1, reserveSize);

	if (i == -1)
		return(-1);

	i--;
	arr[i] = value;

	return(i);
}

string ReplaceString(string inputString, const string from, const string to)
{
	// Используем стандартную функцию
	StringReplace(inputString, from, to);
	return(inputString);
}

string TrimLeft(string s, const ushort ch)
{
	int len = StringLen(s);

	// Найти конец вырезаемого от начала участка
	int cut = 0;

	for (int i = 0; i < len; i++)
	{
		if (StringGetCharacter(s, i) == ch)
			cut++;
		else
			break;
	}

	// Отрезать
	if (cut > 0)
	{
		if (cut > len - 1)
			s = "";
		else
			s = StringSubstr(s, cut);
	}

	return(s);
}

string TrimRight(string s, const ushort ch)
{
	int len = StringLen(s);

	// Найти начало вырезаемого до конца участка
	int cut = len;

	for (int i = len - 1; i >= 0; i--)
	{
		if (StringGetCharacter(s, i) == ch)
			cut--;
		else
			break;
	}

	if (cut != len)
	{
		if (cut == 0)
			s = "";
		else
			s = StringSubstr(s, 0, cut);
	}

	return(s);
}

string Trim(string s)
{
	s = StringTrimRight(s);
	s = StringTrimLeft(s);

	return(s);
}

string Trim(const string s, const ushort ch)
{
	return(TrimLeft(TrimRight(s, ch), ch));
}

int SplitString(string s, const string sep, string &parts[], const bool removeEmpty = false, const bool unique = false)
{
	int count = 0;
	int sepLen = StringLen(sep);
	ArrayFree(parts);

	string part;

	while (true)
	{
		int p = StringFind(s, sep);

		if (p >= 0)
		{
			if (p == 0)
				part = "";
			else
				part = StringSubstr(s, 0, p);

			if (!removeEmpty || (Trim(part) != ""))
			{
				ArrayAdd(parts, part, unique);
				count = ArraySize(parts);
			}

			s = StringSubstr(s, p + sepLen);
		}
		else
		{
			// Последний кусок
			if (!removeEmpty || (Trim(s) != ""))
			{
				ArrayAdd(parts, s, unique);
				count = ArraySize(parts);
			}

			break;
		}
	}

	// удалить последнюю пустую строку
	if ((count > 0) && (parts[count - 1] == ""))
	{
		count--;

		if (count > 0)
			ArrayResize(parts, count);
	}

	return(count);
}

string UpperString(string s)
{
	StringToUpper(s);
	return(s);
}

string DoubleToString(const double d, const uint digits, const uchar separator)
{
	string s = DoubleToString(d, digits) + ""; //HACK: без +"" функция может вернуть пустое значение (билд 697)

	if (separator != '.')
	{
		int p = StringFind(s, ".");

		if (p != -1)
			StringSetCharacter(s, p, separator);
	}

	return(s);
}

string DoubleToCompactString(const double d, const uint digits = 8, const uchar separator = '.')
{
	string s = DoubleToString(d, digits, separator);

	// убрать нули в конце дробной части
	if (StringFind(s, CharToString(separator)) != -1)
	{
		s = TrimRight(s, '0');
		s = TrimRight(s, '.');
	}

	return(s);
}

string AppliedPriceToString(const ENUM_APPLIED_PRICE ap, const bool verbose = false)
{
	if (verbose)
	{
		switch (ap)
		{
			case PRICE_CLOSE: return("Close");
			case PRICE_OPEN: return("Open");
			case PRICE_HIGH: return("High");
			case PRICE_LOW: return("Low");
			case PRICE_MEDIAN: return("Median");
			case PRICE_TYPICAL: return("Typical");
			case PRICE_WEIGHTED: return("Weighted");
			default: return("Unknown");
		}
	}
	else
	{
		switch (ap)
		{
			case PRICE_CLOSE: return("C");
			case PRICE_OPEN: return("O");
			case PRICE_HIGH: return("H");
			case PRICE_LOW: return("L");
			case PRICE_MEDIAN: return("HL");
			case PRICE_TYPICAL: return("HLC");
			case PRICE_WEIGHTED: return("HLCC");
			default: return("?");
		}
	}
}

double IndicatorValue(const double value, const double emptyValue = EMPTY_VALUE)
{
	if (value == 0)
		return(emptyValue);

	return(value);
}

class Ma
{
	private: string _symbol;
	public: string GetSymbol() const { return(_symbol); }

	// параметры средних и цен
	private: uint _maPeriod;
	public: uint GetMaPeriod() const { return(_maPeriod); }

	private: ENUM_MA_METHOD _maMethod;
	public: ENUM_MA_METHOD GetMaMethod() const { return(_maMethod); }

	private: ENUM_APPLIED_PRICE _appliedPrice;
	public: ENUM_APPLIED_PRICE GetAppliedPrice() const { return(_appliedPrice); }

	private: int _maHandler;
	public: int GetMaHandler() const { return(_maHandler); }

	private: bool _isInitialized;
	public: bool IsInitialized() const { return(_isInitialized); }

	public: void Ma(const string symbol, const ENUM_APPLIED_PRICE appliedPrice, const uint maPeriod, const ENUM_MA_METHOD maMethod)
	{
		_symbol = symbol;
		_maPeriod = maPeriod;
		_appliedPrice = appliedPrice;
		_maMethod = maMethod;

		Init();
	}

	public: bool Init()
	{
		_isInitialized = true;

		return(_isInitialized);
	}

	public: bool CheckInitialized()
	{
		if (_isInitialized)
			return(true);

		return(Init());
	}

	public: double GetValue(const int bar, const double emptyValue = EMPTY_VALUE)
	{
		if (!CheckInitialized())
			return(emptyValue);

		return(IndicatorValue(iMA(_symbol, PERIOD_CURRENT, _maPeriod, 0, _maMethod, _appliedPrice, bar), emptyValue));

	}

};

class MaPool
{
	private: Ma *_mas[];

	// default applied price
	private: ENUM_APPLIED_PRICE _appliedPrice;

	// default MA mathod
	private: ENUM_MA_METHOD _maMethod;

	public: void MaPool(const ENUM_APPLIED_PRICE appliedPrice, const ENUM_MA_METHOD maMethod)
	{
		ArrayFree(_mas);
		_appliedPrice = appliedPrice;
		_maMethod = maMethod;
	}

	public: void ~MaPool()
	{
		for (int i = 0, count = Count(); i < count; i++)
			delete(_mas[i]);
	}

	public: int Count()
	{
		return(ArraySize(_mas));
	}

	public: Ma *Get(const string symbol, const uint maPeriod, const ENUM_APPLIED_PRICE appliedPrice)
	{
		Ma *ma = find(symbol, maPeriod, appliedPrice);

		if (CheckPointer(ma) != POINTER_INVALID)
			return(ma);

		int newIndex = Count();
		ArrayResize(_mas, newIndex + 1);
		ma = new Ma(symbol, appliedPrice, maPeriod, _maMethod);
		_mas[newIndex] = ma;

		return(ma);
	}

	private: Ma *find(const string symbol, const uint maPeriod, const ENUM_APPLIED_PRICE appliedPrice)
	{
		for (int i = 0, count = Count(); i < count; i++)
		{
			Ma *ma = _mas[i];

			if ((ma.GetSymbol() == symbol) && (ma.GetMaPeriod() == maPeriod) && (ma.GetAppliedPrice() == appliedPrice))
				return(_mas[i]);
		}

		return(NULL);
	}

};

ENUM_APPLIED_PRICE ReverseAppliedPrice(const ENUM_APPLIED_PRICE appliedPrice, const bool reverse = true)
{
	if (!reverse)
		return(appliedPrice);

	if (appliedPrice == PRICE_HIGH)
		return(PRICE_LOW);

	if (appliedPrice == PRICE_LOW)
		return(PRICE_HIGH);

	return(appliedPrice);
}

int ArrayAdd(IndexElement &arr[], const string sym, const ENUM_DIR dir, const bool isIndex, const bool checkUnique = false, const int reserveSize = 100)
{
	int i;

	// Найти существующий, если необходимо
	if (checkUnique)
	{
		i = ArrayIndexOf(arr, sym, dir, isIndex);

		if (i != -1)
			return(i);
	}

	// Добавить новый элемент в массив
	i = ArrayResize(arr, ArraySize(arr) + 1, reserveSize);

	if (i == -1)
		return(-1);

	i--;
	arr[i].Sym = sym;
	arr[i].Dir = dir;
	arr[i].IsIndex = isIndex;

	return(i);
}

int ArrayIndexOf(const IndexElement &arr[], const string sym, const ENUM_DIR dir, const bool isIndex, const int startingFrom = 0)
{
	int count = ArraySize(arr);

	for (int i = startingFrom; i < count; i++)
	{
		if ((arr[i].Dir == dir) && (arr[i].Sym == sym) && (arr[i].IsIndex == isIndex))
			return(i);
	}

	return(-1);
}

class Index
{
	private: string _indexName;
	public: string GetIndex() const { return(_indexName); }

	private: uint _maPeriod;
	public: uint GetMaPeriod() const { return(_maPeriod); }

	private: ENUM_MA_METHOD _maMethod;
	public: ENUM_MA_METHOD GetMaMethod() const { return(_maMethod); }

	private: ENUM_APPLIED_PRICE _appliedPrice;
	public: ENUM_APPLIED_PRICE GetAppliedPrice() const { return(_appliedPrice); }

	// массивы для данных рассчёта индекса доллара
	private: string _currs[];
	private: IndexElement _elements[];
	private: MaPool *_mas;
	private: int _n;

	// данные основного символа
	private: string _sym;
	private: ENUM_DIR _dir;

	private: bool _isInitialized;
	public: bool IsInitialized() const { return(_isInitialized); }

	public: void Index(const string indexName, const string &currs[], const ENUM_APPLIED_PRICE appliedPrice, const uint maPeriod, const ENUM_MA_METHOD maMethod)
	{
		_indexName = indexName;
		_maPeriod = maPeriod;
		_maMethod = maMethod;
		_appliedPrice = appliedPrice;

		ArrayGroup(currs, _currs);
		ArrayAdd(_currs, _indexName, true);

		_n = ArraySize(_currs);

		_mas = new MaPool(_appliedPrice, _maMethod);
		ArrayFree(_elements);

		Init();
	}

	public: void ~Index()
	{
		delete(_mas);
	}

	public: bool Init()
	{
		_isInitialized = false;

		// индекс
		if (!getSymbols())
			return(false);

		// основной символ
		if (!findSymbol(_indexName, _sym, _dir))
			return(false);

		_isInitialized = true;

		return(_isInitialized);
	}

	public: bool CheckInitialized()
	{
		if (_isInitialized)
			return(true);

		return(Init());
	}

	// Получить значение по средним
	// В случае неудачи возвращается EMPTY_VALUE
	public: double GetValue(const int bar)
	{
		if (!CheckInitialized())
			return(EMPTY_VALUE);

		// вычислить индекс доллара

		double value = 1;

		for (int i = 0; i < _n; i++)
		{
			double q = getSymValue(i, bar);

			if (q == EMPTY_VALUE)
				return(EMPTY_VALUE);

			if (_elements[i].Dir == DIR_DIRECT)
				value *= MathPow(q, 1.0 / _n);
			else
				value *= MathPow(q, -1.0 / _n);
		}

		// если расчёт идёт индекса USD, то основной символ считать не имеет смысла (USD/USD)
		if (_sym == "")
			return(value);

		// умножить на основной кросс
		double mainSymValue = 1;

		mainSymValue = getSymValue(_sym, _dir, bar);

		if (mainSymValue == EMPTY_VALUE)
			return(EMPTY_VALUE);

		if (_dir == DIR_DIRECT)
			return(value * mainSymValue);
		else
			return(value / mainSymValue);
	}

	// Формула индекса в текстовом представлении
	public: string GetFormula() const
	{
		if (!_isInitialized)
			return("");

		string formula = "";

		for (int i = 0; i < _n; i++)
			formula = formula + (_elements[i].Dir == DIR_DIRECT ? "*" : "/") + _elements[i].Sym;

		formula = "(" + formula + ")^1/" + IntegerToString(_n) + " ";

		if (_sym != "")
		{
			if (_dir == DIR_DIRECT)
				formula = formula + " * " + _sym;
			else
				formula = formula + " / " + _sym;
		}

		formula = formula + " [" + EnumToString(_appliedPrice) + "]";

		return(_indexName + " = " + formula);
	}

	private: double getSymValue(const string sym, const ENUM_DIR dir, const int bar) const
	{
		if (sym == "")
			return(1);

		if (_maPeriod == 0)
			return(getPrice(sym, dir, bar));

		return(_mas.Get(sym, _maPeriod, ReverseAppliedPrice(_appliedPrice, dir == DIR_REVERSE)).GetValue(bar));
	}

	private: double getSymValue(const int elementIndex, const int bar) const
	{
		return(getSymValue(_elements[elementIndex].Sym, _elements[elementIndex].Dir, bar));
	}

	// Получить список символов и их степени в формуле для расчета индекса
	private: bool getSymbols()
	{
		ArrayFree(_elements);

		string sym;
		ENUM_DIR dir;

		// количество валют для расчета
		for (int i = 0; i < _n; i++)
		{
			if (!findSymbol(_currs[i], sym, dir))
				return(false);

			ArrayAdd(_elements, sym, dir == DIR_DIRECT ? DIR_REVERSE : DIR_DIRECT, false);
		}

		return(ArraySize(_elements) == _n);
	}

	private: bool findSymbol(const string curr, string &sym, ENUM_DIR &dir) const
	{
		// Получить правильное название символа на основе двух составляющих (curr и USD)

		if (curr == "USD")
		{
			sym = "";
			dir = DIR_DIRECT; // без разницы
			return(true);
		}

		// CFD и индексы
		if ((StringGetCharacter(curr, 0) == '#') || (StringGetCharacter(curr, 0) == '_'))
		{
			if (!checkSymbolExists(curr))
				return(false);

			sym = curr;
			dir = DIR_DIRECT;
			return(true);
		}

		sym = curr + "USD";

		if (checkSymbolExists(sym))
		{
			dir = DIR_DIRECT;
			return(true);
		}

		sym = "USD" + curr;

		if (checkSymbolExists(sym))
		{
			dir = DIR_REVERSE;
			return(true);
		}

		return(false);
	}

	private: bool checkSymbolExists(const string &sym) const
	{
		// В MQL4 приходится делать предварительный запрос, т.к. основной метод может работать очень медленно.
		if (iClose(sym, 0, 0) != 0)
			return(true);

		return(SymbolSelect(sym, true));
	}

	// Получить цену символа для указанного бара символа графика
	private: double getPrice(const string symbol, const ENUM_DIR dir, const int bar) const
	{
		// Определить время бара для символа графика
		datetime times[];

		if (CopyTime(_Symbol, PERIOD_CURRENT, bar, 1, times) != 1)
			return(EMPTY_VALUE);

		double open[], high[], low[], close[];

		ENUM_APPLIED_PRICE appliedPrice = ReverseAppliedPrice(_appliedPrice, dir == DIR_REVERSE);
		MqlRates rates[];

		if (CopyRates(symbol, PERIOD_CURRENT, times[0], 1, rates) != 1)
			return(EMPTY_VALUE);

		MqlRates rate = rates[0];

		switch(appliedPrice)
		{
			case PRICE_CLOSE:
				return(rate.close);

			case PRICE_OPEN:
				return(rate.open);

			case PRICE_HIGH:
				return(rate.high);

			case PRICE_LOW:
				return(rate.low);

			case PRICE_MEDIAN:
				return((rate.high + rate.low) / 2.0);

			case PRICE_TYPICAL:
				return((rate.high + rate.low + rate.close) / 3.0);

			case PRICE_WEIGHTED:
				return((rate.high + rate.low + 2.0 * rate.close) / 4.0);
		}

		return(EMPTY_VALUE);
	}
};

class IndexPool
{
	private: Index *_indexes[];
	private: ENUM_APPLIED_PRICE _appliedPrice;
	private: ENUM_MA_METHOD _maMethod;
	private: string _calcCurrs[];

	public: int Count()
	{
		return(ArraySize(_indexes));
	}

	public: void IndexPool(const ENUM_APPLIED_PRICE appliedPrice, const ENUM_MA_METHOD maMethod, const string &calcCurrs[])
	{
		ArrayFree(_indexes);
		_appliedPrice = appliedPrice;
		_maMethod = maMethod;
		ArrayFree(_calcCurrs);
		ArrayCopy(_calcCurrs, calcCurrs);
	}

	public: void ~IndexPool()
	{
		for (int i = 0, count = Count(); i < count; i++)
			delete(_indexes[i]);
	}

	public: Index *Get(const string indexName, const int maPeriod, const ENUM_APPLIED_PRICE appliedPrice)
	{
		Index *index = find(indexName, maPeriod, appliedPrice);

		if (CheckPointer(index) != POINTER_INVALID)
			return(index);

		int newArrayIndex = Count();
		ArrayResize(_indexes, newArrayIndex + 1);

		index = new Index(indexName, _calcCurrs, _appliedPrice, maPeriod, _maMethod);
		_indexes[newArrayIndex] = index;

		return(index);
	}

	public: bool CheckInitialized()
	{
		bool resul = true;

		for (int i = 0; i < Count(); i++)
		{
			if (!_indexes[i].CheckInitialized())
				return(false);
		}

		return(true);
	}

	private: Index *find(const string indexName, const int maPeriod, const ENUM_APPLIED_PRICE appliedPrice)
	{
		for (int i = 0, count = Count(); i < count; i++)
		{
			if ((_indexes[i].GetIndex() == indexName) && (_indexes[i].GetMaPeriod() == maPeriod) && (_indexes[i].GetAppliedPrice() == appliedPrice))
				return(_indexes[i]);
		}

		return(NULL);
	}
};

double X[];                  // буфер линии
IndexElement _elements[];    // элементы формулы
uint _maPeriod;              // период усреднения
uint _diffMaPeriod;          // период вычитаемой средней
string _indexCalcCurrs[];    // единый набор валют/инструментов для расчёта всех индексов

MaPool * _mas;         // Набор доступных индикаторов MA
IndexPool * _indexes;  // Набор доступных индикаторов Index

/*
Список изменений:

8.3
	* изменены ссылки на сайт
	* список изменений в коде

8.2
	* исправлено: независимо от параметров, в набор валют для расчёта индексов всегда был включен USD

8.1
	* исправлено: в версии для MT5 не работают параметры "MA period" и "Diff MA period"

8.0
	* рефакторинг
	* единая нумерация версий для MT4 и MT5

Только для MetaTrader 4:

7.1
	* исправлено: при рассчёте по ценам High или Low не учитывалось направления кроссов в формуле
	* улучшено: расчёт на основе индекса доллара, был на основе формулы кроссов с долларом, результата это не меняет, но упрощает код
	* основной код почти идентичен коду для MT5, что упростит поддержку

7.0
	* вместо одного индекса можно использовать формулу с умножением и делением нескольких индексов и кроссов

6.3
	* в версии 6.2 по ошибке добавлен параметр StdScore

6.2
	* поддержка MetaTrader 4.00 Build 600 и новее

6.1
	* поддержка нового MQL4

6.0
	* удален параметр Params (все параметры одной строкой)
	* удален параметр BaseCurr, теперь это всегда USD
	* удален грязный режим и параметр Dirty
	* добавлена возможность вычитания средней (новый параметр DiffMAPeriod)
	* сессия основного символа при расчете теперь не учитывается
	* улучшено отображение параметров в заголовке индикатора

5.1
	* возможность отображения в логарифмическом масштабе (новый параметр LogScale)

5.0.0
	* возможность сглаживания (новые параметры MAPeriod, MAMethod)
	* убрано незадокументированное распознавание реверса по rev и reverse

4.1.3
	* улучшен учет пропусков в истории котировок
	* расширена поддержка инструментов (индексы, CFD)

4.1.2
	* улучшен учет пропусков в истории котировок

4.1.1
	* возможная утечка памяти в используемом модуле (без последствий для самого индикатора)

4.1.0
	* первая публичная версия

Только для MetaTrader 5:

5.2
	* исправлено: ошибка в формуле рассчёта, не учитывается направление основного кросса

5.1
	* исправлено: при рассчёте по ценам High или Low не учитывалось направления кроссов в формуле
	* улучшено: расчёт на основе индекса доллара, был на основе формулы кроссов с долларом, результата это не меняет, но упрощает код
	* улучшено: лучшая реакция на подгрузку истории

5.0
	* вместо одного индекса можно использовать формулу с умножением и делением нескольких индексов и кроссов

4.4
	* первая публичная версия

Лицензия:

Разрешается повторное распространение и использование как в виде исходного кода, так и в двоичной форме, с изменениями
или без, при соблюдении следующих условий:

При повторном распространении исходного кода должно оставаться указанное выше уведомление об авторском праве, этот
список условий и последующий отказ от гарантий.

При повторном распространении двоичного кода должна сохраняться указанная выше информация об авторском праве, этот
список условий и последующий отказ от гарантий в документации и/или в других материалах, поставляемых при распространении.

Ни название FXcoder, ни имена ее сотрудников не могут быть использованы в качестве поддержки или продвижения продуктов,
основанных на этом ПО без предварительного письменного разрешения.

ЭТА ПРОГРАММА ПРЕДОСТАВЛЕНА ВЛАДЕЛЬЦАМИ АВТОРСКИХ ПРАВ И/ИЛИ ДРУГИМИ СТОРОНАМИ «КАК ОНА ЕСТЬ» БЕЗ КАКОГО-ЛИБО ВИДА
ГАРАНТИЙ, ВЫРАЖЕННЫХ ЯВНО ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ, ПОДРАЗУМЕВАЕМЫЕ ГАРАНТИИ КОММЕРЧЕСКОЙ
ЦЕННОСТИ И ПРИГОДНОСТИ ДЛЯ КОНКРЕТНОЙ ЦЕЛИ. НИ В КОЕМ СЛУЧАЕ, ЕСЛИ НЕ ТРЕБУЕТСЯ СООТВЕТСТВУЮЩИМ ЗАКОНОМ, ИЛИ НЕ
УСТАНОВЛЕНО В УСТНОЙ ФОРМЕ, НИ ОДИН ВЛАДЕЛЕЦ АВТОРСКИХ ПРАВ И НИ ОДНО ДРУГОЕ ЛИЦО, КОТОРОЕ МОЖЕТ ИЗМЕНЯТЬ И/ИЛИ
ПОВТОРНО РАСПРОСТРАНЯТЬ ПРОГРАММУ, КАК БЫЛО СКАЗАНО ВЫШЕ, НЕ НЕСЁТ ОТВЕТСТВЕННОСТИ, ВКЛЮЧАЯ ЛЮБЫЕ ОБЩИЕ, СЛУЧАЙНЫЕ,
СПЕЦИАЛЬНЫЕ ИЛИ ПОСЛЕДОВАВШИЕ УБЫТКИ, ВСЛЕДСТВИЕ ИСПОЛЬЗОВАНИЯ ИЛИ НЕВОЗМОЖНОСТИ ИСПОЛЬЗОВАНИЯ ПРОГРАММЫ (ВКЛЮЧАЯ, НО
НЕ ОГРАНИЧИВАЯСЬ ПОТЕРЕЙ ДАННЫХ, ИЛИ ДАННЫМИ, СТАВШИМИ НЕПРАВИЛЬНЫМИ, ИЛИ ПОТЕРЯМИ ПРИНЕСЕННЫМИ ИЗ-ЗА ВАС ИЛИ ТРЕТЬИХ
ЛИЦ, ИЛИ ОТКАЗОМ ПРОГРАММЫ РАБОТАТЬ СОВМЕСТНО С ДРУГИМИ ПРОГРАММАМИ), ДАЖЕ ЕСЛИ ТАКОЙ ВЛАДЕЛЕЦ ИЛИ ДРУГОЕ ЛИЦО БЫЛИ
ИЗВЕЩЕНЫ О ВОЗМОЖНОСТИ ТАКИХ УБЫТКОВ.

License:

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

Neither the name of the FXcoder nor the names of its contributors may be used to endorse or promote products derived
from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

// 2016-02-04 06:45:57 UTC. MQLMake 1.37. © FXcoder