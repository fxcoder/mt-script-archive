/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Перечисление: тип будильника.
*/

/**
Тип будильника.
*/
enum ENUM_XLIB_ALARM_TYPE
{
	/** одноразовый будильник */
	XLIB_ALARM_SINGLE,
	/** ежедневный будильник */
	XLIB_ALARM_DAILY
};

