/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Включение всех файлов библиотеки для проверки совместимости.
*/


#property strict

#include "define\color.mqh"
#include "define\convert.mqh"
#include "define\inet.mqh"
#include "define\math.mqh"
#include "define\time.mqh"

#include "enum\alarm_type.mqh"

#include "import\wininet.mqh"

#include "util\array.mqh"
#include "util\chart.mqh"
#include "util\color.mqh"
#include "util\convert.mqh"
#include "util\hyperbolic.mqh"
#include "util\id.mqh"
#include "util\io.mqh"
#include "util\math.mqh"
#include "util\string.mqh"
#include "util\time.mqh"
#include "util\tz.mqh"

#include "class\alarm.mqh"
#include "class\counter.mqh"
#include "class\inet.mqh"
#include "class\log.mqh"
#include "class\symbol.mqh"
#include "class\timer.mqh"
#include "class\touch.mqh"

#include "changes.txt"


/**
@mainpage
# Библиотека функций и классов для MetaTrader (MQL4 и MQL5). #

## Mercurial ##

`hg clone https://bitbucket.org/FXcoder/mql.xlib`

## Предупреждение о рисках ##

Данная библиотека находится на стадии разработки. Любые функции и классы в ней (особенно новые) могут быть недостаточно протестированы.

## Условия использования и распространения ##

Библиотека распространяется под [модифицированной лицензией BSD](https://fxcoder.blogspot.com/modified-bsd-license).

Полная информация на странице проекта: [MQL.XLib](https://fxcoder.blogspot.com/2015/03/mql-xlib-library.html).
*/
