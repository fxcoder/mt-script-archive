/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Функции для работы с массивами.
*/


/**
Убрать дубликаты из массива (сгруппировать значения).

@param src[]   Исходный массив
@param dest[]  Результирующий массив

@return        Количество элементов в результате (количество уникальных значений в исходном массиве).
               В случае ошибки возвращается -1.
*/
template <typename T>
int ArrayGroup(const T &src[], T &dest[])
{
	int count[];
	int destCount = ArrayGroupCount(src, dest, count);
	return (destCount);
}


/**
Сгруппировать массив, получить распределение значений.

@param src[]    Исходный массив
@param dest[]   Результирующий массив
@param count[]  Результирующий массив с числами вхождений

@return         Количество элементов в результате (количество уникальных значений в исходном массиве).
                В случае ошибки возвращается -1.
*/
template <typename T>
int ArrayGroupCount(const T &src[], T &dest[], int &count[])
{
	int srcCount = ArraySize(src);
	
	// Сброс результата
	int destCount = 0;
	ArrayFree(dest);
	ArrayFree(count);
	
	for (int i = 0; i < srcCount; i++)
	{
		int si = ArrayIndexOf(dest, src[i]);

		if (si >= 0)
		{
			// увеличить счетчик вхождений
			count[si]++;
		}
		else
		{
			// добавить новый элемент в результат
			if ((ArrayAdd(dest, src[i], false, srcCount) < 0) || (ArrayAdd(count, 1, false, srcCount) < 0))
				return(-1);
		}
	}
	
	return (ArraySize(dest));
}


/**
Найти значение в массиве и вернуть индекс элемента.

Поиск производится, начиная с элемента с индексом startingFrom.

@param arr[]         Массив для поиска
@param value         Искомое значение
@param startingFrom  Индекс начала поиска

@return              Индекс найденного элемента или -1, если значение не найдено.
*/
template <typename T>
int ArrayIndexOf(const T &arr[], const T value, const int startingFrom = 0)
{
	int size = ArraySize(arr);

	for (int i = startingFrom; i < size; i++)
	{
		if (arr[i] == value)
			return(i);
	}
	
	return(-1);
}


/**
Определить, есть ли значение в массиве.

Поиск производится, начиная с элемента с индексом startingFrom. См. также ArrayIndexOf

@param arr[]         Массив для поиска
@param value         Искомое значение
@param startingFrom  Индекс начала поиска

@return              true - значение есть в массиве, false - нет.
*/
template <typename T>
bool ArrayContains(const T &arr[], const T value, const int startingFrom = 0)
{
	return(ArrayIndexOf(arr, value, startingFrom) != -1);
}


/**
Добавить в массив значение и вернуть его индекс (с проверкой на уникальность).

@param arr[]        Массив, в который нужно добавить новое значение
@param value        Значение
@param checkUnique  Проверять уникальность
@param reserveSize  Резерв памяти, см. описание стандартной функции ArrayResize(). По умолчанию 100.

@return             Индекс добавленного элемента или того, что уже был в массиве (если такой уже был до добавления и checkUnique=True).
                    В случае ошибки возвращается -1.
*/
template <typename T>
int ArrayAdd(T &arr[], T value, const bool checkUnique = false, const int reserveSize = 100)
{
	int i;
	
	// Найти существующий, если необходимо
	if (checkUnique)
	{
		i = ArrayIndexOf(arr, value);
		
		if (i != -1)
			return(i);
	}

	// Добавить новый элемент в массив
	i = ArrayResize(arr, ArraySize(arr) + 1, reserveSize);
	
	if (i == -1)
		return(-1);
	
	i--;
	arr[i] = value;
	
	return(i);
}


/**
Добавить в массив массив значений и вернуть новый размер.
См. также ArrayAdd.

@param resArr[]     Массив, в который нужно добавить новые значения
@param addArr       Массв добавляемых значений
@param checkUnique  Проверять уникальность
@param reserveSize  Резерв памяти, см. описание стандартной функции ArrayResize(). По умолчанию 100.

@return             Размер массива, в который добавляются элементы.
*/
template <typename T>
int ArrayAddArray(T &resArr[], T &addArr[], const bool checkUnique = false, const int reserveSize = 100)
{
	for (int i = 0, size = ArraySize(addArr); i < size; i++)
		ArrayAdd(resArr, addArr[i], checkUnique, reserveSize);
	
	return(ArraySize(resArr));
}


/**
Инициализировать массив (string)

@param a[]  Массив
@param s    Значение, которым необходимо инициализировать массив

@return     Количество элементов в массиве.
*/
int ArrayInitialize(string &a[], const string s = "")
{
	int c = ArraySize(a);

	for (int i = 0; i < c; i++)
		a[i] = s;
		
	return(c); // Вряд ли это где-то необходимо, добавлено для похожести со стандартными функциями
}


/**
Сшить массив в одну строку, используя указанный разделитель.

@param arr[]      Массив.
@param separator  Разделитель.

@return           Строка-результат.
*/
template <typename T>
string ArrayToString(const T &arr[], const string separator)
{
	int size = ArraySize(arr);
	
	if (size == 0)
		return("");

	string result = (string)arr[0];

	for (int i = 1; i < size; i++)
	{
		StringAdd(result, separator);
		StringAdd(result, (string)arr[i]);
	}
	
	return(result);
}


/**
Сшить массив (double) в одну строку, используя указанный разделитель.

@param arr[]      Массив.
@param separator  Разделитель.
@param digits     Количество знаков после запятой при преобразовании дробного числа в строку.

@return           Строка-результат.
*/
string ArrayToString(const double &arr[], const string separator, int digits)
{
	int size = ArraySize(arr);
	
	if (size == 0)
		return("");

	string result = DoubleToString(arr[0], digits);

	for (int i = 1; i < size; i++)
	{
		StringAdd(result, separator);
		StringAdd(result, DoubleToString(arr[i], digits));
	}
	
	return(result);
}


/**
Сшить массив (datetime) в одну строку, используя указанный разделитель.

@param arr[]      Массив.
@param separator  Разделитель.
@param flags      Формат вывода, см. стандартную функцию TimeToString.

@return           Строка-результат.
*/
string ArrayToString(const datetime &arr[], const string separator, int flags)
{
	int size = ArraySize(arr);
	
	if (size == 0)
		return("");

	string result = TimeToString(arr[0], flags);

	for (int i = 1; i < size; i++)
	{
		StringAdd(result, separator);
		StringAdd(result, TimeToString(arr[i], flags));
	}
	
	return(result);
}


/**
Удалить часть массива.

@param arr[]      Массив
@param start      Начало части
@param length     Длина части

@return           Количество элементов в результате
*/
template <typename T>
int ArrayRemove(T &arr[], int start, int length)
{
	int size = ArraySize(arr);

	if (!ArrayCheckRange(arr, start, length))
		return(size);

	T tmp[];
	
	int tmpSize = size - length;
	ArrayResize(tmp, tmpSize);
	
	// левая часть
	if (start > 0)
		ArrayCopy(tmp, arr, 0, 0, start);
		
	// правая часть
	if (start + length < size)
		ArrayCopy(tmp, arr, start, start + length);
		
	ArrayResize(arr, tmpSize);
	return(ArrayCopy(arr, tmp));
}


/**
Суммировать элементы массива.

@deprecated  См. util\math.mqh\Sum
*/
template <typename T>
T ArraySum(const T &arr[], int start = 0, int count = -1)
{
	return(Sum(arr, start, count));
}


/**
Проверка и коррекция границ диапазона для указанного массива. Выходные параметры должны обеспечивать
безопасность обхода массива по ним.

@param arr    Массив.
@param start  Начальный элемент в массиве для обработки.
@param count  Количество элементов для обрабтоки, -1 - от start до конца массива.

@return       true, если границы в норме, либо их удалось привести к норме. false, если границы не пересекаются с массивом.
*/
template <typename T>
bool ArrayCheckRange(const T &arr[], int &start, int &count)
{
	int size = ArraySize(arr);

	// в случае пустого массива результат неопределён, но вернуть как ошибочный
	if (size <= 0)
		return(false);

	// в случае нулевого диапазона результат неопределён, но вернуть как ошибочный
	if (count == 0)
		return(false);

	//TODO: Определиться со start < 0. Возможно, следует искать пересечение и в этом случае.
	// старт выходит за границы массива
	if ((start > size - 1) || (start < 0))
		return(false);
		
	if (count < 0)
	{
		// если количество не указано, вернуть всё от start
		count = size - start;
	}
	else if (count > size - start)
	{
		// если элементов недостаточно для указанного количества, вернуть максимально возможное
		count = size - start;
	}

	return(true);
}

