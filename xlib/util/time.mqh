/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Функции для работы с датой и временем.
*/


#include "..\define\time.mqh"


/**
Преобразовать переменную типа datetime в переменную типа MqlDateTime.

@param time  Время.

@return      Время в структуре MqlDateTime.
*/
MqlDateTime TimeToMqlDateTime(const datetime time)
{
	MqlDateTime timeStruct;
	TimeToStruct(time, timeStruct);
	return(timeStruct);
}

/**
Обновить структуру времени. Может быть полезно после изменения её частей для обновления полей дня недели и дня года.

@param timeStruct  Время в виде структуры.
*/
void TimeUpdateStruct(MqlDateTime &timeStruct)
{
	TimeToStruct(StructToTime(timeStruct), timeStruct);
}

#ifndef __MQL4__
/**
Получить день недели для указанного времени. Только для MQL5, в MQL4 эта функция уже есть.

@param time  Время.

@return      День недели.
*/
ENUM_DAY_OF_WEEK TimeDayOfWeek(const datetime time)
{
	MqlDateTime timeStruct;
	TimeToStruct(time, timeStruct);
	return (ENUM_DAY_OF_WEEK)timeStruct.day_of_week;
}
#endif


/**
Получить время начала дня для указанного времени. См. также шаблон TIME_BEGIN_OF_DAY(T).

@param time  Исходное время. Если не указано, будет использовано последнее время сервера.

@return      Время начала дня.
*/
datetime TimeBeginOfDay(const datetime time)
{
	return(TIME_BEGIN_OF_DAY(time));
}


/**
Получить время от начала дня.

@param time  Исходное время. Если не указано, будет использовано последнее время сервера. 
             См. также шаблон TIME_BEGIN_OF_DAY(T).

@return      Время от начала дня.
*/
datetime TimeOfDay(const datetime time)
{
	return(TIME_OF_DAY(time));
}

/**
Получить время начала недели.

@param time     Исходное время. Если не указано, будет использовано последнее время сервера.

@return         Время начала недели. Считается, что первый день недели - понедельник.
*/
datetime TimeBeginOfWeek(datetime time)
{
	time -= time % TIME__SECONDS_IN_DAY;
	
	int dow = TimeDayOfWeek(time);

	if (dow == 0)
		time -= 6 * TIME__SECONDS_IN_DAY;
	else
		time -= (dow - 1) * TIME__SECONDS_IN_DAY;
	
	return(time);
}

/**
Добавить или вычесть указанное количество рабочих дней.

@param time  Время, к которому нужно прибавить дни.
@param days  Количество рабочих дней, которое нужно добавить. При отрицательном значении дни будут вычитаться.

@return      Время с добавленными или вычтенными днями.
*/
datetime TimeAddWorkDays(datetime time, const int days)
{
	int SECONDS_IN_TWO_DAYS = 2 * TIME__SECONDS_IN_DAY;

	int i;
	ENUM_DAY_OF_WEEK dayOfWeek;

	if (days > 0)
	{
		for (i = 1; i <= days; i++)
		{
			time += TIME__SECONDS_IN_DAY;
			dayOfWeek = (ENUM_DAY_OF_WEEK)TimeDayOfWeek(time);
						
			// Воскресенье сдвигаем на понедельник, субботу сдвигаем на понедельник
			if (dayOfWeek == SUNDAY)
				time += TIME__SECONDS_IN_DAY;
			else if (dayOfWeek == SATURDAY)
				time += SECONDS_IN_TWO_DAYS;
		}
	}
	else
	{
		for (i = 1; i <= -days; i++)
		{
			if (time < TIME__SECONDS_IN_DAY)
				return(0);
			
			time -= TIME__SECONDS_IN_DAY;
			dayOfWeek = (ENUM_DAY_OF_WEEK)TimeDayOfWeek(time);
			
			// Воскресенье сдвигаем на пятницу, субботу сдвигаем на пятницу
			if (dayOfWeek == SUNDAY)
			{
				if (time < SECONDS_IN_TWO_DAYS)
					return(0);

				time -= SECONDS_IN_TWO_DAYS;
			}
			else if (dayOfWeek == SATURDAY)
			{
				if (time < TIME__SECONDS_IN_DAY)
					return(0);

				time -= TIME__SECONDS_IN_DAY;
			}
		}
	}

	return(time);
}

/**
Добавить или вычесть указанное количество недель.

@param time   Время, к которому нужно прибавить недели.
@param weeks  Количество недель, которое нужно добавить. При отрицательном значении недели будут вычитаться.

@return       Время с добавленными или вычтенными неделями.
*/
datetime TimeAddWeeks(const datetime time, const int weeks)
{
	return(time + 7 * TIME__SECONDS_IN_DAY * weeks);
}


/**
Определить, находится ли время внутри заданной сессии.

@param time      Время для сравнения.
@param timeFrom  Строка времени начала сессии.
@param timeTo    Строка времени окончания сессии.

@return          Нахождение времени внутри заданной сессии.

@par Пример: определить, находимся ли мы сейчас в американской сессии

Предполагается, что часовой пояс терминала - CET. Границы сессии условные.

@code
bool isAmerica = TimeIsInSession(TimeCurrent(), "14:30", "20:59:59");
@endcode
*/
bool TimeIsInSession(const datetime time, const string timeFrom, const string timeTo)
{
	datetime now = time;
	datetime today = TIME_BEGIN_OF_DAY(now);
	
	datetime sessionBegin = StringToTime(TimeToString(today, TIME_DATE) + " " + timeFrom);
	datetime sessionEnd = StringToTime(TimeToString(today, TIME_DATE) + " " + timeTo);
	
	if (sessionEnd < sessionBegin)
		sessionEnd += TIME__SECONDS_IN_DAY;
		
	if (now < sessionBegin)
	{
		sessionBegin -= TIME__SECONDS_IN_DAY;
		sessionEnd -= TIME__SECONDS_IN_DAY;
	}

	return((sessionBegin <= now) && (now <= sessionEnd));
}

/**
Определить, является ли день выходным (суббота или воскресенье).

@param time  Время для проверки. Если не указано, будет использовано последнее время сервера.

@return      True - выходной (суббота или воскресенье), false - рабочий день (понедельник - пятница).
*/
bool TimeIsWeekend(const datetime time)
{
#ifndef __MQL4__
	ENUM_DAY_OF_WEEK dow = TimeDayOfWeek(time);
	return((dow == SATURDAY) || (dow == SUNDAY));
#else
	int dow = TimeDayOfWeek(time);
	return((dow == 6) || (dow == 0));
#endif 
}

