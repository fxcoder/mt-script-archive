/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Строковые функции.
*/

#include "array.mqh"


/**
Заменить подстроку в строке на указанную.

@param inputString  Исходная строка.
@param from         Искомая подстрока.
@param to           Строка, на которую нужно заменить указанную подстроку.

@return             Преобразованная строка.
*/
string ReplaceString(string inputString, const string from, const string to)
{
	// Используем стандартную функцию
	StringReplace(inputString, from, to);
	return(inputString);
}


/**
Убрать указанные символы в начале строки.

@param s   Входная строка для преобразований.
@param ch  Удаляемый символ.

@return    Строка без указанных символов в начале.
*/
string TrimLeft(string s, const ushort ch)
{
	int len = StringLen(s);
	
	// Найти конец вырезаемого от начала участка
	int cut = 0;
	
	for (int i = 0; i < len; i++)
	{
		if (StringGetCharacter(s, i) == ch)
			cut++;
		else
			break;
	}
	
	// Отрезать
	if (cut > 0)
	{
		if (cut > len - 1)
			s = "";
		else
			s = StringSubstr(s, cut);
	}

	return(s);
}

/**
Убрать указанные символы в конце строки.

@param s   Входная строка для преобразований.
@param ch  Удаляемый символ. Если не указан (-1), удаляются символы перевода строк, пробелы, табуляция.

@return    Строка без указанных символов в конце.
*/
string TrimRight(string s, const ushort ch)
{
	int len = StringLen(s);
	
	// Найти начало вырезаемого до конца участка
	int cut = len;
	
	for (int i = len - 1; i >= 0; i--)
	{
		if (StringGetCharacter(s, i) == ch)
			cut--;
		else
			break;
	}
	
	if (cut != len)
	{
		if (cut == 0)
			s = "";
		else
			s = StringSubstr(s, 0, cut);
	}		

	return(s);
}

/**
Убрать пустые символы в начале и конце строки

@param s   Входная строка для преобразований.

@return    Строка без указанных символов в начале и конце.
*/
string Trim(string s)
{
#ifdef __MQL4__
	s = StringTrimRight(s);
	s = StringTrimLeft(s);
#else
	StringTrimRight(s);
	StringTrimLeft(s);
#endif
	return(s);
}


/**
Убрать указанные символы в начале и конце строки

@param s   Входная строка для преобразований.
@param ch  Удаляемый символ. Если не указан (-1), удаляются символы перевода строк, пробелы, табуляция.

@return    Строка без указанных символов в начале и конце.
*/
string Trim(const string s, const ushort ch)
{
	return(TrimLeft(TrimRight(s, ch), ch));
}

/**
Разбить строку на отдельные строки, разделенные в исходной строке указанным разделителем.

@param s            Входная строка.
@param sep          Разделитель.
@param parts[]      Ссылка на результат - массив строк.
@param removeEmpty  Пропускать пустые строки.
@param unique       Добавлять только уникальные значения.

@return             Количество строк в результирующем массиве строк. Сам массив передается по ссылке 
                    в параметрах.
*/
int SplitString(string s, const string sep, string &parts[], const bool removeEmpty = false, const bool unique = false)
{
	//TODO: решить, что делать при пустом s и removeEmpty = false

	// Оптимизация наиболее часто используемого варианта за счёт использования стандартной функции со схожим, но ограниченным, функциоалом.
	//TODO: проверить, нужна ли такая оптимизация
	if (!removeEmpty && !unique && (StringLen(sep) == 1))
	{
		ushort sepChar = StringGetCharacter(sep, 0);
		return(StringSplit(s, sepChar, parts));
	}


	int count = 0;
	int sepLen = StringLen(sep);
	ArrayFree(parts);

	string part;
	
	while (true)
	{
		int p = StringFind(s, sep);
		
		if (p >= 0)
		{
			if (p == 0)
				part = "";
			else
				part = StringSubstr(s, 0, p);

			if (!removeEmpty || (Trim(part) != ""))
			{
				ArrayAdd(parts, part, unique);
				count = ArraySize(parts);
			}

			s = StringSubstr(s, p + sepLen);
		}
		else
		{
			// Последний кусок
			if (!removeEmpty || (Trim(s) != ""))
			{
				ArrayAdd(parts, s, unique);
				count = ArraySize(parts);
			}
			
			break;
		}		
	}
	
	// удалить последнюю пустую строку
	if ((count > 0) && (parts[count - 1] == ""))
	{
		count--;
		
		if (count > 0)
			ArrayResize(parts, count);
	}
	
	return(count);
}


/**
Перевести символ в верхний регистр

@param ch  Исходный символ.

@return    Символ, переведенный в верхний регистр.
*/
ushort UpperChar(const ushort ch)
{
	string s = ".";
	StringSetCharacter(s, 0, ch);
	StringToUpper(s);
	return(StringGetCharacter(s, 0));
}

/**
Перевести символ в нижний регистр

@param ch  Исходный символ.

@return    Символ, переведенный в нижний регистр.
*/
ushort LowerChar(const ushort ch)
{
	string s = ".";
	StringSetCharacter(s, 0, ch);
	StringToLower(s);
	return(StringGetCharacter(s, 0));
}

/**
Перевести строку в верхний регистр

@param s  Исходная строка.

@return   Строка, переведенная в верхний регистр.
*/
string UpperString(string s)
{
	StringToUpper(s);
	return(s);
}

/**
Перевести строку в нижний регистр

@param s  Исходная строка.

@return   Строка, переведенная в нижний регистр.
*/
string LowerString(string s)
{
	StringToLower(s);
	return(s);
}

