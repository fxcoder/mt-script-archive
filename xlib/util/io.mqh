/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Функции ввода-вывода.
*/


#include "array.mqh"


/**
Считать текстовый файл по строкам в массив строк.

@param path       Путь до файла.
@param strings[]  Ссылка на массив строк, в который будут загружены строки файла.
@param addFlags   Дополнительные флаги для FileOpen.

@return           Количество загруженных строк (размер массива strings[]) или -1 в случае ошибки.
*/
int ReadStrings(const string path, string &strings[], int addFlags = 0)
{
	int count = 0;
	int arraySize = 0;
	int h = FileOpen(path, FILE_READ | FILE_TXT | addFlags);
	
	if (h == INVALID_HANDLE)
		return(-1);

	ArrayFree(strings);

	while (!FileIsEnding(h))
	{
		string line = FileReadString(h);
		
		if (ArrayAdd(strings, line, false, 1024) < 0)
		{
			FileClose(h);
			return(-1);
		}
	}
		
	FileClose(h);

	return(ArraySize(strings));
}


/**
Сохранить массив строк в текстовый файл построчно.

@param path       Путь до файла.
@param strings[]  Ссылка на массив строк, которые будут сохранены в файл.
@param addFlags   Дополнительные флаги для FileOpen.

@return           Успех операции.
*/
bool WriteStrings(const string path, const string &strings[], int addFlags = 0)
{
	int h = FileOpen(path, FILE_WRITE | FILE_TXT | addFlags);

	if (h == INVALID_HANDLE)
		return(false);

	int charSize = (addFlags & FILE_ANSI) == 0 ? 2 : 1;

	for (int i = 0, count = ArraySize(strings); i < count; i++)
	{
		string line = strings[i];
	
		if (FileWrite(h, line) != charSize * (StringLen(line) + 2))
		{
			FileClose(h);
			return(false);
		}
	}
	
	FileClose(h);
	
	return(true);
}

//TODO: WriteByteArray
/**
Считать файл в массив целых чисел.

@param path   Путь до файла.
@param arr[]  Ссылка на массив чисел, в который будут загружен файл.
@param addFlags   Дополнительные флаги для FileOpen.

@return       Количество загруженных данных или -1 в случае ошибки.
*/
int ReadByteArray(const string path, uchar &arr[], int addFlags = 0)
{
	int count = 0;
	int arraySize = 0;
	int h = FileOpen(path, FILE_BIN | FILE_READ | addFlags);
	
	if (h == INVALID_HANDLE)
		return(-1);

	while (!FileIsEnding(h))
	{
		uchar n = (uchar)FileReadInteger(h, CHAR_VALUE);
		
		if (ArrayAdd(arr, n, false, 1024) < 0)
		{
			FileClose(h);
			return(-1);
		}
	}
		
	FileClose(h);

	return(ArraySize(arr));
}


/**
Считать файл в массив целых чисел.

@param path   Путь до файла.
@param arr[]  Ссылка на массив чисел, в который будут загружен файл.
@param addFlags   Дополнительные флаги для FileOpen.

@return       Количество загруженных данных или -1 в случае ошибки.
*/
int ReadIntArray(const string path, int &arr[], int addFlags = 0)
{
	int count = 0;
	int arraySize = 0;
	int h = FileOpen(path, FILE_BIN | FILE_READ | addFlags);
	
	if (h == INVALID_HANDLE)
		return(-1);

	while (!FileIsEnding(h))
	{
		int n = FileReadInteger(h);
		
		if (ArrayAdd(arr, n, false, 1024) < 0)
		{
			FileClose(h);
			return(-1);
		}
	}
		
	FileClose(h);

	return(ArraySize(arr));
}


/**
Сохранить массив целых чисел в файл.

@param path   Путь до файла.
@param arr[]  Ссылка на массив чисел, которые будут сохранены в файл.
@param addFlags   Дополнительные флаги для FileOpen.

@return       Успех операции.
*/
bool WriteIntArray(const string path, const int &arr[], int addFlags = 0)
{
	int h = FileOpen(path, FILE_WRITE | FILE_BIN | addFlags);

	if (h == INVALID_HANDLE)
		return(false);

	int dataSize = sizeof(int);
	
	for (int i = 0, count = ArraySize(arr); i < count; i++)
	{
		int n = arr[i];
	
		if (FileWriteInteger(h, n) != dataSize)
		{
			FileClose(h);
			return(false);
		}
	}
	
	FileClose(h);
	
	return(true);
}

/**
Считать файл в массив дробных чисел.

@param path   Путь до файла.
@param arr[]  Ссылка на массив чисел, в который будут загружен файл.
@param addFlags   Дополнительные флаги для FileOpen.

@return       Количество загруженных данных или -1 в случае ошибки.
*/
int ReadDoubleArray(const string path, double &arr[], int addFlags = 0)
{
	int count = 0;
	int arraySize = 0;
	int h = FileOpen(path, FILE_BIN | FILE_READ | addFlags);
	
	if (h == INVALID_HANDLE)
		return(-1);

	while (!FileIsEnding(h))
	{
		double n = FileReadDouble(h);
		
		if (ArrayAdd(arr, n, false, 1024) < 0)
		{
			FileClose(h);
			return(-1);
		}
	}
		
	return(ArraySize(arr));
}

/**
Сохранить массив дробных чисел в файл.

@param path   Путь до файла.
@param arr[]  Ссылка на массив чисел, которые будут сохранены в файл.
@param addFlags   Дополнительные флаги для FileOpen.

@return       Успех операции.
*/
bool WriteDoubleArray(const string path, const double &arr[], int addFlags = 0)
{
	int h = FileOpen(path, FILE_WRITE | FILE_BIN | addFlags);

	if (h == INVALID_HANDLE)
		return(false);

	int dataSize = sizeof(double);
	
	for (int i = 0, count = ArraySize(arr); i < count; i++)
	{
		double n = arr[i];
	
		if (FileWriteDouble(h, n) != dataSize)
		{
			FileClose(h);
			return(false);
		}
	}
	
	FileClose(h);
	
	return(true);
}

/**
Загрузить строки.

Загружает информацию (предположительно текстовую) в массив строк и возвращает их количество. Работает только в скриптах и экспертах. 
Используйте класс XInet для загрузки информации из индикаторов.

@param url        Адрес загрузки.
@param strings[]  Ссылка на массив строк, в который будут загружены данные.
@param timeout    Таймаут.

@return           Количество загруженных строк. Загруженные строки передаются по ссылке в параметрах. В случае ошибки возвращается -1.
*/
int DownloadStrings(const string url, string &strings[], const int timeout)
{
	string cookie = NULL;
	string headers;
	char post[];
	char result[];
	
	ResetLastError();
	
	int res = WebRequest("GET", url, cookie, NULL, timeout, post, 0, result, headers);
	
	if (res == -1)
		return(-1);
   
	string rawData = CharArrayToString(result);
	
	// Разделение строк
	StringReplace(rawData, "\r\n", "\n"); //TODO: учесть вариант с "\n\r"
	ArrayFree(strings);
	
	return(StringSplit(rawData, '\n', strings));
}

