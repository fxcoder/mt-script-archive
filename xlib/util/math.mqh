/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Математические функции.
*/


#include "array.mqh"
#include "..\define\math.mqh"


/**
Получить число Фибоначчи по его порядковому номеру.

Максимальное число 1836311903 ограничено точностью типа int и достигается при i=46, дальше возвращается INT_MIN.
При использовании в качестве результата тип long ограничение наступает при i=70, дальше, вероятно, не хватает точности
вычислений с типом double.

@param i  Порядковый номер числа Фибоначчи. Если меньше 0, будет возвращёно 0. Если больше 46, будет возвращено INT_MIN.

@return   Число Фибоначчи: 1, 1, 2, 3, 5, 8, 13, 21...
          
Примеры:

 i  | Результат
----|-----------------------
<0  | 0
0   | 0
1   | 1
2   | 1
3   | 2
4   | 3
5   | 5
6   | 8
46  | 1836311903
>46 | -2147483648 (INT_MIN)  
     
*/
int Fibo(const int i)
{
	// 1.618033988749895 - золотое сечение. необходимо для построения ряда Фибоначи
	// 2.23606797749979  - MathSqrt(5)
	return((int)MathRound(MathPow(1.618033988749895, i) / 2.23606797749979));
}


/**
Сравнить два числа с плавающей точкой.

Т.к. с другими типами проблем с точностью не возникает, смысла удлиннять название до, например, CompareDouble нет.

@param a  Первое число для сравнения.
@param b  Второе число для сравнения. Если не указано, то производится сравнение с 0.
@param e  Точность, выраженная в знаках после запятой.

@return   1, 0, -1, в зависимости от того, больше, равно или меньше a, чем b

Примеры использования:

Условие | Код проверки условия | Альтернативный вариант
--------|----------------------|-----------------------
a = b   | Compare(a, b) == 0   | Compare(a, b) == 0
a != b  | Compare(a, b) != 0   | Compare(a, b) != 0
a > b   | Compare(a, b) == 1   | Compare(a, b) > 0
a < b   | Compare(a, b) == -1  | Compare(a, b) < 0
a >= b  | Compare(a, b) != -1  | Compare(a, b) >= 0
a <= b  | Compare(a, b) != 1   | Compare(a, b) <= 0
*/
int Compare(const double a, const double b = 0, const int e = 8)
{
	double diff = NormalizeDouble(a - b, e);
	
	if (diff > 0)
		return(1);
	else if (diff == 0)
		return(0);
	else
		return(-1);
}

/**
Поместить значение в указанный диапазон.

Можно использовать для ограничения входных параметров. См. также шаблон PUT_IN_RANGE(A, L, H).
Предполагается использование с числовыми значениями, работа с другими типами не проверялась.

@param value  Исходное значение.
@param from   Нижняя граница диапазона.
@param to     Верхняя граница диапазона.

@return       Значение, помещенное в указанный диапазон. Например, для PutInRange(5, 10, 20) 
              будет возвращено 10. Если параметр to меньше параметра from, значение value 
              будет возвращено без изменений.
*/
template <typename T>
T PutInRange(const T value, const T from, const T to)
{
	return(PUT_IN_RANGE(value, from, to));
}

/**
Вычислить медиану.

@param &arr[]    Массив чисел, для которых будет рассчитана медиана.
@param start     Стартовая позиция для расчета. Если не указано, расчет будет производиться с начала массива.
@param count     Количество элементов массива для расчета, начиная с позиции start. Если не указано, для расчета будет 
                 использованы все элементы массива, начиная с start.

@return          Медиана. Если массив пуст, либо пуста его часть, заданная дополнительными параметрами, будет возвращено NaN.
*/
double Median(const double &arr[], int start = 0, int count = -1)
{
	if (!ArrayCheckRange(arr, start, count))
		return(NAN);

	// Скопировать выбранные данные в отдельный массив, т.к. данные необходимо менять
	double selected[];
	ArrayResize(selected, count);
	ArrayCopy(selected, arr, 0, start, count);
	
	// Медиана - середина отсортированного массива
	ArraySort(selected);
	
	// При нечетном - центральный элемент, при четном - среднее двух центральных
	if (count % 2 == 1)
		return(selected[count / 2]);
	else
		return((selected[count / 2 - 1] + selected[count / 2]) / 2.0);
}

/**
Округлить число с указанной погрешностью.

Перегрузка встроенной функции.

@param value  Исходное значение.
@param error  Погрешность.

@return       Округленное с указанной погрешностью число. Например, MathRound(77, 5) = 75, 
              MathRound(8.88, 0.1)=8.9. Если error равно 0, значение возвращается без изменений.
*/
double MathRound(const double value, const double error)
{
	return(error == 0 ? value : MathRound(value / error) * error);
}


/**
Поменять местами два значения одного типа.

@param value1  Первое значение.
@param value2  Второе значение.
*/
template <typename T>
void Swap(T &value1, T &value2)
{
	T tmp = value1;
	value1 = value2;
	value2 = tmp;
}


/**
Суммировать элементы массива. Проверка на переполнение результата не делается.

@param &arr   Массив исходных чисел.
@param start  Начальный элемент в массиве для обработки.
@param count  Количество элементов для обрабтоки, -1 - от start до конца массива.

@return       Сумма элементов массива.
*/
template <typename T>
T Sum(const T &arr[], int start = 0, int count = -1)
{
	if (!ArrayCheckRange(arr, start, count))
		return((T)NULL);
	
	T sum = (T)NULL;

	for (int i = start, end = start + count; i < end; i++)
		sum += arr[i];
	
	return(sum);
}


/**
Вычислить арифметическое среднее.

@param &arr[]    Массив исходных чисел.
@param start     Стартовая позиция для расчета. Если не указано, расчет будет производиться с начала массива.
@param count     Количество элементов массива для расчета, начиная с позиции start. Если не указано, для расчета будет 
                 использованы все элементы массива, начиная с позиции start.

@return          Среднее арифметическое. Если массив пуст, либо пуста его часть, заданная дополнительными параметрами, будет возвращено NaN.
*/
double Mean(const double &arr[], int start = 0, int count = -1)
{
	if (!ArrayCheckRange(arr, start, count))
		return(NAN);

	double sum = 0;

	for (int i = start, end = start + count; i < end; i++)
		sum += arr[i];

	return(sum / count);
}


/**
Вычислить стандартное отклонение.

@param &arr[]    Массив исходных чисел.
@param start     Стартовая позиция для расчета. Если не указано, расчет будет производиться с начала массива.
@param count     Количество элементов массива для расчета, начиная с позиции start. Если не указано, для расчета будет 
                 использованы все элементы массива, начиная с позиции start.

@return          Стандартное отклонение. Если массив пуст, либо пуста его часть, заданная дополнительными параметрами, будет возвращено NaN.
*/
double StdDev(const double &arr[], int start = 0, int count = -1)
{
	if (!ArrayCheckRange(arr, start, count))
		return(NAN);
	
	double mean = Mean(arr, start, count);
	double dev = 0;
	
	for (int i = start, end = start + count; i < end; i++)
		dev += MathPow(arr[i] - mean, 2);
	
	dev = MathSqrt(dev / count);
	
	return(dev);
}

/**
Вычислить стандартную оценку.

@param &arr[]    Массив исходных чисел.
@param value     Оцениваемое значение.
@param start     Стартовая позиция для расчета. Если не указано, расчет будет производиться с начала массива.
@param count     Количество элементов массива для расчета, начиная с позиции start. Если не указано, для расчета будет 
                 использованы все элементы массива, начиная с позиции start.

@return          Стандартная оценка (Standard Score). Если массив пуст, либо пуста его часть, заданная дополнительными параметрами, будет возвращено NaN.
                 Значение NaN также будет возвращено, если вычисляемое в процессе стандартное отклонение будет равно 0.
*/
double StdScore(const double &arr[], const double value, int start = 0, int count = -1)
{
	if (!ArrayCheckRange(arr, start, count))
		return(NAN);
	
	double mean = Mean(arr, start, count);
	double dev = 0;
	
	for (int i = start, end = start + count; i < end; i++)
		dev += MathPow(arr[i] - mean, 2);
	
	dev = MathSqrt(dev / count);

	if (dev == 0)
		return(NAN);
		
	return((value - mean) / dev);
}

