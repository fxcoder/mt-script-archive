/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Функции работы с цветом.
*/


#include "math.mqh"
#include "..\define\color.mqh"


/**
Разложить цвет на компоненты.

@param c  Исходный цвет
@param r  Результат: красная составляющая
@param g  Результат: зеленая составляющая
@param b  Результат: синяя составляющая

@return   Успех операции. Разложение не может быть осуществлено для неопределенного цвета, параметры r, g и b при 
          этом остаются без изменений.
*/
bool ColorToRGB(const color c, int &r, int &g, int &b)
{
	// Если цвет задан неверный, либо задан как отсутствующий, вернуть false
	if (COLOR_IS_NONE(c))
		return(false);
	
	b = (c & 0xFF0000) >> 16;
	g = (c & 0x00FF00) >> 8;
	r = (c & 0x0000FF);
	
	return(true);
}

/**
Собрать цвет из составляющих. См. также шаблон RGB_TO_COLOR(R, G, B).

@param r  Красная составляющая
@param g  Зеленая составляющая
@param b  Синяя составляющая

@return   Цвет-результат.
*/
color RGBToColor(const int r, const int g, const int b)
{
	return(RGB_TO_COLOR(r, g, b));
}

/**
Смешать цвета в заданных пропорциях.

Терминал поддерживает только ограниченное число цветов, поэтому использование всей палитры RGB приводит к ошибкам 
в отображении не только индикатора, но и самого графика. Для ограничения палитры введен параметр шага.

@param color1  Цвет 1
@param color2  Цвет 2
@param mix     Пропорция смешивания (0..1). При mix=0 получается цвет 1, при mix=1 получается цвет 2. Если значение
               выходит за рамки допустимого, оно будет соответственно скорректировано.
@param step    Шаг цвета, огрубление (1..255). Если значение выходит за рамки допустимого, оно будет соответственно 
               скорректировано.

@return        Цвет - результат смешения.
*/
color MixColors(const color color1, const color color2, double mix, double step = 16.0)
{
	// Коррекция неверных параметров
	step = PUT_IN_RANGE(step, 1.0, 255.0);
	mix = PUT_IN_RANGE(mix, 0.0, 1.0);
		
	int r1, g1, b1;
	int r2, g2, b2;

	// Разбить на компоненты	
	ColorToRGB(color1, r1, g1, b1);
	ColorToRGB(color2, r2, g2, b2);

	// вычислить
	int r = PUT_IN_RANGE((int)MathRound(r1 + mix * (r2 - r1), step), 0, 255);
	int g = PUT_IN_RANGE((int)MathRound(g1 + mix * (g2 - g1), step), 0, 255);
	int b = PUT_IN_RANGE((int)MathRound(b1 + mix * (b2 - b1), step), 0, 255);

	return(RGB_TO_COLOR(r, g, b));
}

/**
Инвертировать цвет. См. также шаблон COLOR_INVERT(C)

@param c  Исходный цвет.

@return   Цвет - результат инверсии.
*/
color InvertColor(const color c)
{
	return(COLOR_INVERT(c));
}

/**
Проверить, является ли цвет неопределенным. См. также шаблон COLOR_IS_NONE(C).

@param c  Цвет для проверки

@return   true, если цвет не определен, false, если определен.
*/
bool ColorIsNone(const color c)
{
	return(COLOR_IS_NONE(c));
}

