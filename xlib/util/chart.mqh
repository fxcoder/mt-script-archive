/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Функции для работы с графиком.
*/


/**
Получить шаг баров (расстояние между соседним барами) в пикселах.

@return  Шаг баров в пикселах. Возможные значения: 1, 2, 4, 8, 16, 32. При неопределённом результате возвращается 1.
*/
int GetBarStep(const long chartId = 0)
{
	long chartScale;
	
	if (!ChartGetInteger(chartId, CHART_SCALE, 0, chartScale))
		return(1);
	
	return((int)MathPow(2, chartScale));
}

/**
Получить ширину баров для указанного графика.

Ширина возвращается в условных единицах, указываемых для линий индикатора. В масштабах с 0 по 3 эта единица равна 1 пикселу, 
для остальных - 2 пиксела.

@return  Ширина баров. Возможные значения: 1, 2, 3, 6, 12. При неопределённом результате возвращается 0.
*/
int GetBarBodyWidth(const long chartId = 0)
{
	long chartScale;
	
	if (!ChartGetInteger(chartId, CHART_SCALE, 0, chartScale))
		return(0);
	
	if (chartScale == 0)
		return(1);
	
	if (chartScale == 1)
		return(1);
	
	if (chartScale == 2)
		return(2);
	
	if (chartScale == 3)
		return(3);
	
	if (chartScale == 4)
		return(6);
	
	if (chartScale == 5)
		return(12);
	
	return(0);
}


