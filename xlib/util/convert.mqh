/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Функции преобразования.
*/


#include "string.mqh"
#include "..\define\convert.mqh"


/**
Преобразовать число (double) в строку с указанием знаков и разделителя целой и дробной части.

@param d          Число
@param digits     Количество знаков после запятой
@param separator  Разделитель целой и дробной частей

@return           Число в строковом формате.
*/
string DoubleToString(const double d, const uint digits, const uchar separator)
{
	string s = DoubleToString(d, digits) + ""; //HACK: без +"" функция может вернуть пустое значение (билд 697)
	
	//TODO: сравнить скорость со StringReplace
	if (separator != '.')
	{
		int p = StringFind(s, ".");
		
		if (p != -1)
			StringSetCharacter(s, p, separator);
	}
	
	return(s);
}


/**
Компактно преобразовать число (double) в строку с указанием знаков и разделителя целой и дробной части.

Компактность достигается за счет удаление лишних завершающих нулей в дробной части. Может быть удобно для показа 
числовых данных пользователю, когда нужна максимальная точность, но без отображения лишней информации.

@param d          Число
@param digits     Количество знаков после запятой
@param separator  Разделитель целой и дробной частей

@return           Число в строковом формате без лишних завершающих нулей в дробной части.
*/
string DoubleToCompactString(const double d, const uint digits = 8, const uchar separator = '.')
{
	string s = DoubleToString(d, digits, separator);
	
	// убрать нули в конце дробной части
	if (StringFind(s, CharToString(separator)) != -1)
	{
		s = TrimRight(s, '0');
		s = TrimRight(s, '.');
	}

	return(s);
}

/**
Преобразовать время в строку по заданному шаблону.

@param date    Дата
@param format  Шаблон. Можно использовать следующие подстановки:
                 - yy - год (2 знака),
                 - yyyy - год (4 знака), 
                 - MM - месяц (2 знака), 
                 - dd - день (2 знака), 
                 - HH - час (2 знака, 24-часовой формат), 
                 - mm - минута (2 знака), 
                 - ss - секунда (2 знака).

@return        Время в указанном формате.

@code
// преобразование в американский формат
string amTime = TimeToString(D'2011.01.15', "MM/dd/yyyy"); // 01/15/2011

// преобразование в российский формат
string ruTime = TimeToString(D'2011.01.15', "dd.MM.yyyy"); // 15.01.2011
@endcode
*/
string TimeToString(const datetime date, const string format)
{
	// пример: 18.03.2011 15:20:55
	//yy    11
	//yyyy  2011
	//MM    03
	//dd    18
	//HH    15
	//mm    20
	//ss    55

	// Формат по умолчанию: yyyy.MM.dd HH:mm:ss
	string s = TimeToString(date, TIME_DATE | TIME_MINUTES | TIME_SECONDS);
	string res;

	// для часто используемых форматов использовать быстрое преобразование
	if (format == "MM/dd/yyyy")
	{
		// американский формат
		res = StringSubstr(s, 5, 2) + "/" + StringSubstr(s, 8, 2) + "/" + StringSubstr(s, 0, 4);
	}
	else if (format == "dd.MM.yyyy")
	{
		// русский формат
		res = StringSubstr(s, 8, 2) + "." + StringSubstr(s, 5, 2) + "." + StringSubstr(s, 0, 4);
	}
	else if (format == "yyyy-MM-dd")
	{
		// удобный для сортировки
		res = StringSubstr(s, 0, 4) + "-" + StringSubstr(s, 5, 2) + "-" + StringSubstr(s, 8, 2);
	}
	else
	{
		res = format;
		StringReplace(res, "yyyy", StringSubstr(s, 0, 4));
		StringReplace(res, "yy", StringSubstr(s, 2, 2));
		StringReplace(res, "MM", StringSubstr(s, 5, 2));
		StringReplace(res, "dd", StringSubstr(s, 8, 2));
		StringReplace(res, "HH", StringSubstr(s, 11, 2));
		StringReplace(res, "mm", StringSubstr(s, 14, 2));
		StringReplace(res, "ss", StringSubstr(s, 17, 2));
	}
	
	return (res);
}

/**
Преобразовать период (таймфрейм) в строковое представление.

@param tf  Таймфрейм. Если не указан, используется текущий.

@return    Строковое представление таймфрейма. Могут быть использованы таймфреймы, отсутствующие в терминале.
*/
string PeriodToString(const ENUM_TIMEFRAMES tf = PERIOD_CURRENT)
{
	int period = PeriodSeconds(tf) / 60;
	
	if (period % 43200 == 0)
		return("MN" + IntegerToString(period / 43200));
	else if (period % 10080 == 0)
		return("W" + IntegerToString(period / 10080));
	else if (period % 1440 == 0)
		return("D" + IntegerToString(period / 1440));
	else if (period % 60 == 0)
		return("H" + IntegerToString(period / 60));
	else
		return("M" + IntegerToString(period));
}


/**
Преобразовать период (таймфрейм) в строковое представление. Дополнительный вариант для MQL4.

@param tf  Таймфрейм. Если не указан, используется текущий.

@return    Строковое представление таймфрейма. Могут быть использованы таймфреймы, отсутствующие в терминале.
*/
string PeriodToString(const int period = 0)
{
	if (period % 43200 == 0)
	{
		int months = period / 43200;
		return((months == 1) ? "Monthly" : "MN" + IntegerToString(months));
	}
	else if (period % 10080 == 0)
	{
		int weeks = period / 10080;
		return((weeks == 1) ? "Weekly" : "W" + IntegerToString(weeks));
	}
	else if (period % 1440 == 0)
	{
		int days = period / 1440;
		return((days == 1) ? "Daily" : "D" + IntegerToString(days));
	}
	else if (period % 60 == 0)
	{
		return("H" + IntegerToString(period / 60));
	}
	else
	{
		return("M" + IntegerToString(period));
	}
}


/**
Преобразовать минуту дня в текстовое представление времени.

Примеры преобразования:
Минута | Текстовое представление
------:|-------------------------
     5 | "00:05"
    -5 | "23:55"
     0 | "00:00"
   725 | "12:05"
   789 | "13:09"

@param minute  Минута дня. Можно использовать как положительные значения (от полуночи вперед), так и отрицательные (от полуночи назад).

@return        Текстовое представление времени.
*/
string MinuteToString(const int minute)
{
	// Обрезать лишние дни
	int dayMinute = minute % 1440;
	
	// Если минута указана от полуночи назад, то прибавить сутки
	if (dayMinute < 0)
		dayMinute += 1440;
	
	// Разбить на составляющие
	int h = dayMinute / 60;
	int m = dayMinute % 60;

	return((h < 10 ? "0" : "") + IntegerToString(h) + ":" + (m < 10 ? "0" : "") + IntegerToString(m));
}


/**
Преобразовать целое число в шестнадцатеричное представление.

Например, 123456789 будет преобразовано в "075BCD15".

@param n  Число для преобразования. Допускается весь диапазон чисел типа int, включая отрицательные значения.

@return   Строка - шестнадцатеричное представление указанного числа.
*/
string IntToHex(const int n)
{
	string res = "";
	
	for (int i = 28; i >= 0; i -= 4)
	{
		uchar digit = (uchar)((n & (0xf << i)) >> i);
		
		if (digit > 9)
			StringAdd(res, CharToString((uchar)('A' - 10 + digit)));
		else
			StringAdd(res, CharToString((uchar)('0' + digit)));
	}
	
	return(res);
}

/**
Преобразовать шестнадцатеричное представление числа в целое.

@param hex  Строка для преобразования. Допускается весь диапазон чисел типа int, включая отрицательные значения.
@param res  Результат преобразования.

@return     Успех операции. Не каждая строка может быть преобразована.
*/
bool HexToInt(const string hex, int &res)
{
	// убрать нули слева и поднять регистр
	string hexNormalized = UpperString(TrimLeft(Trim(hex), '0'));
	
	// не влезет в int
	int len = StringLen(hexNormalized);
	
	if (len > 8)
		return(false);

	res = 0;
	
	for (int i = 0; i < len; i++)
	{
		res = res << 4;
		
		ushort ch = StringGetCharacter(hexNormalized, i);
		
		if ((ch >= '0') && (ch <= '9'))
			res = res | (ch - '0');
		else if ((ch >= 'A') && (ch <= 'F'))
			res = res | (10 + ch - 'A');
		else
			return(false);
	}
	
	return(true);
}


/**
Проверить, является ли строка строковым представлением целого числа (int).

@param s  Строка для проверки.

@return   Результат проверки - true/false.
*/
bool StringIsInteger(const string s)
{
	int len = StringLen(s);

	if (len == 0)
		return(false);

	ushort ch = StringGetCharacter(s, 0);
	
	// начальная позиция сканирования на цифры
	int start = ((ch == '-') || (ch == '+')) ? 1 : 0;
	int digitCount = len - start;
	
	if ((digitCount > 10) || (digitCount == 0))
		return(false);
		
	for (int i = start; i < len; i++)
	{
		ch = StringGetCharacter(s, i);
		
		if ((ch < '0') || (ch > '9'))
			return(false);
	}		

	return(true);
}


/**
Преобразовать используемую цену в строковое представление.

Функция может быть полезна при отображении параметров пользователю.

@param ap       Используемая цена (applied price). Диапазон: от PRICE_CLOSE до PRICE_WEIGHTED.
@param verbose  Выводить словесное представление.

@return         Строковое представление используемой цены.
                ap             | Результат (verbose=false) | Результат (verbose=true)
                ---------------|---------------------------|-------------
                PRICE_CLOSE    | "C"                       | "Close"
                PRICE_OPEN     | "O"                       | "Open"
                PRICE_HIGH     | "H"                       | "High"
                PRICE_LOW      | "L"                       | "Low"
                PRICE_MEDIAN   | "HL"                      | "Median"
                PRICE_TYPICAL  | "HLC"                     | "Typical"
                PRICE_WEIGHTED | "HLCC"                    | "Weighted"
                Остальные      | "?"                       | "Unknown"
*/
string AppliedPriceToString(const ENUM_APPLIED_PRICE ap, const bool verbose = false)
{
	if (verbose)
	{
		switch (ap)
		{
			case PRICE_CLOSE: return("Close");
			case PRICE_OPEN: return("Open");
			case PRICE_HIGH: return("High");
			case PRICE_LOW: return("Low");
			case PRICE_MEDIAN: return("Median");
			case PRICE_TYPICAL: return("Typical");
			case PRICE_WEIGHTED: return("Weighted");
			default: return("Unknown");
		}
	}
	else
	{
		switch (ap)
		{
			case PRICE_CLOSE: return("C");
			case PRICE_OPEN: return("O");
			case PRICE_HIGH: return("H");
			case PRICE_LOW: return("L");
			case PRICE_MEDIAN: return("HL");
			case PRICE_TYPICAL: return("HLC");
			case PRICE_WEIGHTED: return("HLCC");
			default: return("?");
		}
	}
}

/**
Преобразовать тип средней в строковое представление.

Функция может быть полезна при отображении параметров пользователю. 

@param maMethod  Тип средней. Диапазон: от MODE_SMA до MODE_LWMA.

@return          Строковое представление типа средней.
                 maMethod           | Результат
                 -------------------|-------------
                 0, MODE_SMA        | "SMA"
                 1, MODE_EMA        | "EMA"
                 2, MODE_SMMA       | "SMMA"
                 3, MODE_LWMA       | "LWMA"
                 Остальные значения | "MA(type=#)" 
*/
string MaMethodToString(const ENUM_MA_METHOD maMethod)
{
	switch (maMethod)
	{
		case MODE_SMA: return("SMA");
		case MODE_EMA: return("EMA");
		case MODE_SMMA: return("SMMA");
		case MODE_LWMA: return("LWMA");
		default: return("MA(type " + IntegerToString(maMethod) +")");
	}
}


/**
Преобразовать таймфрейм в минуты. См. также шаблон PERIOD_TO_MINUTES(P).

@param period  Таймфрейм.

@return        Количество минут в таймфрейме.

@deprecateed   См. PeriodMinutes()
*/
int PeriodToMinutes(const ENUM_TIMEFRAMES period)
{
	return(PERIOD_TO_MINUTES(period));
}


/**
Преобразовать таймфрейм в минуты. См. также шаблон PERIOD_TO_MINUTES(P).

@param period  Таймфрейм.

@return        Количество минут в таймфрейме.
*/
int PeriodMinutes(const ENUM_TIMEFRAMES period = PERIOD_CURRENT)
{
	return(PERIOD_TO_MINUTES(period));
}


/**
Преобразовать минуты в таймфрейм.

@param minutes  Количество минут.
@param period   Таймфрейм.

@return         Успех операции. Есть конечное число таймфреймов, и не каждое число минут может быть
                преобразовано в один из стандартных таймфреймов. В MQL4 дополнительные таймфреймы
                здесь не испульзуются.
*/
bool MinutesToPeriod(const int minutes, ENUM_TIMEFRAMES &period)
{
	switch (minutes)
	{
		case 1:
			period = PERIOD_M1;
			return(true);
			
		case 5:
			period = PERIOD_M5;
			return(true);
			
		case 15:
			period = PERIOD_M15;
			return(true);
			
		case 30:
			period = PERIOD_M30;
			return(true);
			
		case 60:
			period = PERIOD_H1;
			return(true);
			
		case 240:
			period = PERIOD_H4;
			return(true);
			
		case 1440:
			period = PERIOD_D1;
			return(true);
			
		case 10080:
			period = PERIOD_W1;
			return(true);
			
		case 43200:
			period = PERIOD_MN1;
			return(true);
			
		
#ifndef __MQL4__

		case 2:
			period = PERIOD_M2;
			return(true);
			
		case 3:
			period = PERIOD_M3;
			return(true);
			
		case 4:
			period = PERIOD_M4;
			return(true);
			
		case 6:
			period = PERIOD_M6;
			return(true);
			
		case 10:
			period = PERIOD_M10;
			return(true);
			
		case 12:
			period = PERIOD_M12;
			return(true);
			
		case 20:
			period = PERIOD_M20;
			return(true);
			
		case 120:
			period = PERIOD_H2;
			return(true);
			
		case 180:
			period = PERIOD_H3;
			return(true);
			
		case 360:
			period = PERIOD_H6;
			return(true);
			
		case 480:
			period = PERIOD_H8;
			return(true);
			
		case 720:
			period = PERIOD_H12;
			return(true);
		
#endif

	}
	
	return(false);
}

