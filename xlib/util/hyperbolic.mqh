/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Гиперболические функции.

@par Неопределенный результат

Некоторые функции могут возвращать неопределенное значение (NaN). Необходимо проверять аргумент перед передачей 
его в функцию, либо проверять результат на неопределенность с помощью функции IsNaN().

Вариант 1

@code
double x = 0;

if (x < 1)
    Print("Неверный аргумент, функция не может быть рассчитана.");
else
    Print("Arcosh(", x, ") = ", Arcosh(x));
@endcode

Вариант 2

@code
double x = 0;
double y = Arcosh(x);

if (MathIsValidNumber(y))
    Print("Arcosh(", x, ") = ", y);
else
    Print("Неверный аргумент, функция вернула неопределенный результат.");
@endcode
*/


#include "math.mqh"


#ifdef __MQL4__

/**
@param x  Аргумент.

@return   Гиперболический синус.
*/
double MathSinh(const double x)
{
	return((MathExp(x) - MathExp(-x)) / 2.0);
}

/**
@param x  Аргумент.

@return   Гиперболический косинус.
*/
double MathCosh(const double x)
{
	return((MathExp(x) + MathExp(-x)) / 2.0);
}

/**
@param x  Аргумент.

@return   Гиперболический тангенс.
*/
double MathTanh(const double x)
{
	double e2x = MathExp(2.0 * x);
	return((e2x - 1.0) / (e2x + 1.0));
}

#endif 



/**
@param x  Аргумент.

@return   Гиперболический котангенс.
*/
double MathCoth(const double x)
{
	double e2x = MathExp(2.0 * x);
	return((e2x + 1.0) / (e2x - 1.0));
}

/**
@param x  Аргумент.

@return   Гиперболический секанс.
*/
double MathSech(const double x)
{
	return(2.0 / (MathExp(x) + MathExp(-x)));
}

/**
@param x  Аргумент.

@return   Гиперболический косеканс.
*/
double MathCsch(const double x)
{
	return(2.0 / (MathExp(x) - MathExp(-x)));
}

/**
@param x  Аргумент.

@return   Гиперболический арксинус.
*/
double MathArsinh(const double x)
{
	return(MathLog(x + MathSqrt(x * x + 1.0)));
}

/**
@param x  Аргумент.

@return   Гиперболический арккосинус. Если x < 1, будет возвращен неопределенный результат (NaN).
*/
double MathArcosh(const double x)
{
	return x >= 1.0 ? MathLog(x + MathSqrt(x * x - 1.0)) : NAN;
}

/**
@param x  Аргумент.

@return   Гиперболический арктангенс. Если |x| >= 1, будет возвращен неопределенный результат (NaN).
*/
double MathArtanh(const double x)
{
	return(MathAbs(x) < 1.0 ? 0.5 * MathLog((1.0 + x) / (1.0 - x)) : NAN);
}

/**
@param x  Аргумент.

@return   Гиперболический арккотангенс. Если |x| <= 1, будет возвращен неопределенный результат (NaN).
*/
double MathArcoth(const double x)
{
   	return(MathAbs(x) > 1.0 ? 0.5 * MathLog((x + 1.0) / (x - 1.0)) : NAN);
}

/**
@param x  Аргумент.

@return   Гиперболический арксеканс. Если x <= 0 или x > 1 будет возвращен неопределенный результат (NaN).
*/
double MathArsech(const double x)
{
  	return(((x > 0.0) && (x <= 1.0)) ? MathLog((1.0 + MathSqrt(1.0 - x * x)) / x) : NAN);
}

/**
@param x  Аргумент.

@return   Гиперболический арккосеканс. Если x = 0, будет возвращен неопределенный результат (NaN).
*/
double MathArcsch(const double x)
{
	return(x != 0.0 ? MathLog(1.0 / x + MathSqrt(1.0 + x * x) / MathAbs(x)) : NAN);
}

