/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Функции для работы с идентификаторами.
*/


/**
Получить уникальный в пределах терминала целочисленный идентификатор.

Для проверки на уникальность используется функция GetMicrosecondCount(). При интенсивном
использовании возможны коллизии. Значения повторяются примерно каждые пол часа.

@return  Новый уникальный идентификатор. Может быть 0.
*/
int GetUID()
{
	return((int)(GetMicrosecondCount() & 0x7FFFFFFF));
}

/**
Получить уникальный в пределах терминала целочисленный идентификатор в виде
цифро-буквенного набора.

См. также GetUID().

@return  Новый уникальный идентификатор. Может быть пустой строкой.
*/
string GetUIDString(int uid = -1)
{
	// 31 бит
	uint id = uid < 0 ? GetUID() : uid;
	
	// 64-символьная строка (6 бит)
	string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.,";
	string result = "";

	// разбор числа на 6-битные символы
	for (int i = 0; i < 6; i++)
	{
		// если слева больше нет значащих бит, закончить, на коллизии это не повлияет
		if (id == 0)
			break;
		
		uchar ch = (uchar)StringGetCharacter(chars, id & 0x3F);
		result = CharToString(ch) + result;
		
		id = id >> 6;
	}
	
	return(result);
}

