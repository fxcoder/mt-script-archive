/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Функции работы с часовыми поясами.
*/

#include "time.mqh"

/**
Проверить, находится ли указанное время в периоде, когда на 1-3 недели меняется разница 
часовых поясов между Америкой и Европой при переходе на летнее время весной.

В такие периоды американские торговые сессии начинаются на час раньше относительно 
европейских. Не учитываются нюансы со временем и даже днями недели, расчет только на 
рабочие дни.

Переход на летнее время весной:
  - Америка: второе воскресенье марта в 2:00 (на час вперед)
  - Европа: последнее воскресенье марта в 2:00 (на час вперед)

@param time  Время, которое нужно проверить на сдвиг

@return      true, если время находится в периоде весеннего сдвига часовых поясов, false в обычное время.
*/
bool TimeInSpringShift(const datetime time)
{
	MqlDateTime mt;
	TimeToStruct(time, mt);
	
	if (mt.mon != 3)
		return(false);
	
	// 1 марта
	MqlDateTime mar1Date;
	TimeToStruct(D'2000.03.01', mar1Date);
	mar1Date.year = mt.year;
	TimeUpdateStruct(mar1Date);

	// второе воскресенье
	datetime secondSun;
	
	if (mar1Date.day_of_week == 0)
		secondSun = StructToTime(mar1Date) + 86400;
	else
		secondSun = StructToTime(mar1Date) + (14 - mar1Date.day_of_week) * 86400;

	if (time < secondSun)
		return(false);
		
	// 31 марта
	MqlDateTime mar31Date;
	TimeToStruct(D'2000.03.31', mar31Date);
	mar31Date.year = mt.year;
	TimeUpdateStruct(mar31Date);
	
	// последнее воскресенье
	datetime lastSun = StructToTime(mar31Date) - mar31Date.day_of_week * 86400;
	
	return(time < lastSun);
}


/**
Проверить, находится ли указанное время в периоде, когда на 1 неделю меняется разница 
часовых поясов между Америкой и Европой при переходе на зимнее время осенью.

В такие периоды американские торговые сессии начинаются на час раньше относительно 
европейских. Не учитываются нюансы со временем и даже днями недели, расчет только на 
рабочие дни.

Переход на зимнее время осенью:
  - Америка: первое воскресенье ноября в 2:00 (на 1 час назад)
  - Европа: последнее воскресенье октября в 3:00 (на 1 час назад)

@param time  Время, которое нужно проверить на сдвиг

@return      true, если время находится в периоде осеннего сдвига часовых поясов, false в обычное время.
*/
bool TimeInAutumnShift(const datetime time)
{
	MqlDateTime mt;
	TimeToStruct(time, mt);
	
	if ((mt.mon != 10) && (mt.mon != 11))
		return(false);
	
	// первое воскресеньше ноября

	// 1 ноября
	MqlDateTime nov1Date;
	TimeToStruct(D'2000.11.01', nov1Date);
	nov1Date.year = mt.year;
	TimeUpdateStruct(nov1Date);

	// первое воскресенье
	datetime firstSun;
	
	if (nov1Date.day_of_week == 0)
		firstSun = StructToTime(nov1Date);
	else
		firstSun = StructToTime(nov1Date) + (7 - nov1Date.day_of_week) * 86400;
	
	if (time >= firstSun)
		return(false);

	// последнее воскресенье октября
	// 31 октября
	datetime oct31Date = StructToTime(nov1Date) - 86400;
	int oct31dow = nov1Date.day_of_week - 1;
	
	if (oct31dow < 0)
		oct31dow = 6;

	// последнее воскресенье
	datetime lastSun = oct31Date - oct31dow * 86400;
	
	return(time >= lastSun);
}

