/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Класс ведения лога.
*/


#include "..\util\array.mqh"

/**
Класс для ведения лога.

Лог имеет ограниченное количество строк (указывается при инициализации), самые старые записи заменяются новыми.
Можно использовать для отладки, когда требуется отслеживать промежуточные действия.

@par Пример: вывод времени последнего тика

@code
XLog* _log;

int OnInit()
{
    // Инициализировать модуль лога
	_log = new XLog(30);
    _log.Add("Модуль лога инициализирован.");
...
}

int OnCalculate(...)
{
    // добавить в лог время последнего тика и вывести лог в комментарий
    _log.Show("Время последнего тика: " + TimeToStr(TimeCurrent()));
...
}

void OnDeinit(...)
{
	delete(_log);
}
@endcode
*/
class XLog
{
	/** Строки лога */
	private: string _lines[];
	
	/** Временные отметки лога */
	private: datetime _times[];
	
	/** Количества повторений */
	private: int _counts[];
	
	/** Индекс последней записи лога */
	private: int _index;
	
	/** Количество эаписей в логе */
	private: int _count;
	
	/** Использовать локальное время */
	private: bool _useLocalTime;
	
	/** Все строки уникальны */
	private: bool _unique;
	
	
	/**
	Конструктор.
	
	@param lineCount     Количество строк в логе. Не может быть больше 1. Если указано меньше, используется 1.
	@param useLocalTime  Использовать локальное время для событий вместо последнего времени сервера.
	@param unique        Добавлять только уникальные строки. Неуникальные будут показаны с количеством повторов.
	*/
	public: void XLog(const int lineCount, const bool useLocalTime = false, const bool unique = false)
	{
		_count = lineCount > 1 ? lineCount : 1;
		_useLocalTime = useLocalTime;
		_unique = unique;
		
		ArrayResize(_lines, _count);
		ArrayResize(_times, _count);
		ArrayResize(_counts, _count);
		this.Clear();
	}
	
	
	/**
	Добавить событие в лог.
	
	Если строк не хватает, затирается самое старое событие.
	
	@param line    Новая строка лога.
	*/
	public: void Add(const string line)
	{
		datetime lineTime = _useLocalTime ? TimeLocal() : TimeCurrent();
			
		if (_unique || (line != _lines[_index]))
		{
			_index++;
			
			if (_index > _count - 1)
				_index = 0;
	
			_times[_index] = lineTime;
			_lines[_index] = line;
			_counts[_index] = 1;
		}
		else
		{
			_times[_index] = lineTime;
			_counts[_index]++;
		}
	}
	
	
	/**
	Получить весь текст лога.
	
	Старые события в начале, новые в конце. Строки разделяются символом новой строки. Можно использовать для вывода лога в 
	комментарий.
	
	@return  Лог одной строкой с разделителями строк.
	*/
	public: string GetText()
	{
		string res = "";
		
		for (int i = 1; i <= _count; i++)
		{
			int j = (i + _index) % _count;
			
			if (_lines[j] != "")
			{
				string s = TimeToString(_times[j], TIME_MINUTES | TIME_SECONDS) + "  " + _lines[j];
				
				if (_counts[j] > 1)
					s = s + "  [" + IntegerToString(_counts[j]) + "]";
					
				res = res + s + "\n";
			}
		}
		
		return (res);
	}
	
	
	/**
	Очистить лог.
	*/
	public: void Clear()
	{
		ArrayInitialize(_lines, "");
		ArrayInitialize(_times, 0);
		ArrayInitialize(_counts, 0);
		_index = 0;
	}
	
	
	/**
	Добавить событие в лог (опция) и вывести лог в комментарий графика.
	
	Если строк не хватает, затирается самая старая строка.
	
	@param line    Новая строка лога. Если не указана, то будет выведено то, что добавлено ранее.
	*/
	public: void Show(const string line = "")
	{
		if (line != "")
			this.Add(line);
			
		Comment(this.GetText());
	}
};	

