/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Класс для работы с сетью.
*/


#include "..\define\inet.mqh"
#include "..\import\wininet.mqh"


/**
Класс для работы с сетью.

Используется системная библиотека wininet.dll, поэтому в терминале необходимо разрешить вызов DLL.
*/
class XInet
{
	/** Код сессии */
	private: int _sessionHandle;
	
	/** Пауза между загрузками кусков в миллисекундах */
	private: int _loadPause;
	
	
	/**
	Конструктор
	
	@param loadPause  Задержка в миллисекундах между загрузками кусков данных. В некоторых случаях необходимо ненулевое значение.
	
	@return           Экземпляр класса
	*/
	public: void XInet(const int loadPause = 0)
	{
		_sessionHandle = 0;
		_loadPause = loadPause;
	}
	
	
	/**
	Закрыть сетевое соединение.
	
	Необходимо использовать при завершении работы скрипта.
	*/
	public: void Close()
	{
		if (_sessionHandle > 0)
		{
			InternetCloseHandle(_sessionHandle);
			_sessionHandle = 0;
		}
	}


	/**
	Открыть сетевое соединение.
	
	Необходимо перед началом работы с сетью.
	
	@param userAgent  Идентификатор клиента.
	
	@return           Успех операции.
	*/
	public: bool Open(string userAgent = "Mozilla/4.0 (MSIE 6.0; Windows NT 5.1)")
	{
		string proxyName = "";
		string proxyBypass = "";
		_sessionHandle = InternetOpenW(userAgent, INET__IOT_DIRECT, proxyName, proxyBypass, 0);
		return(_sessionHandle > 0);
	}


	/**
	Проверить готовность сессии. 
	
	Сессия готова к использованию, если ранее успешно была выполнена команда Open().
	
	@return Готовность сети, true - да, false - нет.
	*/
	public: bool IsReady() const
	{
		return(_sessionHandle > 0);
	}
	
	
	/**
	Возвращает идентификатор сессии.
	
	@return Идентификатор сессии
	*/
	public: int GetSessionHandle() const
	{
		return(_sessionHandle);
	}
	
	
	/**
	Загрузить строки.
	
	Загружает информацию (предположительно текстовую) в массив строк и возвращает их количество.
	
	@param url        Адрес загрузки.
	@param strings[]  Ссылка на массив строк, в который будут загружены данные.
	
	@return           Количество загруженных строк. Загруженные строки передаются по ссылке в параметрах.
	*/
	public: int DownloadStrings(string url, string &strings[]) const
	{
		// проверить возможность оиспользования DLL
		if(!MQLInfoInteger(MQL_DLLS_ALLOWED))
			return(0);
	
		if (!IsReady())
			return(0);

		string headers = "";
	
		// Несмотря на указанные параметры, в W7+IE9 (остальное не тестировалось) все равно 
		// используется кэш IE, при этом запросы выполняются очень медленно
		int hURL = InternetOpenUrlW(_sessionHandle, url, headers, 0, INET__IF_NO_CACHE_WRITE | INET__IF_PRAGMA_NOCACHE | INET__IF_RELOAD, 0);
		
		if (hURL <= 0)
			return(0);
	
		// 255 байт - буфер для приема данных
		uchar buffer[255];
		int bytesRead = 0;
	
		int bufferSize = 255;
	
		// потоковое чтение и преобразование в массив строк
		int count = 0;
		int ac = ArrayResize(strings, 100);
		string s = ""; // потоковые данные
		int c = 0; // размер потока
		int j = 0; // индекс потока
	
		while (!IsStopped())
		{
			c = StringLen(s);
			
			if ((c == 0) || (j == c))
			{
				// подгружаем строку
				int res = InternetReadFile(hURL, buffer, bufferSize, bytesRead);
	
				if (bytesRead == 0)
					break;
					
				s = s + CharArrayToString(buffer, 0, bytesRead);
				c = StringLen(s);
			}
			
			// продолжаем разбор строки
			if ((c > 0) && (j != c))
			{
				if (StringGetCharacter(s, j) == 10)
				{
					count++;
	
					if (ac < count)
						ac = ArrayResize(strings, ac + 100);
	
					strings[count - 1] = StringSubstr(s, 0, j);
					
					s = StringSubstr(s, j + 1);
					j = 0;
				}
			}
			else
			{
				break;
			}
	
			j++;
	
			// Пауза между приемами блоков, без нее на некоторых системах бывают ошибки при загрузке
			if (_loadPause > 0)
				Sleep(_loadPause);
		}
	
		// последняя строка
		if (s != "")
		{
			count++;
	
			if (count != ac)
				ArrayResize(strings, count);
	
			strings[count-1] = s;
		}
		else
		{
			ArrayResize(strings, count);
		}
	
		return(count);
	}
	
};
