/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Класс касания.
*/


#include "..\util\array.mqh"
#include "..\util\math.mqh"


/**
Класс для определения касания уровней.

@par Пример: открытие позиции на отскоке от уровня

@code
input double OpenLevel = 1.3430; // уровень входа

XTouch* _touch;

int OnInit()
{
    // Инициализация модуля и установка уровня
    _touch = new XTouch(OpenLevel);

    return(0);
}

int OnCalculate(...)
{
    // Открыть только на указанном уровне при отскоке
    // (покупка при касании сверху и наоборот)
    if (_touch.Check())
    {
        if (_touch.GetSide() == 1)
        	// buy code here
        else if (_touch.GetSide() == -1)
        	// sell code here
    }
}

void OnDeinit(...)
{
	delete(_touch);
}
@endcode
*/
class XTouch
{
	/** Уровень касания */
	private: double _level;
	
	/** Последняя цена (нужна для определения касания) */
	private: double _lastPrice;
	
	/** Признак того, что уже коснулсь уровня */
	private: bool _complete;
	
	/** Позиция цены относительно уровня */
	private: int _side;


	/**
	Конструктор.
	*/
	public: void XTouch()
	{
		this.Set(0);
	}
	
	
	/**
	Конструктор 2.
	*/
	public: void XTouch(const double level)
	{
		this.Set(level);
	}
	
	
	/**
	Установить уровень касания.
	
	Если уровень не найден по названию, добавляется новый.
	
	@param level  Уровень.
	@param bid    Текущая цена. Если не указано, используется Bid текущего символа.
	
	@return       Успех операции. Если цена не указана явно, и последняя котировка текущего символа недоступна, будет возвращено false.
	*/
	public: bool Set(const double level, double bid = EMPTY_VALUE)
	{
		if (!this.checkBid(bid))
			return(false);
	
		_level = level;
		_lastPrice = bid;
		_complete = false;
		_side = Compare(bid, level);
		return(true);
	}
	
	
	/**
	Проверить касание уровня.
	
	Касание считается один раз.

	@param bid   Текущая цена. Если не указано, используется Bid текущего символа.
	
	@return      true - цена коснулась уровня, false - не коснулись, касалась его ранее, либо уровень не определен. 
	             Если цена не указана явно, и последняя котировка текущего символа недоступна, будет возвращено false.
	*/
	public: bool Check(double bid = EMPTY_VALUE)
	{
		if (_complete)
			return(false);
	
		if (!this.checkBid(bid))
			return(false);
	
		bool touch = false;

		// проверить касание

		double price = _lastPrice;
		double h = MAX(price, bid);
		double l = MIN(price, bid);
		
		touch = (l <= _level) && (_level <= h);
		
		// скольжение по уровню за касание не считать, иначе появляются неопределенности
		touch = touch && (_side != 0);

		// блокировка будущих срабатываний		
		if (touch)
			_complete = true;
		else
			_side = Compare(bid, _level);

		_lastPrice = bid;
	
		return(touch);
	}
	
	
	/**
	Получить сторону касания уровня.
	
	Значение обновляется с момента инициализации до касания. Сторона не может смениться с +1 на -1 или обратно, но может 
	выйти из неопределенности (0).

	@return  Сторона, с которой было или будет касание уровня: 1 - сверху, -1 - снизу, 0 - не определена (установка 
	         уровня произошла в момент нахождения цены на нем).
	*/
	public: int GetSide() const
	{
		return(_side);
	}
	
	
	/**
	Сбросить признак касания.
	
	Вызов функции аналогичен вызову TouchSet() со старым значением уровня.

	@param name  Название уровня.
	@param bid   Текущая цена. Если не указано, используется Bid текущего символа.
	
	@return      Успех операции. Если цена не указана явно, и последняя котировка текущего символа недоступна, будет возвращено false.
	*/
	public: bool Reset(double bid = EMPTY_VALUE)
	{
		if (!this.checkBid(bid))
			return(false);
		
		_complete = false;
		_lastPrice = bid;
		_side = Compare(bid, _level);
		return(true);
	}


	/**
	Проверить текущую цену.
	
	@param bid  Цена для проверки.
	
	@return     Если цена указана явно (не равна EMPTY_VALUE), возвращается true. Если цена не указана, то результат зависи от успеха получения 
	            последней котировки текущего символа.
	*/	
	private: bool checkBid(double &bid) const
	{
		if (bid != EMPTY_VALUE)
			return(true);
	
		MqlTick tick;
		
		if (!SymbolInfoTick(_Symbol, tick))
			return(false);
			
		bid = tick.bid;
		return(true);
	}
};

