/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Таймер.
*/


/**
Класс таймера.

Для проверки срабатывания таймера необходимо проверять его состояние через метод Check(). 
При срабатывании таймера происходит его перезапуск.

@code
void OnStart()
{
	// Два таймера
    XTimer* timer5sec = new XTimer(5);
    XTimer* timer1min = new XTimer(60);
    
    while (!IsStopped())
    {
        if (timer5sec.Check())
            Print("Сработал 5-секундный таймер");
        
        if (timer1min.Check())
            Print("Сработал 1-минутный таймер");
    }
    
    delete(timer5sec);
    delete(timer1min);
}
@endcode
*/
class XTimer
{
	/** Период таймера в секундах */
	private: int _seconds;
	
	/** Время запуска */
	private: datetime _lastTime;
	
	/** Своя переменная признака тестирования для оптимизации */
	private: bool _isTesting;


	/**
	Конструктор.
	
	@param seconds  Период таймера в секундах.
	@param reset    Сбросить таймер. Если при создании таймер не сбрасывать, то он сработает при первом вызове. Если сбрасывать, то
	                первый раз он сработает только через указанное количество секунд.
	
	@return         Экземпляр класса.
	*/
	public: void XTimer(const int seconds, const bool reset = true)
	{
		_seconds = seconds;
		_isTesting = MQLInfoInteger(MQL_TESTER);
		
		if (reset)
			Reset();
		else
			_lastTime = 0;
	}
	
	/**
	Проверить состояние таймера.
	
	@return  true, если таймер сработал. false, если нет.
	*/
	public: bool Check()
	{
		// проверить ожидание
		datetime now = this.getCurrentTime();

		bool stop = now >= _lastTime + _seconds;
		
		// сбрасываем таймер
		if (stop)
			_lastTime = now;
	
		return(stop);
	}

	/**
	Сбросить таймер.
	*/
	public: void Reset()
	{
		_lastTime = this.getCurrentTime();
	}
	
	/**
	Получить текущее время.
	
	@return  Текущее время. В тестере возвращается текущее имитированное серверное время, в иных случаях - текущее локальное.
	*/
	private: datetime getCurrentTime() const
	{
		return(_isTesting ? TimeCurrent() : TimeLocal());
	}

};
