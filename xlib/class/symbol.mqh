/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Класс для работы с рыночными инструментами.
*/


/**
Класс для работы с рыночными инструментами.
*/
class XSymbol
{
	/** Название символа */
	private: string _symbol;
	
	/** Количество знаков */
	private: long _digits;
	
	/** Пункт */
	private: double _point;
	
	/** Масштаб: 1.0 - 4/2 знака, 10.0 - 5/3 знаков */
	private: double _scale;

	/** Минимальный лот */
	private: double _minLot;
	
	/** Максимальный лот */
	private: double _maxLot;
	
	/** Шаг лота */
	private: double _lotStep;
	
	/** Точность лота в знаках после запятой */
	private: int _lotDigits;
	
	/** Признак успешной инициализации модуля */
	private: bool _initialized;


	/**
	Конструктор.
	
	@param symbol  Инструмент (краткое обозначение)
	
	@return  Экземпляр класса
	*/
	public: void XSymbol(const string symbol = NULL)
	{
		_symbol = symbol == NULL ? _Symbol : symbol;
	}
	
	
	/**
	Инициализировать инструмент.
	
	Символ может быть недоступен для работы, поэтому необходимо проверять его доступность 
	по крайней мере после создания экземпляра класса. При инициализации также считываются
	неизменяемые данные для ускорения дальнейших обращений к ним.
	
	@return Успех операции.
	*/
	public: bool Init()
	{
		// Проверить условия успешной иницилазации.
		_initialized = SymbolInfoDouble(_symbol, SYMBOL_VOLUME_MIN, _minLot)
			&& SymbolInfoDouble(_symbol, SYMBOL_VOLUME_MAX, _maxLot)
			&& SymbolInfoDouble(_symbol, SYMBOL_VOLUME_STEP, _lotStep)
			&& SymbolInfoDouble(_symbol, SYMBOL_POINT, _point)
			&& SymbolInfoInteger(_symbol, SYMBOL_DIGITS, _digits);
			
		// Определить количество знаков лота
		if (_lotStep > 0)
			_lotDigits = (int)MathCeil(MathLog10(1.0 / _lotStep));
		else
			_initialized = false;
	
		// пятизнак
		if (_initialized)
		{
			if ((_digits == 3) || (_digits == 5))
				_scale = 10.0;
			else
				_scale = 1.0;
		}
	
		return(_initialized);
	}


	/**
	Получить признак успешной инициализации - результат последнего выполнения метода Init().
	
	@return Признак успешной инициализации.
	*/
	public: bool IsInitialized() const
	{
		return(_initialized);
	}


	/**
	Преобразовать пункты в пятизнак, если требуется.
	
	При работе на графиках с четырехзнаковым инструментов преобразование не происходит.
	
	@param pips  Значение в пунктах для 4-знакового (2-х для кроссов с JPY) отображения.
	
	@return      Значение в пунктах для 5-знакового (3-х для кроссов с JPY) отображения.
	*/
	public: double En5(const double pips) const
	{
		return(pips * _scale);
	}


	/**
	Преобразовать пункты из пятизнака, если требуется.

	При работе на графиках с четырехзнаковым инструментов преобразование не происходит.
	
	@param pips  Значение в пунктах для 5-знакового (3-х для кроссов с JPY) отображения.
	
	@return      Значение в пунктах для 4-знакового (2-х для кроссов с JPY) отображения.
	*/
	public: double De5(const double pips) const
	{
		return(pips / _scale);
	}


	/**
	Получить цену Ask.
	
	@return  Цена Ask.
	*/
	public: double GetAsk() const
	{
		return(SymbolInfoDouble(_symbol, SYMBOL_ASK));
	}


	/**
	Получить цену Bid.
	
	@return  Цена Bid.
	*/
	public: double GetBid() const
	{
		return(SymbolInfoDouble(_symbol, SYMBOL_BID));
	}


	/**
	Получить спред как разницу Ask-Bid.
	
	@return  Спред (Ask-Bid).
	*/
	public: double GetSpread() const
	{
		return(SymbolInfoDouble(_symbol, SYMBOL_ASK) - SymbolInfoDouble(_symbol, SYMBOL_BID));
	}


	/**
	Получить размер пункта.
	
	Устанавливается при инициализации.

	@return  Размер пункта.
	*/
	public: double GetPoint() const
	{
		return(_point);
	}


	/**
	Получить количество знаков после запятой.
	
	Устанавливается при инициализации.

	@return  Количество знаков после запятой.
	*/
	public: int GetDigits() const
	{
		return((int)_digits);
	}


	/**
	Получить название символа (краткое обозначение).
	
	Устанавливается при инициализации.
	
	@return  Название символа (краткое обозначение).
	*/
	public: string GetName() const
	{
		return(_symbol);
	}


	/**
	Получить минимальный лот.
	
	Устанавливается при инициализации.
	
	@return  Минимальный лот.
	*/
	public: double GetMinLot() const
	{
		return(_minLot);
	}


	/**
	Получить максимальный лот.
	
	Устанавливается при инициализации.
	
	@return  Максимальный лот.
	*/
	public: double GetMaxLot() const
	{
		return(_maxLot);
	}


	/**
	Получить количество знаков после запятой для лота.
	
	Устанавливается при инициализации.
	
	@return  Количество знаков после запятой для лота.
	*/
	public: int GetLotDigits() const
	{
		return(_lotDigits);
	}
	
	
	/**
	Нормализовать лот.
	
	Лот будет ограничен сверху максимальным лотом, снизу - минимальным лотом и округлен до 
	точности лота для данного инструмента.
	
	@param lot  Лот
	
	@return     Нормализованный лот.
	*/
	public: double NormalizeLot(double lot) const
	{
		lot = NormalizeDouble(lot, _lotDigits);
		
		if (lot < _minLot)
			lot = _minLot;
		else if (lot > _maxLot)
			lot = _maxLot;
			
		return(lot);
	}
	
	
	/**
	Получить значение дробного свойства инструмента.
	
	@param propertyId  Код свойства.
	
	@return            Значение свойства.
	*/
	public: double GetProperty(const ENUM_SYMBOL_INFO_DOUBLE propertyId) const
	{
		return(SymbolInfoDouble(_symbol, propertyId));
	}
	
	
	/**
	Получить значение целочисленного свойства инструмента.
	
	@param propertyId  Код свойства.
	
	@return            Значение свойства.
	*/
	public: long GetProperty(const ENUM_SYMBOL_INFO_INTEGER propertyId) const
	{
		return(SymbolInfoInteger(_symbol, propertyId));
	}
	
	
	/**
	Получить значение строкового свойства инструмента.
	
	@param propertyId  Код свойства.
	
	@return            Значение свойства.
	*/
	public: string GetProperty(const ENUM_SYMBOL_INFO_STRING propertyId) const
	{
		return(SymbolInfoString(_symbol, propertyId));
	}
	
	
	/**
	Получить значение дробного свойства инструмента.
	
	@param propertyId  Код свойства.
	@param value       Возращаемое значение.
	
	@return            Успех операции.
	*/
	public: bool GetProperty(const ENUM_SYMBOL_INFO_DOUBLE propertyId, double &value) const
	{
		return(SymbolInfoDouble(_symbol, propertyId, value));
	}
	
	
	/**
	Получить значение целочисленного свойства инструмента.
	
	@param propertyId  Код свойства.
	@param value       Возращаемое значение.
	
	@return            Успех операции.
	*/
	public: bool GetProperty(const ENUM_SYMBOL_INFO_INTEGER propertyId, long &value) const
	{
		return(SymbolInfoInteger(_symbol, propertyId, value));
	}
	
	
	/**
	Получить значение строкового свойства инструмента.
	
	@param propertyId  Код свойства.
	@param value       Возращаемое значение.
	
	@return            Успех операции.
	*/
	public: bool GetProperty(const ENUM_SYMBOL_INFO_STRING propertyId, string &value) const
	{
		return(SymbolInfoString(_symbol, propertyId, value));
	}


	/**
	Выбрать инструмент в окне обзора рынка, либо убрать его.
	
	Повторяет стандартную функцию SymbolSelect().
	
	@param select  Выбрать или убрать символ из окна обзора рынка. 
	@return        Успех операции
	*/
	public: bool Select(const bool select) const
	{
		return(SymbolSelect(_symbol, select));
	}


	/**
	Получить признак синхронизированности данных по указанному инструменту в терминале с данными 
	на торговом сервере.
	
	Повторяет стандартную функцию SymbolIsSynchronized().
	
	@return   Признак синхронизированности данных.
	*/
	public: bool IsSynchronized() const
	{
		return(SymbolIsSynchronized(_symbol));
	}
	
};

