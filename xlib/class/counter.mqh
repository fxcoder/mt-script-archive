/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Счётчик.

*/


#include "..\util\array.mqh"

/**
Класс счётчика.

Счетчик работает как будильник или таймер - один раз или периодически через указанное 
число раз. Каждое чтение состояния счетчика увеличивает его на единицу.
*/
class XCounter
{
	/** Необходимое число срабатываний */
	private: int _n;
	
	/** Фактическое число срабатываний */
	private: int _lastN;
	
	
	/**
	Конструктор.
	
	@param n     Период счетчика.
	*/
	public: void XCounter(const int n)
	{
		this.Set(n);
	}
	
	
	/**
	Установить счетчик.
	
	@param n     Период счетчика.
	*/
	public: void Set(const int n)
	{
		_n = n;
		_lastN = 0;
	}
	

	/**
	Сбросить счетчик.
	*/
	public: void Reset()
	{
		_lastN = 0;
	}
	

	/**
	Проверить состояние счетчика до первого срабатывания.
	
	@return      true - счетчик сработал, false - нет.
	*/
	public: bool CheckFirst()
	{
		bool alarm = false;
		
		// проверить счетчик
		int n = _lastN + 1;
		
		// срабатывание
		alarm = n == _n;
		
		if (alarm)
		{
			// защита от переполнения
			_lastN = _n + 1;
		}
		else
		{
			_lastN = n;
		}
	
		return(alarm);
	}
	
	
	/**
	Проверить состояние зацикленного счетчика.
	
	Срабатывание происходит периодически через указанное при установке счетчика число раз.
	
	@return      true - счетчик сработал, false - нет.
	*/
	public: bool Check()
	{
		bool alarm = false;

		// проверить счетчик
		int n = _lastN + 1;
		
		// срабатывание
		alarm = n == _n;
		
		if (alarm)
			_lastN = 0;
	
		return(alarm);
	}
		
};
	
