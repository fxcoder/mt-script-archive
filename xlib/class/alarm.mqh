/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Будильник.
*/

#include "..\enum\alarm_type.mqh"

/**
Класс будильника
 
@code
XAlarm* nyAlarm;    // новогодний будильник для 2015 г.
XAlarm* noonAlarm;  // полуденный будильник

int OnInit()
{
    // Создать и установить будильники
    nyAlarm = new XAlarm(D'2015.01.01 00:00', XLIB_ALARM_SINGLE);
    noonAlarm = new XAlarm(D'12:00', XLIB_ALARM_DAILY);

    return(0);
}

void OnTick()
{
    if (nyAlarm.Check())
        Alert("С Новым Годом!");

    if (noonAlarm.Check())
        Alert("Полдень.");
}

void OnDeinit(const int reason)
{
    delete(nyAlarm);
    delete(noonAlarm);
} 
@endcode
*/
class XAlarm
{
	/** Время срабатывания */
	private: datetime _time;
	
	/** Тип будильника */
	private: ENUM_XLIB_ALARM_TYPE _type;
	
	/** Время предыдущей проверки */
	private: datetime _lastCheckTime;


	/** 
	Конструктор.
	
	@param time  Дата и время срабатывания будильника.
	@param type  Тип будильника: XLIB_ALARM_SINGLE - одинарный, XLIB_ALARM_DAILY - ежедневный (кроме выходных).
	
	@return Экземпляр класса
	*/
	public: void XAlarm(const datetime time, const ENUM_XLIB_ALARM_TYPE type = XLIB_ALARM_SINGLE)
	{
		_time = time;
		_type = type;
		_lastCheckTime = TimeCurrent();
	}


	/**
	Проверить состояние будильника.
	
	@return Результат проверки состояния будильника, true - будильник сработал, false - нет.
	*/
	public: bool Check()
	{
		bool alarm = false;
	
		datetime tc = TimeCurrent();
		
		datetime prev = _lastCheckTime;
		datetime time = _time;
		datetime now = tc;
		const int SECONDS_IN_DAY = 86400;

		// для ежедневных будильников выделить время
		if (_type == XLIB_ALARM_DAILY)
		{
			datetime day = prev - (prev % SECONDS_IN_DAY);
			time = day + (time % SECONDS_IN_DAY);
			
			if (time < prev)
				time += SECONDS_IN_DAY;
		}
		
		// Проверить пересечение времени, выходные пропускам
		MqlDateTime timeStruct;
		TimeToStruct(time, timeStruct);
		
		//TODO: где-то здесь ошибка, пропускается срабатывание 00:00 в понедельник
		if (timeStruct.day_of_week != 6 && timeStruct.day_of_week != 0)
			alarm = prev < time && time <= now;
		
		// Всегда обновлять время предыдущей проверки
		_lastCheckTime = tc;
		
		return(alarm);
	}
};

