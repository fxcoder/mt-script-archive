/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Импорт функций из wininet.dll.
*/

#import "wininet.dll"
int InternetOpenW(string &lpszAgent, int dwAccessType, string &lpszProxyName, string &lpszProxyBypass, int dwFlags);
int InternetOpenUrlW(int hInternet, string &lpszUrl, string &lpszHeaders, int dwHeadersLength, int dwFlags, int dwContext);
int InternetReadFile(int hFile, uchar &lpBuffer[], int dwNumberOfBytesToRead, int &lpdwNumberOfBytesRead);
int InternetCloseHandle(int hInternet);
#import
