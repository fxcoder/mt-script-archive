/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Функции для работы с датой и временем (определения).
*/

/** Количество секунд в сутках. */
#define TIME__SECONDS_IN_DAY 86400

/** Получить время начала дня для указанного времени. */
#define TIME_BEGIN_OF_DAY(T) ((T) - ((T) % TIME__SECONDS_IN_DAY))

/** Получить время от начала дня. */
#define TIME_OF_DAY(T) ((T) % TIME__SECONDS_IN_DAY)

