/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Функции работы с цветом (определения).
*/


/** Инвертировать цвет. */
//TODO: -> COLOR_INVERT
#define INVERT_COLOR(C) ((color)((C) ^ 0xFFFFFF))
#define COLOR_INVERT(C) ((color)((C) ^ 0xFFFFFF))

/** Проверить, является ли цвет неопределенным. */
#define COLOR_IS_NONE(C) (((C) >> 24) != 0)

/** Собрать цвет из составляющих. */
//TODO: -> COLOR_FROM_RGB
#define RGB_TO_COLOR(R, G, B) ((color)((((B) & 0xFF) << 16) | (((G) & 0xFF) << 8) | ((R) & 0xFF)))
#define COLOR_FROM_RGB(R, G, B) ((color)((((B) & 0xFF) << 16) | (((G) & 0xFF) << 8) | ((R) & 0xFF)))

