/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Математические функции (определения).
*/


/** Специальное значение, обозначающее отсутсвие числа (Non a Number). */
#define NAN MathLog(-1)

/** Поместить значение A в указанный диапазон между L и H. */
#define PUT_IN_RANGE(A, L, H) ((H) < (L) ? (A) : ((A) < (L) ? (L) : ((A) > (H) ? (H) : (A))))
#define IS_IN_RANGE(A, L, H) (((A) >= (L)) && ((A) <= (H)))

/** Максимальное из двух значений */
#define MAX(A, B) (((A) > (B)) ? (A) : (B))
#define MAX3(A, B, C) (((A) > (B)) ? (((A) > (C)) ? (A) : (C)) : (((B) > (C)) ? (B) : (C)))

/** Минимальное из двух значений */
#define MIN(A, B) (((A) < (B)) ? (A) : (B))
#define MIN3(A, B, C) (((A) < (B)) ? (((A) < (C)) ? (A) : (C)) : (((B) < (C)) ? (B) : (C)))

/** Модуль */
#define ABS(X) ((X) >= 0 ? (X) : -(X))

/** Знак числа (double): -1.0, 0.0, +1.0 */
#define SIGNUM(X) ((X) > 0 ? 1.0 : ((X) < 0 ? -1.0 : 0.0))

/** Знак числа (int): -1, 0, +1 */
#define SIGNUM_INT(X) ((X) > 0 ? 1 : ((X) < 0 ? -1 : 0))
