/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Класс для работы с сетью (определения).
*/


// IOT=INTERNET_OPEN_TYPE
#define INET__IOT_PRECONFIG 0 // use registry configuration
#define INET__IOT_DIRECT 1    // direct to net
#define INET__IOT_PROXY 3     // via named proxy


// IF=INTERNET_FLAG
#define INET__IF_NO_CACHE_WRITE 1024
#define INET__IF_PRAGMA_NOCACHE 16384
#define INET__IF_RELOAD 65536

