/**
@file
Библиотека MQL.XLib. Copyright © FXcoder. https://fxcoder.blogspot.com
Функции преобразования (определения).
*/


/** Преобразовать таймфрейм в минуты.  */
#define PERIOD_TO_MINUTES(P) (PeriodSeconds(P) / 60)

