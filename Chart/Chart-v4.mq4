#property copyright "График. Chart v4.2. © FXcoder"
#property link      "http://fxcoder.blogspot.com"
#property strict

#property indicator_separate_window
#property indicator_buffers 11            // The first buffer is for value, other 10 are for candle drawing
#property indicator_color1  clrNONE       // Value
#property indicator_color2  clrLimeGreen  // BullBody
#property indicator_color3  clrLimeGreen  // BullBody2
#property indicator_color4  clrRed        // BearBody
#property indicator_color5  clrRed        // BearBody2
#property indicator_color6  clrNONE       // BodyMask
#property indicator_color7  clrLimeGreen  // BullShadow
#property indicator_color8  clrLimeGreen  // BullShadow2
#property indicator_color9  clrRed        // BearShadow
#property indicator_color10 clrRed        // BearShadow2
#property indicator_color11 clrNONE       // ShadowMask

#define COLOR_IS_NONE(C) (((C) >> 24) != 0)

input string ChartSym = "";                                // Chart symbol
input bool Reverse = false;                                // Reverse
input ENUM_TIMEFRAMES ChartPer = PERIOD_CURRENT;           // Timeframe
input color BullColor = clrLimeGreen;                      // Bull candle color
input color BearColor = clrRed;                            // Bear candle color
input color BackColor = clrNONE;                           // Background color (None=auto)
input int BodyWidth = 0;                                   // Candle body width (0=auto)
input ENUM_APPLIED_PRICE ValueAppliedPrice = PRICE_CLOSE;  // Value applied price
input int MaxBars = 0;                                     // Maximum bars
input int Shift = 0;                                       // Shift
input bool LogScale = false;                               // Logarithmic scale

int start()
{
	updateBodyWidth();
	updateMaskColor();

	// Определить количество баров для расчета
	int countedBars = IndicatorCounted();

	if (countedBars > 0)
		countedBars--;

	int barsCount = Bars - countedBars;

	// Если таймфрейм чужой, перерисовываем все бары
	if (_period != _Period)
	{
		barsCount = Bars;
		clearData();
	}

	// Ограничить максимальное число баров для расчета пользовательским значением
	if (MaxBars > 0)
	{
		if (Bars - countedBars > 2)
		{
			barsCount = MaxBars;
			clearData();
		}
		else
		{
			clearData(MaxBars);
			barsCount = MathMin(barsCount, MaxBars);
		}
	}

	double o, h, l, c, value;

	for (int i = barsCount - 1; i >= 0; i--)
	{
		// очистить текущий бар
		clearData(i);

		o = EMPTY_VALUE;
		h = EMPTY_VALUE;
		l = EMPTY_VALUE;
		c = EMPTY_VALUE;
		value = EMPTY_VALUE;

		datetime dt = iTime(Symbol(), _period, i);
		int j = iBarShift(_symbol, _period, dt, true);

		// Если бара для данного времени нет, то не отображаем ничего, но в значение пишем прошлый бар
		if (j == -1)
		{
			if (i + 1 < Bars - 1)
				Value[i] = Value[i + 1];

			continue;
		}

		value = getPrice(_symbol, _period, ValueAppliedPrice, j, Reverse, o, h, l, c);

		if (value == EMPTY_VALUE)
			continue;

		// Установка значений в буферы линий
		if (LogScale)
		{
			if ((value <= 0) || (o <= 0) || (h <= 0) || (l <= 0) || (c <= 0))
				continue;

			o = 100.0 * MathLog(o);
			h = 100.0 * MathLog(h);
			l = 100.0 * MathLog(l);
			c = 100.0 * MathLog(c);
			value = 100.0 * MathLog(value);
		}

		drawBar(o, h, l, c, i);
		Value[i] = value;
	}

	return(0);
}

int init()
{
	_symbol = ChartSym == "" ? Symbol() : UpperString(ChartSym);
	_period = PeriodSeconds(ChartPer) / 60;
	_bodyWidth = BodyWidth;

	if (_bodyWidth <= 0)
		_bodyWidth = 1;

	SetIndexStyle(0, DRAW_NONE);
	SetIndexStyle(1, DRAW_HISTOGRAM, EMPTY, _bodyWidth, BullColor);
	SetIndexStyle(2, DRAW_HISTOGRAM, EMPTY, _bodyWidth, BullColor);
	SetIndexStyle(3, DRAW_HISTOGRAM, EMPTY, _bodyWidth, BearColor);
	SetIndexStyle(4, DRAW_HISTOGRAM, EMPTY, _bodyWidth, BearColor);
	SetIndexStyle(5, DRAW_HISTOGRAM, EMPTY, _bodyWidth, _bgColor);
	SetIndexStyle(6, DRAW_HISTOGRAM, EMPTY, EMPTY, BullColor);
	SetIndexStyle(7, DRAW_HISTOGRAM, EMPTY, EMPTY, BullColor);
	SetIndexStyle(8, DRAW_HISTOGRAM, EMPTY, EMPTY, BearColor);
	SetIndexStyle(9, DRAW_HISTOGRAM, EMPTY, EMPTY, BearColor);
	SetIndexStyle(10, DRAW_HISTOGRAM, EMPTY, 1, _bgColor);

	SetIndexBuffer(0, Value);
	SetIndexBuffer(1, BullBody);
	SetIndexBuffer(2, BullBody2);
	SetIndexBuffer(3, BearBody);
	SetIndexBuffer(4, BearBody2);
	SetIndexBuffer(5, BodyMask);
	SetIndexBuffer(6, BullShadow);
	SetIndexBuffer(7, BullShadow2);
	SetIndexBuffer(8, BearShadow);
	SetIndexBuffer(9, BearShadow2);
	SetIndexBuffer(10, ShadowMask);

	for (int i = 0; i <= 10; i++)
		SetIndexShift(i, Shift);

	SetIndexLabel(0, "Value");
	SetIndexLabel(1, "Bull Body");
	SetIndexLabel(2, "Bull Body 2");
	SetIndexLabel(3, "Bear Body");
	SetIndexLabel(4, "Bear Body 2");
	SetIndexLabel(5, "Body Mask");
	SetIndexLabel(6, "Bull Shadow");
	SetIndexLabel(7, "Bull Shadow 2");
	SetIndexLabel(8, "Bear Shadow");
	SetIndexLabel(9, "Bear Shadow 2");
	SetIndexLabel(10, "Shadow Mask");

	string name =
		(Reverse ? "1/" : "") +
		_symbol +
		"," + PeriodToString((ENUM_TIMEFRAMES)_period) +
		(LogScale ? ",log" : "");

	IndicatorShortName(name);

	return(0);
}

void OnChartEvent(const int id, const long& lparam, const double& dparam, const string& sparam)
{
	if (id == CHARTEVENT_CHART_CHANGE)
	{
		if (updateBodyWidth() || updateMaskColor())
			ChartRedraw();
	}
}

bool updateMaskColor()
{
	if (!ColorIsNone(BackColor))
		return(false);

	// Определить цвета фона
	color newBgColor = (color)ChartGetInteger(0, CHART_COLOR_BACKGROUND);

	if (newBgColor == _bgColor)
		return(false);

	_bgColor = newBgColor;

	SetIndexStyle(5, DRAW_HISTOGRAM, EMPTY, EMPTY, _bgColor);
	SetIndexStyle(10, DRAW_HISTOGRAM, EMPTY, EMPTY, _bgColor);

	return(true);
}

bool updateBodyWidth()
{
	if (BodyWidth > 0)
		return(false);

	// Определить толщину тела бара
	int newBodyWidth = barStepToBodyWidth(GetBarStep());

	if (newBodyWidth == _bodyWidth)
		return(false);

	_bodyWidth = newBodyWidth;

	for (int i = 1; i <= 5; i++)
		SetIndexStyle(i, DRAW_HISTOGRAM, EMPTY, _bodyWidth);

	return(true);
}

void clearData(int bar = -1)
{
	if (bar == -1)
	{
		ArrayInitialize(Value, EMPTY_VALUE);
		ArrayInitialize(BullBody, EMPTY_VALUE);
		ArrayInitialize(BullBody2, EMPTY_VALUE);
		ArrayInitialize(BearBody, EMPTY_VALUE);
		ArrayInitialize(BearBody2, EMPTY_VALUE);
		ArrayInitialize(BodyMask, EMPTY_VALUE);
		ArrayInitialize(BullShadow, EMPTY_VALUE);
		ArrayInitialize(BullShadow2, EMPTY_VALUE);
		ArrayInitialize(BearShadow, EMPTY_VALUE);
		ArrayInitialize(BearShadow2, EMPTY_VALUE);
		ArrayInitialize(ShadowMask, EMPTY_VALUE);
	}
	else
	{
		Value[bar] = EMPTY_VALUE;
		BullBody[bar] = EMPTY_VALUE;
		BullBody2[bar] = EMPTY_VALUE;
		BearBody[bar] = EMPTY_VALUE;
		BearBody2[bar] = EMPTY_VALUE;
		BodyMask[bar] = EMPTY_VALUE;
		BullShadow[bar] = EMPTY_VALUE;
		BullShadow2[bar] = EMPTY_VALUE;
		BearShadow[bar] = EMPTY_VALUE;
		BearShadow2[bar] = EMPTY_VALUE;
		ShadowMask[bar] = EMPTY_VALUE;
	}
}

double getPrice(string &symbol, int timeframe, int ap, int bar, bool reverse, double &open, double &high, double &low, double &close)
{
	double o = iOpen(symbol, timeframe, bar);
	double h = iHigh(symbol, timeframe, bar);
	double l = iLow(symbol, timeframe, bar);
	double c = iClose(symbol, timeframe, bar);

	if ((o == 0) || (h == 0) || (l == 0) || (c == 0))
		return(EMPTY_VALUE);

	open = reverse ? 1.0 / o : o;
	high = reverse ? 1.0 / l : h;
	low = reverse ? 1.0 / h : l;
	close = reverse ? 1.0 / c : c;

	switch(ap)
	{
		case 0:
			return(close);

		case 1:
			return(open);

		case 2:
			return(high);

		case 3:
			return(low);

		case 4:
			if ((high != 0) && (low != 0))
				return((high + low) / 2.0);

		case 5:
			if ((high != 0) && (low != 0) && (close != 0))
				return((high + low + close) / 3.0);

		case 6:
			if ((high != 0) && (low != 0) && (close != 0))
				return((high + low + 2.0 * close) / 4.0);
	}

	return(EMPTY_VALUE);
}

// Нарисовать бар
void drawBar(double open, double high, double low, double close, int bar)
{
	bool isBull = close >= open;
	bool isBear = !isBull;

	double bodyMax = open > close ? open : close;
	double bodyMin = open < close ? open : close;

	BullBody[bar] = isBull && (close >= 0) ? close : EMPTY_VALUE;
	BullBody2[bar] = isBull && (open < 0) ? open : EMPTY_VALUE;

	BearBody[bar] = isBear && (open >= 0) ? open : EMPTY_VALUE;
	BearBody2[bar] = isBear && (close < 0) ? close : EMPTY_VALUE;

	BodyMask[bar] = (bodyMin > 0) ? bodyMin : (bodyMax < 0 ? bodyMax : EMPTY_VALUE);

	BullShadow[bar] = isBull && (high >= 0) ? high : EMPTY_VALUE;
	BullShadow2[bar] = isBull && (low < 0) ? low : EMPTY_VALUE;

	BearShadow[bar] = isBear && (high >= 0) ? high : EMPTY_VALUE;
	BearShadow2[bar] = isBear && (low < 0) ? low : EMPTY_VALUE;

	ShadowMask[bar] = low >= 0 ? low : (high < 0 ? high : EMPTY_VALUE);
}

int GetBarStep(const long chartId = 0)
{
	long chartScale;

	if (!ChartGetInteger(chartId, CHART_SCALE, 0, chartScale))
		return(1);

	return((int)MathPow(2, chartScale));
}

bool ColorIsNone(const color c)
{
	return(COLOR_IS_NONE(c));
}

string UpperString(string s)
{
	StringToUpper(s);
	return(s);
}

string PeriodToString(const int tf = 0)
{
	int period = PeriodSeconds(tf) / 60;

	if (period % 43200 == 0)
		return("MN" + IntegerToString(period / 43200));
	else if (period % 10080 == 0)
		return("W" + IntegerToString(period / 10080));
	else if (period % 1440 == 0)
		return("D" + IntegerToString(period / 1440));
	else if (period % 60 == 0)
		return("H" + IntegerToString(period / 60));
	else
		return("M" + IntegerToString(period));
}

int barStepToBodyWidth(const int step)
{
	if (step < 4)
		return(1);

	if (step < 8)
		return(2);

	if (step < 16)
		return(3);

	if (step < 32)
		return(6);

	// >=32
	return(12);
}

double Value[], BullBody[], BullBody2[], BearBody[], BearBody2[], BodyMask[], BullShadow[], BullShadow2[], BearShadow[], BearShadow2[], ShadowMask[];

int _bodyWidth = 2;
color _bgColor = clrNONE;
string _symbol;
int _period;

/*
Список изменений:

4.2
	* изменены ссылки на сайт
	* список изменений в коде

4.1
	* удалены устаревшие функции, использующие вызовы DLL
	* более оперативное обновление ширины тела свечи
	* более оперативное реагирование на изменение цвета фона
	* изменён тип параметра Timeframe, но в числовом представлении он остался прежним, поэтому сохранилась совместимость со старыми шаблонами

4.0
	* использование новых возможностей указания параметров в MT4 build 600
	* при логарифмировании теперь не происходит смещения на 1000 вверх из-за ограничений в терминалах старых версий
	* параметр Shift смещает график вправо, как стандартные индикаторы

3.2
	* совместимость с MetaTrader версии 4.00 Build 600 и новее

3.1
	* возможность отображения в логарифмическом масштабе (новый параметр LogScale)

3.0.7
	* улучшен учет пропусков в истории котировок

3.0.6
	* Неверное отображение для реверсов кроссов JPY

3.0.5
	* +Chart.mq4 3.0.5: ChartSymbol содержал usdchf по умолчанию (должно быть пустое значение)

3.0.4
	* поправки для мелких таймфреймов

3.0.3
	* первая публичная версия

Лицензия:

Разрешается повторное распространение и использование как в виде исходного кода, так и в двоичной форме, с изменениями
или без, при соблюдении следующих условий:

При повторном распространении исходного кода должно оставаться указанное выше уведомление об авторском праве, этот
список условий и последующий отказ от гарантий.

При повторном распространении двоичного кода должна сохраняться указанная выше информация об авторском праве, этот
список условий и последующий отказ от гарантий в документации и/или в других материалах, поставляемых при распространении.

Ни название FXcoder, ни имена ее сотрудников не могут быть использованы в качестве поддержки или продвижения продуктов,
основанных на этом ПО без предварительного письменного разрешения.

ЭТА ПРОГРАММА ПРЕДОСТАВЛЕНА ВЛАДЕЛЬЦАМИ АВТОРСКИХ ПРАВ И/ИЛИ ДРУГИМИ СТОРОНАМИ «КАК ОНА ЕСТЬ» БЕЗ КАКОГО-ЛИБО ВИДА
ГАРАНТИЙ, ВЫРАЖЕННЫХ ЯВНО ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ, ПОДРАЗУМЕВАЕМЫЕ ГАРАНТИИ КОММЕРЧЕСКОЙ
ЦЕННОСТИ И ПРИГОДНОСТИ ДЛЯ КОНКРЕТНОЙ ЦЕЛИ. НИ В КОЕМ СЛУЧАЕ, ЕСЛИ НЕ ТРЕБУЕТСЯ СООТВЕТСТВУЮЩИМ ЗАКОНОМ, ИЛИ НЕ
УСТАНОВЛЕНО В УСТНОЙ ФОРМЕ, НИ ОДИН ВЛАДЕЛЕЦ АВТОРСКИХ ПРАВ И НИ ОДНО ДРУГОЕ ЛИЦО, КОТОРОЕ МОЖЕТ ИЗМЕНЯТЬ И/ИЛИ
ПОВТОРНО РАСПРОСТРАНЯТЬ ПРОГРАММУ, КАК БЫЛО СКАЗАНО ВЫШЕ, НЕ НЕСЁТ ОТВЕТСТВЕННОСТИ, ВКЛЮЧАЯ ЛЮБЫЕ ОБЩИЕ, СЛУЧАЙНЫЕ,
СПЕЦИАЛЬНЫЕ ИЛИ ПОСЛЕДОВАВШИЕ УБЫТКИ, ВСЛЕДСТВИЕ ИСПОЛЬЗОВАНИЯ ИЛИ НЕВОЗМОЖНОСТИ ИСПОЛЬЗОВАНИЯ ПРОГРАММЫ (ВКЛЮЧАЯ, НО
НЕ ОГРАНИЧИВАЯСЬ ПОТЕРЕЙ ДАННЫХ, ИЛИ ДАННЫМИ, СТАВШИМИ НЕПРАВИЛЬНЫМИ, ИЛИ ПОТЕРЯМИ ПРИНЕСЕННЫМИ ИЗ-ЗА ВАС ИЛИ ТРЕТЬИХ
ЛИЦ, ИЛИ ОТКАЗОМ ПРОГРАММЫ РАБОТАТЬ СОВМЕСТНО С ДРУГИМИ ПРОГРАММАМИ), ДАЖЕ ЕСЛИ ТАКОЙ ВЛАДЕЛЕЦ ИЛИ ДРУГОЕ ЛИЦО БЫЛИ
ИЗВЕЩЕНЫ О ВОЗМОЖНОСТИ ТАКИХ УБЫТКОВ.

License:

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

Neither the name of the FXcoder nor the names of its contributors may be used to endorse or promote products derived
from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

// 2016-02-04 06:45:36 UTC. MQLMake 1.37. © FXcoder