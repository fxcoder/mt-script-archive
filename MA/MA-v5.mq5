#property copyright "MA v5.0. © FXcoder"
#property link      "http://fxcoder.blogspot.com"
#property strict

#property indicator_chart_window

#property indicator_buffers 2
#property indicator_plots 1
#property indicator_color1 clrCrimson, C'219,43,78', C'218,66,97', C'217,90,115', C'216,113,133', C'215,136,152', C'213,159,170', C'212,182,188'

#define COLOR_COUNT 8
#define NAN MathLog(-1)
#define PUT_IN_RANGE(A, L, H) ((H) < (L) ? (A) : ((A) < (L) ? (L) : ((A) > (H) ? (H) : (A))))
#define COLOR_IS_NONE(C) (((C) >> 24) != 0)
#define RGB_TO_COLOR(R, G, B) ((color)((((B) & 0x0000FF) << 16) + (((G) & 0x0000FF) << 8) + ((R) & 0x0000FF)))
#define PERIOD_TO_MINUTES(P) (PeriodSeconds(P) / 60)

/* Параметры расчета */
input int MaPeriod = 20;                                   // MA period
input ENUM_MA_METHOD MaMethod = MODE_SMA;                  // MA method
input ENUM_TIMEFRAMES TimeFrame = PERIOD_D1;               // Period
input bool Interpolate = false;                            // Interpolate
input ENUM_APPLIED_PRICE AppliedPrice = PRICE_CLOSE;       // Applied price

/* Оформление */
input color LineColor = Crimson;                           // Line color
input color TouchColor = CLR_NONE;                         // Touch color
input ENUM_LINE_STYLE LineStyle = STYLE_SOLID;             // Line style
input int LineWidth = 1;                                   // Line width
int Sensitivity = 100;                               // Sensitivity

int OnInit()
{
	// периоды в минутах
	_tf = PeriodMinutes(TimeFrame);
	_period = PeriodMinutes(PERIOD_CURRENT);
	_maPeriod = Interpolate ? MaPeriod * _tf / _period : MaPeriod;

	if (_maPeriod < 1)
		_maPeriod = 1;

	string mas = MaMethodToString(MaMethod);
	string l = mas + (string)MaPeriod + "@" + PeriodToString((ENUM_TIMEFRAMES)_tf) + (Interpolate ? " (~" + mas + (string)_maPeriod + ")" : "");
	int drawBegin = Interpolate ? _maPeriod : 1;

	SetIndexBuffer(0, _ma.Buffer, INDICATOR_DATA);
	SetIndexBuffer(1, _color.Buffer, INDICATOR_COLOR_INDEX);
	PlotIndexSetInteger(0, PLOT_COLOR_INDEXES, COLOR_COUNT);

	_mah = iMA(_Symbol, Interpolate ? _Period : TimeFrame, _maPeriod, 0, MaMethod, AppliedPrice);

	PlotIndexSetInteger(0, PLOT_DRAW_TYPE, DRAW_COLOR_LINE);
	PlotIndexSetInteger(0, PLOT_LINE_STYLE, LineStyle);
	PlotIndexSetInteger(0, PLOT_LINE_WIDTH, LineWidth);
	PlotIndexSetInteger(0, PLOT_DRAW_BEGIN, drawBegin);
	PlotIndexSetDouble(0, PLOT_EMPTY_VALUE, EMPTY_VALUE);
	PlotIndexSetString(0, PLOT_LABEL, l);

	ArraySetAsSeries(_ma.Buffer, true);
	ArraySetAsSeries(_color.Buffer, true);

	IndicatorSetString(INDICATOR_SHORTNAME, MQLInfoString(MQL_PROGRAM_NAME) + ": " + l);

	UpdateAutoColors();

	if (Interpolate)
		_isParamsOk = (_tf <= 1440) && (_period <= 1440);
	else
		_isParamsOk = _tf >= _period;

	return(INIT_SUCCEEDED);
}

int OnCalculate(const int ratesTotal, const int prevCalculated, const datetime &time[], const double &open[], const double &high[], const double &low[], const double &close[], const long &tick_volume[], const long &volume[], const int &spread[])
{

	if (!_isParamsOk)
		return(0);

	// недостаточно баров для расчёта даже одного бара в режиме интерполяции или MaPeriod баров в нормальном
	if (ratesTotal < _maPeriod)
		return(0);

	int barsToCalc = ratesTotal - prevCalculated;
	int calculatedBars = ratesTotal - barsToCalc;

	if (Interpolate)
	{
		// если баров для расчёта не хватает (с запасом в 1 бар для рисования)
		if (barsToCalc + _maPeriod > ratesTotal)
		{
			barsToCalc = ratesTotal - _maPeriod;

			// слишком мало баров на графике
			if (barsToCalc <= 0)
				return(0);
		}

		// последний бар рисовать всегда
		if (barsToCalc <= 0)
			barsToCalc = 1;

		// Количество рассчитанных баров для возврата результа
		calculatedBars = ratesTotal - barsToCalc;
	}
	else
	{
		// коррекция для режима чужого тф

		/* 1/2. Коррекция левой границы редкий случай, но требующий проверки.
		        Если количество баров на большем тф меньше, чем на текущем (с учётом масштаба). */

		// количество баров на чужом таймрейме минус запас для расчётов и 1 бар для рисования, если тф тот же
		int otherTfBars = Bars(_Symbol, TimeFrame) - (_maPeriod - 1) - (_tf == _period ? 1 : 0);

		datetime otherTfFirstBarTime;

		if (!TimeOfBar(_Symbol, TimeFrame, otherTfBars - 1, otherTfFirstBarTime))
			return(0);

		// найти бар на текущем ТФ, который соответствовал бы времени бара на чужом (0 - левый)
		int otherTfFirstBar = ArrayBsearch(time, otherTfFirstBarTime);

		// левее этого бара расчёт невозможен, поэтому корректируем начало расчёта
		if (calculatedBars < otherTfFirstBar + 1)
		{
			calculatedBars = otherTfFirstBar + 1;
			barsToCalc = ratesTotal - calculatedBars;

			if (barsToCalc <= 0)
				return(0);
		}

		// 2/2. Коррекция правой границы

		int otherTfBarsOnThisTf = _tf / _period;

		// необходимо перерисовывать весь последний бар чужого тф
		if (barsToCalc < otherTfBarsOnThisTf + 1)
		{
			barsToCalc = otherTfBarsOnThisTf + 1;
			calculatedBars = ratesTotal - barsToCalc;
		}
	}

	// для удобства отладки делаем отсчёт такой же, как в MT4
	ArraySetAsSeries(time, true);
	ArraySetAsSeries(high, true);
	ArraySetAsSeries(low, true);

	// обновить цвета
	UpdateAutoColors();

	EmptyBars(_isFirstRun ? -1 : barsToCalc);
	_isFirstRun = false;

	// получить средний размер нескольких последних баров
	double touchDistance = 0;

	if (Sensitivity > 0)
	{
		// Быстрый способ рассчитать среднюю высоту баров за некоторое заведомо статистически приемлемое количество последних баров
		int checkBarCount = PutInRange(20 * MaPeriod * _tf / _period, _maPeriod, ratesTotal);
		double avgHigh = Mean(high, 0, checkBarCount);
		double avgLow = Mean(low, 0, checkBarCount);
		double avgRange = PutInRange(MathAbs(avgHigh - avgLow), _Point, DBL_MAX);

		touchDistance = avgRange * Sensitivity / 100.0;
	}

	double ma0; // MA текущего бара

	double distance; // расстояние средней до бара

	for (int i = barsToCalc - 1; i >= 0 ; i--)
	{
		EmptyBar(i);

		// Получить значение MA для текущего бара и бара слева от него
		if (Interpolate)
		{
			ma0 = IndicatorValueByTime(_mah, time[i]);

			if (ma0 == EMPTY_VALUE)
			{
				break;
			}
		}
		else
		{
			// определить время бара другого таймфрейма по времени текущего бара текущего тф
			datetime times[];

			if (CopyTime(_Symbol, TimeFrame, time[i], 2, times) < 2)
			{
				break;
			}

			datetime time0 = times[1]; // время текущего бара
			datetime timeL = times[0]; // время бара слева от текущего, time0 > timeL

			ma0 = IndicatorValueByTime(_mah, time0);

			if (ma0 == EMPTY_VALUE)
			{

				break;
			}

		}

		int bar0 = i;
		int barL = (i < ratesTotal - 1) ? i + 1 : i;

		_ma.Buffer[bar0] = ma0;
		_color.Buffer[bar0] = 0;

		// Расстояние от линии до цены
		distance = GetDistance(ma0, low[i], high[i]);

		// В зависимости от расстояния раскрасить дополнительные линии-тени
		if (Sensitivity > 0)
		{
			double maxDistance = 2;

			for (int ci = 1; ci < COLOR_COUNT; ci++)
			{
				double k = maxDistance * (COLOR_COUNT - 1 - ci) / (COLOR_COUNT - 2);

				if (distance <= k * touchDistance)
					_color.Buffer[bar0] = ci;
			}

		}

		calculatedBars++;
	}

	return(calculatedBars);
}

void OnDeinit(const int reason)
{
	IndicatorRelease(_mah);
}

void OnChartEvent(const int id, const long &lparam, const double &dparam, const string &sparam)
{
	if (id == CHARTEVENT_CHART_CHANGE)
		UpdateAutoColors();
}

// Обновить цвета, вычисляемые автоматически. Если обновление произошло, вернуть true, иначе false
bool UpdateAutoColors()
{
	color newBgColor = (color)ChartGetInteger(0, CHART_COLOR_BACKGROUND);

	if (newBgColor == _prevBackgroundColor)
		return(false);

	color lineColor = ColorIsNone(LineColor) ? newBgColor : LineColor;
	color touchColor = ColorIsNone(TouchColor) ? newBgColor : TouchColor;
	double shift = ColorIsNone(LineColor) ? 0.15 : 0;

	for (int i = 0; i < COLOR_COUNT; i++)
	{
		PlotIndexSetInteger(0, PLOT_LINE_COLOR, i, MixColors(lineColor, touchColor, shift + 0.85 * i / (COLOR_COUNT - 1), 1));
	}

	_prevBackgroundColor = newBgColor;
	return(true);
}

bool EmptyBars(int bars = -1)
{
	if (bars < 0)
	{
		ArrayInitialize(_ma.Buffer, EMPTY_VALUE);
		ArrayInitialize(_color.Buffer, 0);
	}
	else
	{
		ArrayInitialize(_ma.Buffer, EMPTY_VALUE, 0, bars);
		ArrayInitialize(_color.Buffer, 0, 0, bars);
	}

	return(true);
}

void EmptyBar(int bar)
{
	_ma.Buffer[bar] = EMPTY_VALUE;
	_color.Buffer[bar] = 0;
}

double GetDistance(const double value, const double low, const double high)
{
	if (low >= value)
		return(low - value);
	else if (high <= value)
		return(value - high);

	return(0);
}

struct Plot
{
	double Buffer[];  // буфер линии

	int Handle;       // хэндл основного индикатора

};

template <typename T>
bool ArrayCheckRange(const T &arr[], int &start, int &count)
{
	int size = ArraySize(arr);

	// в случае пустого массива результат неопределён, но вернуть как ошибочный
	if (size <= 0)
		return(false);

	// в случае нулевого диапазона результат неопределён, но вернуть как ошибочный
	if (count == 0)
		return(false);

	// старт выходит за границы массива
	if ((start > size - 1) || (start < 0))
		return(false);

	if (count < 0)
	{
		// если количество не указано, вернуть всё от start
		count = size - start;
	}
	else if (count > size - start)
	{
		// если элементов недостаточно для указанного количества, вернуть максимально возможное
		count = size - start;
	}

	return(true);
}

template <typename T>
T PutInRange(const T value, const T from, const T to)
{
	return(PUT_IN_RANGE(value, from, to));
}

double MathRound(const double value, const double error)
{
	return(error == 0 ? value : MathRound(value / error) * error);
}

double Mean(const double &arr[], int start = 0, int count = -1)
{
	if (!ArrayCheckRange(arr, start, count))
		return(NAN);

	double sum = 0;

	for (int i = start, end = start + count; i < end; i++)
		sum += arr[i];

	return(sum / count);
}

bool ColorToRGB(const color c, int &r, int &g, int &b)
{
	// Если цвет задан неверный, либо задан как отсутствующий, вернуть false
	if (COLOR_IS_NONE(c))
		return(false);

	b = (c & 0xFF0000) >> 16;
	g = (c & 0x00FF00) >> 8;
	r = (c & 0x0000FF);

	return(true);
}

color MixColors(const color color1, const color color2, double mix, double step = 16)
{
	// Коррекция неверных параметров
	step = PUT_IN_RANGE(step, 1.0, 255.0);
	mix = PUT_IN_RANGE(mix, 0.0, 1.0);

	int r1, g1, b1;
	int r2, g2, b2;

	// Разбить на компоненты
	ColorToRGB(color1, r1, g1, b1);
	ColorToRGB(color2, r2, g2, b2);

	// вычислить
	int r = PUT_IN_RANGE((int)MathRound(r1 + mix * (r2 - r1), step), 0, 255);
	int g = PUT_IN_RANGE((int)MathRound(g1 + mix * (g2 - g1), step), 0, 255);
	int b = PUT_IN_RANGE((int)MathRound(b1 + mix * (b2 - b1), step), 0, 255);

	return(RGB_TO_COLOR(r, g, b));
}

bool ColorIsNone(const color c)
{
	return(COLOR_IS_NONE(c));
}

string PeriodToString(const ENUM_TIMEFRAMES tf = PERIOD_CURRENT)
{
	int period = PeriodSeconds(tf) / 60;

	if (period % 43200 == 0)
		return("MN" + IntegerToString(period / 43200));
	else if (period % 10080 == 0)
		return("W" + IntegerToString(period / 10080));
	else if (period % 1440 == 0)
		return("D" + IntegerToString(period / 1440));
	else if (period % 60 == 0)
		return("H" + IntegerToString(period / 60));
	else
		return("M" + IntegerToString(period));
}

string MaMethodToString(const ENUM_MA_METHOD maMethod)
{
	switch (maMethod)
	{
		case MODE_SMA: return("SMA");
		case MODE_EMA: return("EMA");
		case MODE_SMMA: return("SMMA");
		case MODE_LWMA: return("LWMA");
		default: return("MA(type " + IntegerToString(maMethod) +")");
	}
}

int PeriodMinutes(const ENUM_TIMEFRAMES period)
{
	return(PERIOD_TO_MINUTES(period));
}

double IndicatorValueByTime(const int handle, const datetime barTime, const double emptyValue = EMPTY_VALUE)
{
	if (handle == INVALID_HANDLE)
		return(emptyValue);

	double values[];

	if (CopyBuffer(handle, 0, barTime, 1, values) != 1)
		return(emptyValue);

	return(values[0]);
}

template <typename ArrayType, typename ValueType>
int ArrayInitialize(ArrayType &arr[], const ValueType value, int start, int count = -1)
{
	if (!ArrayCheckRange(arr, start, count))
		return(0);

	for (int i = start, end = start + count; i < end; i++)
		arr[i] = (ArrayType)value;

	return(count);
}

bool TimeOfBar(string symbol_name, ENUM_TIMEFRAMES timeframe, int start_pos, datetime &time)
{
	datetime time_array[];
	int result = CopyTime(symbol_name, timeframe, start_pos, 1, time_array);

	if (result == 1)
		time = time_array[0];

	return(result > 0);
}

int _maPeriod;
int _tf;
int _period; // Текущий период в минутах
bool _isFirstRun = true;

color _prevBackgroundColor = clrNONE;
bool _isParamsOk = true;

Plot _ma;
Plot _color;
int _mah;

/*
Список изменений в индикаторе MA

5.0
	* для MT5 используется встроенный тип многоцветной линии вместо имитации, количество цветов увеличено до 8
	* изменены ссылки на сайт
	* список изменений в коде

4.1
	* исправлена ошибка при сборке релиза

4.0
	* добавлен вариант для MetaTrader 5
	* убран параметр TouchDistance, приближение и касание теперь определяется автоматически

3.1
	* оптимизация за счёт обновления в используемых библиотеках
	* изменены ссылки на сайт

3.0
	* адаптация к новым возможностям MQL4 (build 600+)
	* больше нет необходимости во внешних библиотеках (DLL)
	* исправлено: выход за границы массива

2.2
    * поддержка MetaTrader 4.00 Build 600 и новее.

2.1
    * оптимизация скорости выполнения
    * исправлено смешение цветов (обновление в MT4.XLib)

2.0
    * визуализация касания цены (новые параметры LineColor, TouchColor, LineStyle, LineWidth, TouchDistance)
    * отключена работа индикатора в режиме интерполяции для таймфреймов (графика или индикатора) больше D1 (1440)
    * возможность отображения средней меньшего таймфрейма на графике большего в режиме интерполяции (например, MA200@H1 интерполируется в MA8@D1)
    * улучшен текст подсказок

1.1.1
    * первая публичная версия

Лицензия:

Разрешается повторное распространение и использование как в виде исходного кода, так и в двоичной форме, с изменениями
или без, при соблюдении следующих условий:

При повторном распространении исходного кода должно оставаться указанное выше уведомление об авторском праве, этот
список условий и последующий отказ от гарантий.

При повторном распространении двоичного кода должна сохраняться указанная выше информация об авторском праве, этот
список условий и последующий отказ от гарантий в документации и/или в других материалах, поставляемых при распространении.

Ни название FXcoder, ни имена ее сотрудников не могут быть использованы в качестве поддержки или продвижения продуктов,
основанных на этом ПО без предварительного письменного разрешения.

ЭТА ПРОГРАММА ПРЕДОСТАВЛЕНА ВЛАДЕЛЬЦАМИ АВТОРСКИХ ПРАВ И/ИЛИ ДРУГИМИ СТОРОНАМИ «КАК ОНА ЕСТЬ» БЕЗ КАКОГО-ЛИБО ВИДА
ГАРАНТИЙ, ВЫРАЖЕННЫХ ЯВНО ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ, ПОДРАЗУМЕВАЕМЫЕ ГАРАНТИИ КОММЕРЧЕСКОЙ
ЦЕННОСТИ И ПРИГОДНОСТИ ДЛЯ КОНКРЕТНОЙ ЦЕЛИ. НИ В КОЕМ СЛУЧАЕ, ЕСЛИ НЕ ТРЕБУЕТСЯ СООТВЕТСТВУЮЩИМ ЗАКОНОМ, ИЛИ НЕ
УСТАНОВЛЕНО В УСТНОЙ ФОРМЕ, НИ ОДИН ВЛАДЕЛЕЦ АВТОРСКИХ ПРАВ И НИ ОДНО ДРУГОЕ ЛИЦО, КОТОРОЕ МОЖЕТ ИЗМЕНЯТЬ И/ИЛИ
ПОВТОРНО РАСПРОСТРАНЯТЬ ПРОГРАММУ, КАК БЫЛО СКАЗАНО ВЫШЕ, НЕ НЕСЁТ ОТВЕТСТВЕННОСТИ, ВКЛЮЧАЯ ЛЮБЫЕ ОБЩИЕ, СЛУЧАЙНЫЕ,
СПЕЦИАЛЬНЫЕ ИЛИ ПОСЛЕДОВАВШИЕ УБЫТКИ, ВСЛЕДСТВИЕ ИСПОЛЬЗОВАНИЯ ИЛИ НЕВОЗМОЖНОСТИ ИСПОЛЬЗОВАНИЯ ПРОГРАММЫ (ВКЛЮЧАЯ, НО
НЕ ОГРАНИЧИВАЯСЬ ПОТЕРЕЙ ДАННЫХ, ИЛИ ДАННЫМИ, СТАВШИМИ НЕПРАВИЛЬНЫМИ, ИЛИ ПОТЕРЯМИ ПРИНЕСЕННЫМИ ИЗ-ЗА ВАС ИЛИ ТРЕТЬИХ
ЛИЦ, ИЛИ ОТКАЗОМ ПРОГРАММЫ РАБОТАТЬ СОВМЕСТНО С ДРУГИМИ ПРОГРАММАМИ), ДАЖЕ ЕСЛИ ТАКОЙ ВЛАДЕЛЕЦ ИЛИ ДРУГОЕ ЛИЦО БЫЛИ
ИЗВЕЩЕНЫ О ВОЗМОЖНОСТИ ТАКИХ УБЫТКОВ.

License:

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

Neither the name of the FXcoder nor the names of its contributors may be used to endorse or promote products derived
from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

// 2016-02-04 06:48:14 UTC. MQLMake 1.37. © FXcoder