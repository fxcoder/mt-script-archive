#property copyright "Color Manager v2.3. © FXcoder"
#property link      "http://fxcoder.blogspot.com"
#property strict

#define FOLDER "ColorManager\\"
#define BUTTON_COLOR clrBlack
#define BUTTON_BACK_COLOR clrGainsboro
#define BUTTON_DOWN_BACK_COLOR clrGray
#define DELETE_BUTTON_COLOR clrRed
#define FIELD_BACK_COLOR clrWhite

#include <chartobjects\chartobjectstxtcontrols.mqh>

CChartObjectButton *buttons[]; // scheme buttons          // кнопки схем
CChartObjectEdit *edSaveName;  // scheme file name field  // поле имени файла схемы
CChartObjectButton *btSave;    // save button             // кнопка сохранения
CChartObjectButton *btClose;   // close button            // кнопка закрытия

void OnStart()
{
	bool doRestart = true;

	// Interface restart loop (after changes)
	// Цикл перезапуска интерфейса (после изменений)
	while (doRestart)
	{
		// Graphic objects name prefix
		// Префикс имен графических объектов
		string namePrefix = "cm_";

		// Remove old buttons
		// Удалить старые кнопки
		ArrayFree(buttons);

		int y = 10;

		// Find files with .colors extention
		// Найти файлы с расширением .colors
		string filter = "*.colors";

		// Add schemes buttons
		// Добавить кнопки схем
		string fileName;
		long search = FileFindFirst(FOLDER + filter, fileName);

		if (search != INVALID_HANDLE)
		{
			StringReplace(fileName, ".colors", "");

			addButton(namePrefix + fileName, fileName, 40, y);
			addButton(namePrefix + fileName + "?delete", "X", 10, y, 20, 20).Color(clrRed);

			y += 25;

			while (FileFindNext(search, fileName))
			{
				StringReplace(fileName, ".colors", "");
				addButton(namePrefix + fileName, fileName, 40, y);
				addButton(namePrefix + fileName + "?delete", "X", 10, y, 20, 20).Color(clrRed);
				y += 25;
			}

			FileFindClose(search);
		}

		edSaveName = new CChartObjectEdit();
		edSaveName.Create(0, namePrefix + "edSaveName", 0, 40, y, 100, 20);
		edSaveName.Description("enter name...");
		edSaveName.BackColor(FIELD_BACK_COLOR);
		edSaveName.Color(BUTTON_COLOR);
		edSaveName.FontSize(8);

		btSave = new CChartObjectButton();
		btSave.Create(0, namePrefix + "btSave", 0, 150, y, 50, 20);
		btSave.Description("Save");
		btSave.Color(BUTTON_COLOR);
		btSave.BackColor(BUTTON_BACK_COLOR);
		btSave.FontSize(8);

		btClose = new CChartObjectButton();
		btClose.Create(0, namePrefix + "btClose", 0, 210, y, 50, 20);
		btClose.Description("Close");
		btClose.Color(BUTTON_COLOR);
		btClose.BackColor(BUTTON_BACK_COLOR);
		btClose.FontSize(8);

		int bc = ArraySize(buttons);

		// Reset restart flag
		// Сбросить флаг перезапуска
		doRestart = false;

		ChartRedraw();

		// Interface loop (waiting for user command)
		// Цикл интерфейса (ожидание команд пользователя)
		while(!IsStopped())
		{
			// Check buttons state
			for (int i = 0; i < bc; i++)
			{
				if (buttons[i].State())
				{
					string name = buttons[i].Name();

					// choose or detele
					// выбор или удаление
					if (StringFind(name, "?delete") == -1)
					{
						loadColors(buttons[i].Description());
						edSaveName.Description(buttons[i].Description());

						visibleButtonRelease(buttons[i]);
					}
					else
					{
						// Get file name from button name
						// Получить имя файла из имени кнопки
						StringReplace(name, "?delete", "");
						name = StringSubstr(name, StringLen(namePrefix));
						FileDelete(FOLDER + name + ".colors");

						doRestart = true;
						buttons[i].Description("enter name...");

						visibleButtonRelease(buttons[i]);
						break;
					}
				}
			}

			// Restart after deletion
			// Перезапустить после удаления
			if (doRestart)
				break;

			// Save
			// Сохранить
			if (btSave.State())
			{
				saveColors(edSaveName.Description());
				visibleButtonRelease(btSave);
				doRestart = true;
				break;
			}

			// Close (no restart)
			// Закрыть (без перезапуска)
			if (btClose.State())
				break;

			Sleep(20);
		}

		// Remove schemes buttons
		// Удалить кнопки схем
		for (int i = 0; i < bc; i++)
			delete(buttons[i]);

		// Remove main controls
		// Удалить основные элементы управления
		delete(edSaveName);
		delete(btSave);
		delete(btClose);
	}
}

// Visible (slow) button release
// Видимое (Медленное) отжатие кнопки
void visibleButtonRelease(CChartObjectButton* button)
{
	button.BackColor(BUTTON_DOWN_BACK_COLOR);
	ChartRedraw();
	Sleep(77);
	button.BackColor(BUTTON_BACK_COLOR);
	button.State(false);
	ChartRedraw();
}

// Add scheme button
// Добавить кнопку схемы
CChartObjectButton* addButton(string name, string text, int x, int y, int w = 100, int h = 20)
{
	int bc = ArraySize(buttons);
	ArrayResize(buttons, bc + 1);

	buttons[bc] = new CChartObjectButton();
	buttons[bc].Create(0, name, 0, x, y, w, h);
	buttons[bc].Description(text);
	buttons[bc].Color(BUTTON_COLOR);
	buttons[bc].BackColor(BUTTON_BACK_COLOR);
	buttons[bc].FontSize(8);

	return (buttons[bc]);
}

// Load colors from file
// Загрузить цвета из файла
void loadColors(string name)
{
	int h = FileOpen(FOLDER + name + ".colors", FILE_READ | FILE_TXT | FILE_ANSI);

	if (h != INVALID_HANDLE)
	{
		ChartSetInteger(0, CHART_COLOR_ASK, getColorParam(FileReadString(h)));
		ChartSetInteger(0, CHART_COLOR_BACKGROUND, getColorParam(FileReadString(h)));
		ChartSetInteger(0, CHART_COLOR_BID, getColorParam(FileReadString(h)));
		ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, getColorParam(FileReadString(h)));
		ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, getColorParam(FileReadString(h)));
		ChartSetInteger(0, CHART_COLOR_CHART_DOWN, getColorParam(FileReadString(h)));
		ChartSetInteger(0, CHART_COLOR_CHART_LINE, getColorParam(FileReadString(h)));
		ChartSetInteger(0, CHART_COLOR_CHART_UP, getColorParam(FileReadString(h)));
		ChartSetInteger(0, CHART_COLOR_FOREGROUND, getColorParam(FileReadString(h)));
		ChartSetInteger(0, CHART_COLOR_GRID, getColorParam(FileReadString(h)));
		ChartSetInteger(0, CHART_COLOR_LAST, getColorParam(FileReadString(h)));
		ChartSetInteger(0, CHART_COLOR_STOP_LEVEL, getColorParam(FileReadString(h)));
		ChartSetInteger(0, CHART_COLOR_VOLUME, getColorParam(FileReadString(h)));

		FileClose(h);
	}

	ChartRedraw();
}

// Get color parameter from string
// Получить цветовой параметр из строки
color getColorParam(string line)
{
	int pos = StringFind(line, "=");

	if (pos >= 0)
		return((color)StringToInteger(StringSubstr(line, pos + 1)));
	else
		return(clrNONE);
}

// Save colors to file
// Сохранить цвета в файл
void saveColors(string name)
{
	int h = FileOpen(FOLDER + name + ".colors", FILE_WRITE | FILE_TXT | FILE_ANSI);

	if (h != INVALID_HANDLE)
	{
		FileWriteString(h, "askline_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_ASK)) + "\n");
		FileWriteString(h, "background_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_BACKGROUND)) + "\n");
		FileWriteString(h, "bidline_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_BID)) + "\n");
		FileWriteString(h, "bearcandle_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_CANDLE_BEAR)) + "\n");
		FileWriteString(h, "bullcandle_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_CANDLE_BULL)) + "\n");
		FileWriteString(h, "bardown_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_CHART_DOWN)) + "\n");
		FileWriteString(h, "chartline_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_CHART_LINE)) + "\n");
		FileWriteString(h, "barup_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_CHART_UP)) + "\n");
		FileWriteString(h, "foreground_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_FOREGROUND)) + "\n");
		FileWriteString(h, "grid_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_GRID)) + "\n");
		FileWriteString(h, "lastline_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_LAST)) + "\n");
		FileWriteString(h, "stops_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_STOP_LEVEL)) + "\n");
		FileWriteString(h, "volumes_color=" + IntegerToString(ChartGetInteger(0, CHART_COLOR_VOLUME)) + "\n");

		FileClose(h);
	}
}

/*
Список изменений:

2.3
	* изменены ссылки на сайт
	* список изменений в коде
	* рефакторинг

2.2
	* исправлено: большая нагрузка на процессор

2.1
	* изменены ссылки на сайт

2.0
	* первая опубликованная версия

Лицензия:

Разрешается повторное распространение и использование как в виде исходного кода, так и в двоичной форме, с изменениями
или без, при соблюдении следующих условий:

При повторном распространении исходного кода должно оставаться указанное выше уведомление об авторском праве, этот
список условий и последующий отказ от гарантий.

При повторном распространении двоичного кода должна сохраняться указанная выше информация об авторском праве, этот
список условий и последующий отказ от гарантий в документации и/или в других материалах, поставляемых при распространении.

Ни название FXcoder, ни имена ее сотрудников не могут быть использованы в качестве поддержки или продвижения продуктов,
основанных на этом ПО без предварительного письменного разрешения.

ЭТА ПРОГРАММА ПРЕДОСТАВЛЕНА ВЛАДЕЛЬЦАМИ АВТОРСКИХ ПРАВ И/ИЛИ ДРУГИМИ СТОРОНАМИ «КАК ОНА ЕСТЬ» БЕЗ КАКОГО-ЛИБО ВИДА
ГАРАНТИЙ, ВЫРАЖЕННЫХ ЯВНО ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ, ПОДРАЗУМЕВАЕМЫЕ ГАРАНТИИ КОММЕРЧЕСКОЙ
ЦЕННОСТИ И ПРИГОДНОСТИ ДЛЯ КОНКРЕТНОЙ ЦЕЛИ. НИ В КОЕМ СЛУЧАЕ, ЕСЛИ НЕ ТРЕБУЕТСЯ СООТВЕТСТВУЮЩИМ ЗАКОНОМ, ИЛИ НЕ
УСТАНОВЛЕНО В УСТНОЙ ФОРМЕ, НИ ОДИН ВЛАДЕЛЕЦ АВТОРСКИХ ПРАВ И НИ ОДНО ДРУГОЕ ЛИЦО, КОТОРОЕ МОЖЕТ ИЗМЕНЯТЬ И/ИЛИ
ПОВТОРНО РАСПРОСТРАНЯТЬ ПРОГРАММУ, КАК БЫЛО СКАЗАНО ВЫШЕ, НЕ НЕСЁТ ОТВЕТСТВЕННОСТИ, ВКЛЮЧАЯ ЛЮБЫЕ ОБЩИЕ, СЛУЧАЙНЫЕ,
СПЕЦИАЛЬНЫЕ ИЛИ ПОСЛЕДОВАВШИЕ УБЫТКИ, ВСЛЕДСТВИЕ ИСПОЛЬЗОВАНИЯ ИЛИ НЕВОЗМОЖНОСТИ ИСПОЛЬЗОВАНИЯ ПРОГРАММЫ (ВКЛЮЧАЯ, НО
НЕ ОГРАНИЧИВАЯСЬ ПОТЕРЕЙ ДАННЫХ, ИЛИ ДАННЫМИ, СТАВШИМИ НЕПРАВИЛЬНЫМИ, ИЛИ ПОТЕРЯМИ ПРИНЕСЕННЫМИ ИЗ-ЗА ВАС ИЛИ ТРЕТЬИХ
ЛИЦ, ИЛИ ОТКАЗОМ ПРОГРАММЫ РАБОТАТЬ СОВМЕСТНО С ДРУГИМИ ПРОГРАММАМИ), ДАЖЕ ЕСЛИ ТАКОЙ ВЛАДЕЛЕЦ ИЛИ ДРУГОЕ ЛИЦО БЫЛИ
ИЗВЕЩЕНЫ О ВОЗМОЖНОСТИ ТАКИХ УБЫТКОВ.

License:

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following
disclaimer.

Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided with the distribution.

Neither the name of the FXcoder nor the names of its contributors may be used to endorse or promote products derived
from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

*/

// 2016-02-04 06:37:48 UTC. MQLMake 1.37. © FXcoder