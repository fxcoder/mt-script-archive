English: [README_EN.md](README_EN.md).

# Архив скриптов для MetaTrader

Здесь находятся старые версии скриптов, оставленные для

- фанатов старых версий
- тех, кому не подходит лицензия новых версий скриптов (здесь BSD 3-clause, в новых версиях - GPL v3.0).

Новые версии скриптов здесь: <https://gitlab.com/fxcoder-mql>.

См. также: <https://fxcoder.blogspot.com/search/label/[Скрипт]>.
