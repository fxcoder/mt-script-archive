# Archive of scripts for MetaTrader

Here are the old scripts left for

- fans of the old versions
- those who are not suitable for the license of new versions of scripts (it is BSD 3-clause here, and GPL v3.0 for new versions).

New versions of scripts are here: <https://gitlab.com/fxcoder-mql>.

See also (Russian): <https://fxcoder.blogspot.com/search/label/[Скрипт]>.
